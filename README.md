Zebramaze
=========

Code repository for the analysis used in the paper **"Zebrafish exploit visual cues and geometric relationships to form a spatial memory"**

Raw data is available at [Gin](https://web.gin.g-node.org/KseniaYashina/Zebramaze).

Data structure is described in the `docs` folder, `access_layer`
notebook. The setup is described in the
`docs\description_of_experimental_set-up`.

## Installation
The code is written in Python 3.6. 

* Install the required Python packages: 
    `pip install -r requirements.txt`
* Download the raw files, and specify their location in `md.py`. By
  default raw files are assumed to be located in the folder
  `module_folder\raw_data`, the tables with the description of protocols
  in `module_folder\raw_data\tables\`.

## Exploring the data
Use python notebooks in the folder `notebooks` to explore different
aspects of the analysis. 

Most notebooks are dedicated to a particular type of plot of analysis or
data handling, e.g. `occupancy.ipynb` illustrates how occupancy plots,
such as moving averages, are generated. 

The subfolder `protocol_analysis` contains notebooks for analysis of
specific experimental protocols, including the moving averages and
permutation tests.

The subfolder `heatmap_analysis` contains notebooks for heatmap analysis
of individual trajectories in different experimental protocols.

## Figure generation
The code used for the generation of figures is (obviously) placed in the
scripts names `figure_XX,py`. Each script generates a png and tiff of
the figure with all panels. The style of the figures is specifies in the
files `figstyle.INI` and `paper.mplstyle`.