{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Access layer documentation\n",
    "\n",
    "This notebook explains how to access fish trajectory data and its related metadata. It describes, from a functional point of view, the structure of the experimental data we have and how we have decided to make it accessible. Where necessary it illustrates design tradeoffs and identifies missing bits. Analysis proper is done elsewhere."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "import pandas as pd\n",
    "import zebramaze.md as md"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Base tables: `subjects`\n",
    "\n",
    "|Field|Explanation|\n",
    "|-----|-----------|\n",
    "|`cross_date`| date when parents are set for mating; the fish actually lay eggs in the morning on the next day, i.e. their real birth date is cross_date + 1 day|\n",
    "|`sj_id`|a date-based id that tacks on a serial number on the date. Format is DDMMYY-NN, where DDMMYY is the date of the cross, NN - serial number of fish of certain cross date|"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "sj = md.subjects()\n",
    "sj.head(2)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Base tables: `experiments`\n",
    "Each subject generally undergoes only one experiment and an experiment doesn't span more than one day, so the identifier of the experiment is just the subject id with -01 tacked on."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "xp = md.experiments()\n",
    "xp.sample(2)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "|Field|Explanation| \n",
    "|-----|-----------|\n",
    "|`xp_duration`|Experiment duration in frames; the recording rate in all experiments so far has been 60 frames per second. It is a sum of `ss_duration` from `session` table and is stored here for convenience|\n",
    "|`xp_age`|Age of the fish on the day of the experiment.|\n",
    "|`xp_type`|Experiment type; it is of type `category`, i.e. it is internally recoded to an integer with the possiblity of NaN|\n",
    "|`length`|Body length of the fish at the time of the experiment. It is estimated from the video of experiments as a distance between head and tail tip, which are defined by the user, detected throughout the 'control' stage of each experiment. <div class=alert-warning> As from November 2016 recordings are supported by recorded videos of experiments, which allow for better size estimation</div>|\n",
    "|`time`|Time of the day, when experiment was carried out|\n",
    "|`sh_duration`|Duration of a single electric shock in frames|\n",
    "|`rotifers`|True if the growing larvae were fed rotifers (usually resulted in faster growth)| \n",
    "|`env_layout`|A categorical variable to describe the layout of the maze (and give order to maze arms)|\n",
    "|`env_geometry`|A list of coordinates, describing positions of the arms of the maze. Each arm is represented by five coordinates, corresponding to four corners and the midpoint of the wall, which is farthest from the arm entrance|"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "xp.dtypes"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "xp.xp_type.unique()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Merging subjects and experiments is 1 to 1 and straightforward. Here we use it to check that the age field has been properly computed (the `exp_date` minus the `cross_date` should give the `age` plus 1, as crossing is one day before birth.)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "xp_sj =  xp.merge(sj, on='sj_id')\n",
    "pd.DataFrame(dict(computed=xp_sj.xp_date - xp_sj.cross_date, \n",
    "                  registered_in_days=xp_sj.xp_age.values)).sample(4)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Maze position in an experiment is described by environment variables: layout and geometry. The first assigns names to polygons (l - left, u - upper, r - right, b - bottom), coordinates of which are given in the second."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "env_columns = ['env_layout', 'env_geometry']\n",
    "xp[env_columns].sample(3)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The environment layout is a string of characters, each one describing one arm or polygon, e.g. `lur`. It is also stored as a categorical variable"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "xp.env_layout.unique()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Base tables: `sessions`\n",
    "An experiment is composed of several sessions."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ss = md.sessions()\n",
    "ss.head(3)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "|Field|Explanation|\n",
    "|-----|-----------|\n",
    "|`ss_duration`| Duration of the session in frames.| \n",
    "|`ss_no`| identifies ordinally the session within the experiment, and it is tacked on `xp_id` to give an unique `ss_id` that identifies the session and will be used (below) to access session-level tracking data|\n",
    "|`ss_type`|category field, identifies what event is associated with this particular session|\n",
    "|`env_patterns`|pattern configuration of the environment (can change with each session, e.g. to test spatial recognition of the arrangement). Format: like `env_layout` -- a list of characters that matches the `env_layout` and geometry. For example, a `env_pattern` with value `xvi` on session 0 matching an `env_layout` of `lur` means the pattern `x` for the orientation `l` and so on, and could be changed by *clockwise rotation* to `ixv` for session, whereby pattern `i` would match orientation `l` and so on.|\n",
    "|`stim_at`| an arm number corresponding to `env_layout` and `env_geometry`. Arms are numberd 0, 1, and 2; 3 corresponds to the center of the maze. Shocked pattern can be calculated by `env_patterns`[`stim_at`]|\n",
    "|`stim_current`| strength of the current applied during the session **[in mA]**. Can be either a float or a NaN (in case when shocks were switched off, e.g. during HABIT or TEST sessions)|"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ss.dtypes"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ss.ss_type.unique()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Session types can be:\n",
    "* HABIT (first habituation session, no shocks)\n",
    "* CONDIT (conditioning session after control)\n",
    "* TEST (no-shock session after conditioning)\n",
    "* ROTATE +/- 120 (rotation of patterns clockwise and by how many degrees they are rotated)\n",
    "* GREY (switch shocked pattern to uniform grey)\n",
    "* REPLACE (switch shocked pattern to another pattern with dots)\n",
    "* REPLACE_TURN +/- 120 (switch shocked pattern to another one and rotate the patterns by specified degrees)\n",
    "* SWAP (swap of the safe patterns)\n",
    "\n",
    "The following combinations will produce `xp_type`:\n",
    "* HABIT -> control\n",
    "* HABIT-CONDIT -> training\n",
    "* HABIT-CONDIT-GREY -> remove\n",
    "* HABIT-CONDIT-REPLACE -> replace\n",
    "* HABIT-CONDIT-TEST -> test\n",
    "* HABIT-CONDIT-GREY-TEST -> memory_5\n",
    "* HABIT-CONDIT-TEST & env_patterns == 'ddd' -> same_pattern\n",
    "* HABIT-CONDIT-TURN -> turn\n",
    "* HABIT-CONDIT-REPLACE_TURN -> replace_turn\n",
    "* HABIT-CONDIT-REPLACE_SWWAP -> swap"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# NEW FORMAT Session-wise position data\n",
    "\n",
    "We have a helper function which merges subjects, experiments and sessions into a single data frame containing all fields for a given session. We generally call such data frames in abbreviated form `ssa` (*session_all*). Having merged all the metadata, we also add for simplicity a path to the coordinate data in the column `ss_coords_path`.\n",
    "\n",
    "Positional data can then be retrieved with the Pandas HDF interface.\n",
    "During the experiment positional data is estimated based on background subtraction and filtered in real-time with Kalman filter. Thus available from the recording are `x` and `y` coordinates of the fish. In addition we store a folder with videos of experiments. These videos are used after the experiment for an improved estimation of fish's position, direction, distance to the maze wall."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ssa = md.ssa_new.sample(1)\n",
    "ssa.head(2)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "coords_ex = pd.read_hdf(ssa.ss_coords_path.iloc[0])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "coords_ex.sample(5)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "coords_ex.dtypes"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "len(coords_ex)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "|Field|Explanation| \n",
    "|-----|-----------|\n",
    "|`x`, `y`| x and y coordinates of fish's center of mass (identified from the contour).|\n",
    "|`shock`| True if shock was applied during this frame|\n",
    "|`arm`| which arm is the fish in at the current frame, based on raw coordinates. Arm column is generated by comparing raw (x, y) coordinates of fish's center to coordinates of maze layout (stored in `xp_table.env_geometry`).|\n",
    "|`hh`/`mm`/`ss`|hour/minute/second stamp of this frame|"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "coords_ex.arm.unique()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Corrected positional data\n",
    "After the experiments we use recorded videos to go through the recorded positions of the fish and apply additional filtering to exclude jitter of the real-time recording. We recalculate fish positions, as well as calculate heading direction of the fish and its distance from the walls of the maze."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "|Field|Explanation| \n",
    "|-----|-----------|\n",
    "|`xfix`, `yfix`, `armfix`| x, y coordinats of the fish and the corresponding arm of the maze after an offline analysis if the trajectories, using a slower but more efficient algorithm for detection of fish's position.|\n",
    "|`ang`| heading direction of the fish, calculated from a vector from the tail tip to the head of the fish.|\n",
    "|`thigmo`| Euclidean distance from the fish's (x, y) to the nearest maze wall in pixels (1 cm = 70 pixels). Can be used to estimate thigmotaxis.|"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "-------"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## OLD FORMAT Session-wise position data\n",
    "\n",
    "Positional data can then be retrieved with the Pandas HDF interface.\n",
    "\n",
    "<div class=alert-warning> As of November 2016 recording algorithm changed. During the experiment positional data is estimated based on background subtraction and filtered in real-time with Kalman filter. Thus available from the recording are `x` and `y` coordinates of the fish. In addition we store a folder with videos of experiments. These videos can be used for extimation of fish's tail position, direction etc.</div>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ssa = md.ssa.loc[~md.xp_new]\n",
    "ssa.head(2)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "coords_ex = pd.read_hdf(ssa.ss_coords_path.iloc[0])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "coords_ex.sample(5)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "coords_ex.dtypes"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "len(coords_ex)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "|Field|Explanation| \n",
    "|-----|-----------|\n",
    "|`frame`| frame number|\n",
    "|`m`| mask; `True` if visual stimulation was on in that frame; `False` if visual stimulation was off (can happen if the fish is stuck in shock-arm for > 60 sec)| \n",
    "|`t_x`, `t_y`| x resp. y coordinates of fish's tail tip (identified as the furthermost point of contour from center of mass); obviously will produce problems when the fish freezes - then 'tail' will slowly merge with 'center').|\n",
    "|`x`, `y`| x and y coordinates of fish's center of mass (identified from the contour).|\n",
    "|`arm`| which arm is the fish in at the current frame, based on raw coordinates. The convention is that 1 always maps the 'North' of the video (thus for a Y-maze arms 0 is left, 1 - upper, 2 - right, 3 - center, 4 - unidentified). Arm column is generated by comparing raw coordinates of fish's center to coordinates of maze layout (stored in `xp_table.env_geometry`).| "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "coords_ex.arm.unique()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Normally `m` column should have `True` values. However if an experiment was interrupted for some time (e.g. if fish spent too much time in the shocked arm, receiving too many electric shocks) and visual and electric stimulation were switched off, `m` column has `False` values."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(np.sum(coords_ex.m != 1))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "coords_ex.loc[coords_ex.m != 1]"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.4"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 1
}
