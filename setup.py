# coding: utf-8

try:
    from setuptools import setup
except ImportError:
    from distutils.core import setup

config = {
    'description': 'Data analysis package for fish spatial behaviour.',
    'author': 'Ksenia Yashina, Álvaro Tejero-Cantero',
    'url': 'URL to get it at.',
    'download_url': 'Where to download it.',
    'author_email': 'yashina@neuro.mpg.de, tejero@bio.lmu.de',
    'version': '0.1',
    'install_requires': ['nose', 'numpy', 'pandas', 'matplotlib'],
    'packages': ['zebramaze'],
    'scripts': [],
    'name': 'zebramaze'
}

setup(**config)
