__author__ = 'yashina'

import cv2, numpy as np


ALPHA = 0.5  # (black area)/(whole area) ratio
N = 4  # number of vertical lines, must be even
M = 4  # number of dots in a horizontal line
A = 100  # height
B = 100  # width
C = 180.0  # lighter color for patterns in 0..255

ANGLE = 10.0  # vision resolution in degrees
SIZE = int(A*2*np.tan(ANGLE*np.pi/180/2))  # kernel size for filtering
if SIZE % 2:
    pass
else:
    SIZE += 1

col2rgb = {'r': (255, 0, 0), 'g': (0, 255, 0), 'b': (0, 0, 255)}


def dots(bg, n, m, alpha, **kw):
    """
    Create a pattern of white dots on the background
    :param bg: array for the background
    :param n: number of dots in a row
    :param m: number of rows
    :param a:
    :param b:
    :param alpha:
    :param kw:
    :return:
    """

    a, b = bg.shape[:2]
    r = int(np.sqrt(alpha/np.pi/(m**2))*a)  # dot radius
    l_a = a / n
    l_b = b / m

    for i in range(n):
        for j in range(m+1):
            if i % 2:
                cv2.circle(bg, (int(l_b*(0.5+j)), int(l_a*(0.5+i))),
                           r, (0, 0, 0), -1)
            else:
                cv2.circle(bg, (int(l_b*j), int(l_a*(0.5+i))),
                           r, (0, 0, 0), -1)
    return bg


def vlines(bg, n, m, alpha, **kw):

    a, b = bg.shape[:2]
    l_b = b / m

    for j in range(m):
        cv2.rectangle(bg, (int((j+1-alpha)*l_b), 0), (int((j+1)*l_b), b),
                      (0, 0, 0), -1)
    return bg


def hlines(bg, n, m, alpha, **kw):

    a, b = bg.shape[:2]
    l_a = a / n

    for i in range(n):
        cv2.rectangle(bg, (0, int(i*l_a)), (a, int((i+alpha)*l_a)),
                      (0, 0, 0), -1)
    return bg


def color(bg, n, m, alpha, col=np.array([125, 125, 125])):
    return np.ones_like(bg) * col


def xadrez(bg, n, m, alpha, **kw):

    a, b = bg.shape[:2]
    l_a = a / n
    l_b = b / n
    # n is the number of white&black tiles in a row -> n/2 black tiles
    size = np.sqrt(alpha/n/n*2)*a
    # print l_a, l_b

    for i in range(n):
        if i % 2:
            jj = range(1, m, 2)
        else:
            jj = range(0, m, 2)
        # print i, jj
        for j in jj:
            cv2.rectangle(bg,
                          (int((i+0.5)*l_a-size/2), int((j+0.5)*l_b-size/2)),
                          (int((i+0.5)*l_a+size/2), int((j+0.5)*l_b+size/2)),
                          (0, 0, 0), -1)
    return bg


def inv_dots(bg, n, m, alpha, **kw):

    a, b = bg.shape[:2]
    R = int(np.sqrt((1 - alpha)/np.pi/(m**2))*a)  # inversed color dot radius
    l_a = a / n
    l_b = b / m
    c = C/255.0

    bg *= 0
    for i in range(n):
        for j in range(m+1):
            if i % 2:
                cv2.circle(bg, (int(l_b*(0.5+j)), int(l_a*(0.5+i))), R,
                           (c, c, c), -1)
            else:
                cv2.circle(bg, (int(l_b*j), int(l_a*(0.5+i))), R,
                           (c, c, c), -1)
    return bg


def show_patt(a, b, n, m, alpha, name, func, col=None, save=False, show=True):

    bg = np.ones((a, b, 3))*C/255.
    cv2.resizeWindow(name, b, a)
    cv2.moveWindow(name, 10, 10)

    if col is None:
        bg = func(bg, n, m, alpha)
    else:
        bg = func(bg, n, m, alpha, col=col)

    if show:
        cv2.imshow(name, coarse(bg))
        while not(cv2.waitKey(10) == 13):
            pass
        cv2.destroyWindow(name)
    if save:
        cv2.imwrite(name+'.png', bg*255)
    else:
        return bg


def coarse(im):
    """ size -- filter size, is in relation with resolution of an eye
                viewed from 1 cm above
    """

    return cv2.GaussianBlur(im, (SIZE, SIZE), 0)


def create_rounded_maze(size, ratio=(1, 2), num=3, arr=False, patt=False,
                        seq=None, outer=0, e_ratio=3, inter=False,
                        c_col=(0.53, 0.53, 0.53), expand_center=0,
                        colors=None, bg=(0, 0, 0), cont=False, outline=False):
    """ create the maze

    size -- arm width in pixels
    ratio -- width to length ratio
    num -- number of arms
    patt -- flag to include patterns into maze image
    seq -- sequence of patterns, only relevant for patt=True

    """
    w = int(size)
    l = int(size*ratio[1]/ratio[0])
    a = arm_area(w, l)
    dist_c = int(center_dist(w, a/2))
    im_half = l+dist_c+outer
    field = im_half*2  # whole image is a square
    maze = np.zeros([field, field, 3]) + bg
    maze_pts = []
    for i in range(num):
        arm, arm_pts, arm_bg = create_arm(w, l, e_ratio=e_ratio, outer=outer,
                                          bg=bg)
        if expand_center != 0:
            arm = arm[:, :-expand_center]
            arm_pts[0][0] -= expand_center
            arm_pts[-1][0] -= expand_center
        if patt:
            if colors is None:
                arm = add_patt(arm, seq[i])
            else:
                arm = add_patt(arm, seq[i], colors[i], cont=cont, arr=arr,
                               outline=outline)
        temp_maze, temp_pts = place_arm(arm, arm_pts, arm_bg, field,
                                        (0, int(im_half-(w/2+outer))), bg=bg)
        temp_maze, temp_pts = rotate_arm(temp_maze, temp_pts, -i*360/num,
                                         (im_half, im_half))
        maze += temp_maze
        maze = maze.clip(0, 1)
        maze_pts.append(temp_pts)

    maze = draw_center(maze, maze_pts, True, inter=inter, cont=cont, col=c_col)

    w, h, c = maze.shape
    m_cont = np.zeros([h, w+2, 3])
    m_cont[:, 2:] += maze
    m_cont[:, :2] = 0
    m_cont = (m_cont * 255).copy().astype(np.uint8)
    e, m_cont = cv2.threshold(m_cont, 10, 255, cv2.THRESH_BINARY)
    cont_draw, cont, hier = cv2.findContours(m_cont[:, :, 0].astype(np.uint8),
                                             cv2.RETR_TREE,
                                             cv2.CHAIN_APPROX_SIMPLE)

    epsilon = 0.00025 * cv2.arcLength(cont[0], True)
    # a check that contour is correct
    m = cv2.drawContours(m_cont, cont, 0, 255)

    # bg = np.zeros_like(m_cont)
    cont1 = cv2.approxPolyDP(cont[0], epsilon, True)

    return maze, cont1, maze_pts


def write_svg(m_cont, name, size, scale):
    """
    scale -- number of pixels in a mm
    """
    import svgwrite

    size = size/scale*7
    maze = svgwrite.Drawing(filename=name,
                            size=("{}mm".format(size),
                                  "{}mm".format(size)))
    maze.viewbox(width=size, height=size)

    for cont in m_cont:
        cont = cont/float(scale)

        # for some reason need an offset on y axis of 178
        cont[:, 1] += size + 178
        d = [coord2command(cont[0], 'M')]

        for i in range(1, cont.shape[0]):
            d.append(coord2command(cont[i], 'L'))
        d.append('z')
        ln = svgwrite.path.Path(d=d)
        ln.stroke('black', 0.1)
        ln.fill("none")
        maze.add(ln)

    maze.save()


def coord2command(co, command):
    return command, co[0], co[1]


def create_arm(w, l, e_ratio=3, outer=0, bg=(0, 0, 0)):
    """ create one arm of a maze with rounded end.
    it can later be moved to a necessary position outside of this function

    size -- arm width in pixels
    ratio -- width to length of the arm
    e_ratio -- width to ellipse's smaller radius
    outer -- nonzero if an outer border of the maze is created, a distance
             from inner border to outer

    returns arm -- array with black for arm, white for background
    returns arm_pts -- array with 5 points around the arm
    """
    e = int(w/e_ratio + outer)
    arm = np.zeros([w + outer*2, l+outer, 3])
    arm_bg = np.ones_like(arm) * bg
    cv2.ellipse(arm, (e, int(w/2 + outer)), (e, int(w/2 + outer)),
                0, 90, 270, (1, 1, 1), -1)
    cv2.rectangle(arm, (e, 0), (l+outer, w + outer*2), (1, 1, 1), -1)
    arm_pts = np.array([[l+outer, w+outer*2], [0, w+outer*2], [0, w/2+outer],
                        [0, 0], [l+outer, 0]])

    cv2.ellipse(arm_bg, (e, int(w/2 + outer)), (e, int(w/2 + outer)),
                0, 90, 270, (0, 0, 0), -1)
    cv2.rectangle(arm_bg, (e, 0), (l+outer, w + outer*2), (0, 0, 0), -1)

    return arm, arm_pts, arm_bg


def arm_area(w, l, e_ratio=3):
    """ area of an arm
    Arm is composed of a rectangle of width w and length (l-e),
    and adjacent half-ellipse of radii w/2 and e
    """
    e = w/e_ratio
    rect_a = w*(l-e)
    elli_a = np.pi*w/2*e

    return rect_a + 1/2*elli_a


def add_patt(arm, p, col=None, cont=False, arr=False, outline=False):
    """ add patten as background of an arm

    arm -- array with white for arm area and black for background
    p -- a symbol corresponding to a pattern"""

    patt_dict = {'h': hlines,
                 'd': dots,
                 'i': inv_dots,
                 'v': vlines,
                 'x': xadrez,
                 'c': color}
    alpha = 0.5
    if p == 'x':
        a, b = 8, 8
    elif p == 'i':
        a, b = 3, 3
    else:
        a, b = 4, 4

    w = arm.shape[0]

    if cont or outline:
        _, cont_arm, hier = cv2.findContours(arm[:, :, 0].astype(np.uint8),
                                             cv2.RETR_TREE,
                                             cv2.CHAIN_APPROX_SIMPLE)
    patt = show_patt(w, w, a, b, alpha,
                     p, patt_dict[p], col=col, show=False)
    im = patt.copy()
    while im.shape[0] < arm.shape[0]:
        im = np.append(im, patt, axis=0)
    while im.shape[1] < arm.shape[1]:
        im = np.append(im, patt, axis=1)
    arm *= im[im.shape[0]-arm.shape[0]:, im.shape[1]-arm.shape[1]:]
    arm = arm.clip(0, 1)
    if cont:
        cv2.drawContours(arm, cont_arm, 0, col, 300)
    if outline:
        cv2.drawContours(arm, cont_arm, 0, (0, 0, 0), 2)
    if arr:
        tail = 0.1
        thick = 0.13
        length = 0.55
        tip = 0.35
        cv2.fillConvexPoly(arm, np.array([[w, (0.5-thick)*w],
            [w, (0.5+thick)*w], [(1-length+tip)*w, (0.5+thick)*w],
            [(1-length+tip)*w, (0.5+thick+tail)*w], [(1-length)*w, 0.5*w],
            [(1-length+tip)*w, (0.5-thick-tail)*w],
            [(1-length+tip)*w, (0.5-thick)*w]]).astype(int), col)
        cv2.polylines(arm, np.array([[[w, (0.5-thick)*w],
            [w, (0.5+thick)*w], [(1-length+tip)*w, (0.5+thick)*w],
            [(1-length+tip)*w, (0.5+thick+tail)*w], [(1-length)*w, 0.5*w],
            [(1-length+tip)*w, (0.5-thick-tail)*w],
            [(1-length+tip)*w, (0.5-thick)*w]]]).astype(int), True, (1, 1, 1),
                      40)
    return arm


def place_arm(template, temp_pts, temp_bg, s, co, bg=0):
    """ place arm template on a template maze

    template -- template arm
    s -- size of the template maze image
    co -- new position of arm's left upper corner
    """
    temp_maze = np.zeros([s, s, 3])
    w, h, c = template.shape
    temp_maze[co[1]:co[1]+w, co[0]:co[0]+h, :] += template - bg + temp_bg
    temp_pts[:, 0] += co[0]
    temp_pts[:, 1] += co[1]

    return temp_maze, temp_pts


def rotate_arm(template, temp_pts, ang, co):
    """ rotate arm template

    template -- maze template with arm in it
    ang -- angle of rotation
    co -- axis of rotation
    """
    w, h, c = template.shape
    M = cv2.getRotationMatrix2D(co, ang, 1)

    new_pts = temp_pts.copy().astype(np.float32)
    new_pts[:, 0] = temp_pts[:, 0]*M[0, 0] + temp_pts[:, 1]*M[0, 1] + M[0, 2]
    new_pts[:, 1] = temp_pts[:, 0]*M[1, 0] + temp_pts[:, 1]*M[1, 1] + M[1, 2]

    return cv2.warpAffine(template, M, (w, h), borderValue=0), new_pts


def center_dist(w, s):
    """ calculate distance from an arm to maze center

    w -- width of the arm
    s -- area of the center

    Originally s was planned to be half of the area of the arm.
    Center is a hexagon, formed from an equilateral triangle by cutting off
    vertices (3 edges are of size w, and 3 of size x);
    3 sides
    """
    x = -2*w + np.sqrt(3*w**2 + 4*s/np.sqrt(3))  # first we find edge x of 6gon
    if x < 0:
        print("width:length ratio allows only for triangular center")
        return w*np.sqrt(3)/6
    y = np.sqrt(x**2 + w**2 + x*w)  # edge of eq-lat 3angle, fitted into 6gon
    r = y/np.sqrt(3)  # radius of circumcircle around 6gon
    d = np.sqrt(r**2 - (w/2)**2)  # dist from 6gon center to w edge

    return d


def draw_center(maze, maze_pts, fill=True, inter=False, cont=False,
                col=(0.53, 0.53, 0.53)):
    """ draw center of the maze
    """
    cntr = np.round(central_points(maze_pts, inter)).astype(int)
    if fill:
        cv2.fillConvexPoly(maze, cntr, col)
    else:
        # broken in inter=True
        cv2.line(maze, tuple(cntr[1, :]), tuple(cntr[2, :]), 0)
        cv2.line(maze, tuple(cntr[3, :]), tuple(cntr[4, :]), 0)
        cv2.line(maze, tuple(cntr[5, :]), tuple(cntr[0, :]), 0)
    if cont:
        cv2.polylines(maze, [cntr], True, (0.25, 0.25, 0.25), 50)
    return maze


def draw_shock(s, active=True):
    bg_shock = np.ones([int(s*2.2), s, 3]) * 0
    coords = np.array([[2.5, 0], [0.5, 0.5], [1.5, 3], [1, 3.25], [1.8, 5.1],
                       [1.5, 5.25], [2.5, 8], [2.5, 5.25], [3, 5], [2.5, 3.25],
                       [3, 3]]) + [0, 0.5]
    if active:
        col = (1, 0, 0)
    else:
        col = (0.5, 0.5, 0.5)
    scale = s/4
    cv2.fillConvexPoly(bg_shock, (coords*scale).astype(int), col)
    cv2.polylines(bg_shock, [(coords * scale).astype(np.int32)], True,
                  (1, 1, 1), 20)
    shock_mask = np.ones([int(s*2.2), s, 3])
    cv2.fillConvexPoly(shock_mask, (coords * scale).astype(int), (0, 0, 0))
    cv2.polylines(shock_mask, [(coords * scale).astype(np.int32)], True,
                  (255, 255, 255), 20)
    return bg_shock, shock_mask


def add_shock(maze, shock, shock_mask, pos):
    h, w = int(shock.shape[0]), int(shock.shape[1])
    pos[1] = (pos[1] + 0.1*h).astype(int)
    maze[max(0, pos[1]-h):pos[1], pos[0]-int(w/2):pos[0]-int(w/2)+w] *= \
        shock_mask[h - min(h, pos[1]):, :, :]
    maze[max(0, pos[1]-h):pos[1], pos[0]-int(w/2):pos[0]-int(w/2)+w] += \
        shock[h - min(h, pos[1]):, :, :]
    return maze


def central_points(maze_pts, inter=False):
    """ find contours of maze center

    maze_pts -- list, each element are 5 points of an arm
    if arms are intersecting, returns points of only inner part
    """
    if inter:
        maze_lines = []
        for i in range(len(maze_pts)):
            mp = maze_pts[i]
            if mp[0,0]-mp[4,0] == 0:
                a = 90
                b = mp[0,0]
            else:
                a = np.arctan((mp[0,1]-mp[4,1])/(mp[0,0]-mp[4,0]))*180/np.pi
                b = (mp[4,1]*mp[0,0]-mp[0,1]*mp[4,0])/(mp[0,0]-mp[4,0])
            if abs(abs(a)-90) < 0.5:
                a = 90
                b = mp[0, 0]
            maze_lines.append((a, b))
        ctr_pts = []
        for i in range(len(maze_lines)):
            a1, b1 = maze_lines[i]
            if i == len(maze_lines)-1:
                j = 0
            else:
                j = i+1
            a2, b2 = maze_lines[j]
            if a1 == 90:
                x = b1
                y = np.tan(a2*np.pi/180.0)*x+b2
            elif a2 == 90:
                x = b2
                y = np.tan(a1*np.pi/180.0)*x+b1
            else:
                x = (b1-b2)/(np.tan(a2*np.pi/180.0)-np.tan(a1*np.pi/180.0))
                y = np.tan(a1*np.pi/180.0)*x+b1
            ctr_pts.append((x, y))
        ctr_pts = np.array(ctr_pts)
    else:
        ctr_pts = np.zeros([6, 2])
        for i in range(len(maze_pts)):
          ctr_pts[2*i, :] = maze_pts[i][0, :]
          ctr_pts[2*i+1, :] = maze_pts[i][4, :]

    return ctr_pts


if __name__ == '__main__':
    sz = 360  # big
    show_patt(sz, sz, 4, 4, 0.5, 'd', dots, col=None, save=True, show=False)

    seq = 'ivx'
    clrs = np.array([(19, 37, 212), (0, 165, 255), (80, 208, 146)])/255
    clrs = np.array([(80, 208, 146), (80, 208, 146), (19, 37, 212)])/255
    clrs = np.array([(218, 190, 135), (139, 172, 245), (244, 245, 247)])/255
    c_col = np.array([224]*3)/255
    seq = 'ccc'
    maze, _, maze_pts = create_rounded_maze(sz, ratio=(1, 1), patt=True,
                                            seq=seq, outer=0, inter=False,
                                            e_ratio=5, expand_center=0,
                                            cont=False, colors=clrs,
                                            c_col=c_col, outline=True,
                                            bg=(1, 1, 1), arr=False)
    shock, shock_mask = draw_shock(int(sz / 2), True)
