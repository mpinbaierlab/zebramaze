import numpy as np
import pandas as pd

import matplotlib.pyplot as plt
from PIL import Image

import seaborn as sns

import zebramaze.md as md
import zebramaze.analysis as als
import zebramaze.plotting as aplt
import zebramaze.figure_4 as f4

from scipy.stats import mannwhitneyu


def figure_5(dpi=600):
    fig = plt.figure(figsize=[8, 11])

    # plot first half of the figure for xp with pattern replacement
    strategy_names = ['Prefers\ncenter', 'Prefers\npattern',
                      'Prefers\nlocation']
    session_names = ['Conditioning\nlast 5\'',
                     'Test (after pattern swap)\nfirst 5\'']
    arm_names = [['Conditioned arm', 'Preferred safe\narm/pattern',
                  'Other safe arm', 'Center'],
                 ['Conditioned arm', 'Preferred safe\narm',
                  'Preferred safe\npattern', 'Center']]
    ss_list = [md.ssa.loc[md.ssa.xp_id == x] for x in
               ['170118-05-01', '100118-03-01', '110118-05-01']]
    seq_list = ['vxi', 'vix']
    ss_swap = md.ssa_swap
    f4.heat_figure(fig, ss_list, strategy_names, arm_names, session_names,
                   ss_swap, seq_list, push_high_shocked=True,
                   x0=0., x1=1., y0=0.5, y1=1.0, invert=True)

    # plot second half of the figure for xp with pattern swap
    strategy_names = ['Prefers\ncenter', 'Ignores\nrotation',
                      'Follows\nrotation', 'Ignores\nrotation']
    session_names = ['Conditioning\nlast 5\'',
                     'Test (after rotation)\nfirst 5\'']
    arm_names = [['Conditioned\narm/pattern', 'Preferred safe\narm/pattern',
                  'Other safe arm', 'Center'],
                 ['Conditioned pattern\n(preferred safe arm)',
                  'Conditioned arm', 'Preferred safe\npattern', 'Center']]
    ss_list = [md.ssa.loc[md.ssa.xp_id == x] for x in
               ['160917-02-01', '130817-10-01', '240617-08-01',
                '290917-08-01']]
    seq_list = ['ivx', 'xiv']
    ss_turn = md.ssa_turn[md.xp_new]
    conflict = []  # smallest chance of confusion?

    for g, gg in ss_turn.groupby('xp_id'):
        co_1 = pd.read_hdf(gg.iloc[1].ss_coords_path)
        co_1 = co_1.iloc[-18000:]
        all_arms = [0, 1, 2]
        all_arms.remove(gg.iloc[1].stim_at)
        co_1_0 = (co_1.armfix == all_arms[0]).sum()
        co_1_1 = (co_1.armfix == all_arms[1]).sum()
        if co_1_0 > co_1_1:
            pref_arm = all_arms[0]
        else:
            pref_arm = all_arms[1]
        if gg.iloc[2].stim_at == pref_arm:
            conflict.append(g)

    ss_turn = ss_turn.loc[ss_turn.xp_id.isin(conflict)]
    f4.heat_figure(fig, ss_list, strategy_names, arm_names, session_names,
                   ss_turn, seq_list, push_high_shocked=False, invert=True,
                   x0=0., x1=1., y0=0.0, y1=0.5)

    # label the panels
    panel_font = 15
    panel_top = 0.98
    panel_bot = 0.48
    fig.text(0.12, panel_top, 'A', fontsize=panel_font)
    fig.text(0.33, panel_top, 'B', fontsize=panel_font)
    fig.text(0.6, panel_top, 'C', fontsize=panel_font)
    fig.text(0.12, panel_bot, 'D', fontsize=panel_font)
    fig.text(0.33, panel_bot, 'E', fontsize=panel_font)
    fig.text(0.6, panel_bot, 'F', fontsize=panel_font)
    fig.savefig('../figures/figure_6.png', dpi=dpi)

    im = Image.open('../figures/figure_6.png')
    im.save('../figures/figure_6.tiff', compression='tiff_lzw')


def figure_5s1(dpi=100):
    cf = aplt.CF['BORDERS']
    fig = plt.figure(figsize=[8, 10])
    schema_bottom = 0.9
    schema_h = 1-cf['t_margin']-schema_bottom

    half_h = (1-cf['t_margin']-cf['b_margin']-2*cf['gap_h'])/2
    slide_h = (half_h-schema_h-cf['gap_m']-cf['gap_h'])/2
    box_h = (half_h-4*cf['gap_h'])/2
    slide_w = (1 - cf['r_margin']-cf['l_margin']-5*cf['gap_w'])*0.4
    box_w = 1-cf['r_margin']-slide_w-cf['l_margin']-cf['gap_w']*6

    sss = [md.ssa_replace, md.ssa_swap]
    ss_list = []
    for ss_sub in sss:

        ft = als.fish_types(ss_sub)
        over = ft['Overstayer']
        center = ft['Center']
        one_arm = ft['One arm']
        two_arms = ft['Two arms']

        print("{} fish with {} overstayers".format(
            int(ss_sub.shape[0] / len(ss_sub.ss_no.unique())), len(over)))
        ss_list.append(ss_sub.loc[ss_sub.xp_id.isin(
            one_arm+two_arms+center+ft['Non-responder'])])

    seq = ['vxi', 'vxi']
    for i, ss in enumerate(ss_list):
        left_i = cf['l_margin'] + cf['gap_w']
        bottom_i = cf['b_margin']+(len(ss_list)-i-1) * (half_h + 2*cf['gap_h'])

        if i == 0:
            rep = True
            fl = False
        else:
            rep = False
            fl = True
        aplt.plot_protocol(fig, '', seq[i], left_i,
                           bottom_i+slide_h*2+cf['gap_h']+cf['gap_m'],
                           slide_w + 2*cf['gap_w'], schema_h,
                           replace=rep, flip=fl, cont=False)
        ax1 = fig.add_axes([left_i+cf['gap_w'], bottom_i+slide_h+cf['gap_m'],
                            slide_w, slide_h])
        ax2 = fig.add_axes([left_i+cf['gap_w'], bottom_i, slide_w, slide_h])
        ax3 = fig.add_axes([left_i+slide_w+5*cf['gap_w'],
                            bottom_i+box_h+2*cf['gap_m']+cf['gap_h'],
                            box_w, box_h])
        ax4 = fig.add_axes([left_i+slide_w+5*cf['gap_w'], bottom_i,
                            box_w, box_h])

        avg_e, avg_sum_e = als.moving_avg(ss, als.entry,
                                          exclude_overstayers=False,
                                          **{'win_size': 300,
                                              'sliding_overlap': 270})
        avg_o, avg_sum_o = als.moving_avg(ss, als.occupancy,
                                          exclude_overstayers=False,
                                          **{'val': [0, 1, 2, 3],
                                             'win_size': 300,
                                             'sliding_overlap': 270
                                             })

        aplt.plot_effects(ss, avg_e, 'EF score', avg_sum=avg_sum_e,
                          show=False, ax=ax1,
                          se=True, **{'val': [0, 1, 2], 'win_size': 300,
                                              'sliding_overlap': 270})
        aplt.plot_effects(ss, avg_o, 'OC score', show=False, ax=ax2,
                          se=True, **{'val': [0, 1, 2, 3], 'win_size': 300,
                                              'sliding_overlap': 270})
        ax1.set_xticks([0, 30, 90, 120])
        ax2.set_xticks([0, 30, 90, 120])
        ax1.set_xlabel('')
        ax1.set_xticklabels([])

        ax1.text(1, 1,
                 'n = {}'.format(int(ss.shape[0] / len(ss.ss_no.unique()))),
                 transform=ax1.transAxes, va='bottom', ha='right')

        pts = [49, 169, 180]

        aplt.plot_single_bar(ss, avg_e, avg_sum=avg_sum_e, func=als.entry,
                             ax=ax3, pts=pts)
        aplt.plot_single_bar(ss, avg_o, func=als.occupancy, ax=ax4, pts=pts)
        ax3.set_xlabel('')
        ax4.set_xlabel('')

    top_panel = 0.92
    left_panel = 0.04
    panel_font = 15

    fig.text(left_panel, top_panel, 'A', fontsize=panel_font)
    fig.text(left_panel, 0.83, 'B', fontsize=panel_font)
    fig.text(0.48, top_panel, 'C', fontsize=panel_font)
    fig.text(left_panel, 0.45, 'D', fontsize=panel_font)
    fig.text(left_panel, 0.37, 'E', fontsize=panel_font)
    fig.text(0.48, 0.45, 'F', fontsize=panel_font)

    fig.savefig('../figures/figure_s2.png', dpi=dpi)


def figure_s4(dpi=100):

    f_w, f_h = 8, 5
    scale = 10/f_h
    fig_suppl = plt.figure(figsize=(f_w, f_h))
    sss = [md.ssa_swap]

    ss_list = []
    for ss_sub in sss:
        ft = als.fish_types(ss_sub)
        over = ft['Overstayer']
        center = ft['Center']
        one_arm = ft['One arm']
        two_arms = ft['Two arms']

        print("{} fish with {} overstayers".format(
            int(ss_sub.shape[0] / len(ss_sub.ss_no.unique())), len(over)))
        ss_list.append(ss_sub.loc[ss_sub.xp_id.isin(
            one_arm + two_arms + center + ft['Non-responder'])])

    cf = aplt.CF['BORDERS']

    schema_h = 0.13

    half_h = 1-(cf['t_margin']+cf['b_margin'])*scale
    slide_h = (half_h-schema_h-scale*(cf['gap_h']+cf['gap_m']))/2
    slide_w = (1 - cf['r_margin']-cf['l_margin']-cf['gap_w']*6)*0.4
    box_w = (1-cf['r_margin']-slide_w-cf['l_margin']-6*cf['gap_w'])

    seq = ['vxi']
    names = ['']
    # names = ['Control for the change\nto a neutral pattern']
    for i, ss in enumerate(ss_list):
        left_i = 2*cf['gap_w'] + cf['l_margin']

        aplt.plot_protocol(fig_suppl, names[i], seq[i], left_i - cf['gap_w'],
                           (cf['b_margin']+cf['gap_h']+cf['gap_m'])*scale +
                           2*slide_h,
                           slide_w + 2*cf['gap_w'], schema_h,
                           flip=True, cont=False)

        ax1 = fig_suppl.add_axes([left_i, scale*(cf['b_margin']+cf[
            'gap_m'])+slide_h, slide_w, slide_h])
        ax2 = fig_suppl.add_axes([left_i, cf['b_margin']*scale, slide_w,
                                  slide_h])
        ax3 = fig_suppl.add_axes([left_i+slide_w+4*cf['gap_w'],
                                  scale*(cf['b_margin']+2*cf['gap_m'])+slide_h,
                                  box_w, slide_h])
        ax4 = fig_suppl.add_axes([left_i + slide_w + 4*cf['gap_w'],
                                  cf['b_margin']*scale, box_w, slide_h])

        avg_e, avg_sum_e = als.moving_avg(ss, als.entry,
                                          exclude_overstayers=False,
                                          **{'win_size': 300,
                                              'sliding_overlap': 270})
        avg_o, avg_sum_o = als.moving_avg(ss, als.occupancy,
                                          exclude_overstayers=False,
                                          **{'val': [0, 1, 2, 3], 'win_size':
                                              300,
                                              'sliding_overlap': 270})

        aplt.plot_effects(ss, avg_e, 'EF score', avg_sum=avg_sum_e,
                          show=False, ax=ax1,
                          se=True, **{'val': [0, 1, 2], 'win_size': 300,
                                              'sliding_overlap': 270})
        aplt.plot_effects(ss, avg_o, 'OC score', show=False, ax=ax2,
                          se=True, **{'val': [0, 1, 2, 3], 'win_size': 300,
                                              'sliding_overlap': 270})
        ax1.set_xticks([0, 30, 90, 120])
        ax2.set_xticks([0, 30, 90, 120])
        ax1.set_xlabel('')
        ax1.set_xticklabels([])

        ax1.text(1, 1,
                 'n = {}'.format(int(ss.shape[0] / len(ss.ss_no.unique()))),
                 transform=ax1.transAxes, va='bottom', ha='right')

        pts = [49, 169, 180]

        aplt.plot_single_bar(ss, avg_e, avg_sum=avg_sum_e, func=als.entry,
                             ax=ax3, pts=pts)
        aplt.plot_single_bar(ss, avg_o, func=als.occupancy, ax=ax4, pts=pts)
        ax3.set_xlabel('')
        ax3.set_xticklabels('')
        ax4.set_xlabel('')

    top_panel = 0.95
    left_panel = 0.04
    panel_font = 15

    fig_suppl.text(left_panel, top_panel, 'A', fontsize=panel_font)
    fig_suppl.text(left_panel, 0.75, 'B', fontsize=panel_font)
    fig_suppl.text(0.46, 0.77, 'C', fontsize=panel_font)

    fig_suppl.savefig('../figures/figure_s4.png', dpi=dpi)


def figure_s2(dpi=100):

    f_w, f_h = 8, 5
    scale = 10/f_h
    fig_suppl = plt.figure(figsize=(f_w, f_h))
    sss = [md.ssa_replace]

    ss_list = []
    for ss_sub in sss:
        ft = als.fish_types(ss_sub)
        over = ft['Overstayer']
        center = ft['Center']
        one_arm = ft['One arm']
        two_arms = ft['Two arms']

        print("{} fish with {} overstayers".format(
            int(ss_sub.shape[0] / len(ss_sub.ss_no.unique())), len(over)))
        ss_list.append(ss_sub.loc[ss_sub.xp_id.isin(
            one_arm + two_arms + center + ft['Non-responder'])])

    cf = aplt.CF['BORDERS']

    schema_h = 0.13

    half_h = 1-(cf['t_margin']+cf['b_margin'])*scale
    slide_h = (half_h-schema_h-scale*(cf['gap_h']+cf['gap_m']))/2
    slide_w = (1 - cf['r_margin']-cf['l_margin']-cf['gap_w']*6)*0.4
    box_w = (1-cf['r_margin']-slide_w-cf['l_margin']-6*cf['gap_w'])

    seq = ['vxi']
    names = ['']
    # names = ['Control for the change\nto a neutral pattern']
    for i, ss in enumerate(ss_list):
        left_i = 2*cf['gap_w'] + cf['l_margin']

        aplt.plot_protocol(fig_suppl, names[i], seq[i], left_i - cf['gap_w'],
                           (cf['b_margin']+cf['gap_h']+cf['gap_m'])*scale +
                           2*slide_h,
                           slide_w + 2*cf['gap_w'], schema_h,
                           replace=True, cont=False)

        ax1 = fig_suppl.add_axes([left_i, scale*(cf['b_margin']+cf[
            'gap_m'])+slide_h, slide_w, slide_h])
        ax2 = fig_suppl.add_axes([left_i, cf['b_margin']*scale, slide_w,
                                  slide_h])
        ax3 = fig_suppl.add_axes([left_i+slide_w+4*cf['gap_w'],
                                  scale*(cf['b_margin']+2*cf['gap_m'])+slide_h,
                                  box_w, slide_h])
        ax4 = fig_suppl.add_axes([left_i + slide_w + 4*cf['gap_w'],
                                  cf['b_margin']*scale, box_w, slide_h])

        avg_e, avg_sum_e = als.moving_avg(ss, als.entry,
                                          exclude_overstayers=False,
                                          **{'win_size': 300,
                                              'sliding_overlap': 270})
        avg_o, avg_sum_o = als.moving_avg(ss, als.occupancy,
                                          exclude_overstayers=False,
                                          **{'val': [0, 1, 2, 3], 'win_size':
                                              300,
                                              'sliding_overlap': 270})

        aplt.plot_effects(ss, avg_e, 'EF score', avg_sum=avg_sum_e,
                          show=False, ax=ax1,
                          se=True, **{'val': [0, 1, 2], 'win_size': 300,
                                              'sliding_overlap': 270})
        aplt.plot_effects(ss, avg_o, 'OC score', show=False, ax=ax2,
                          se=True, **{'val': [0, 1, 2, 3], 'win_size': 300,
                                              'sliding_overlap': 270})
        ax1.set_xticks([0, 30, 90, 120])
        ax2.set_xticks([0, 30, 90, 120])
        ax1.set_xlabel('')
        ax1.set_xticklabels([])

        ax1.text(1, 1,
                 'n = {}'.format(int(ss.shape[0] / len(ss.ss_no.unique()))),
                 transform=ax1.transAxes, va='bottom', ha='right')

        pts = [49, 169, 180]

        aplt.plot_single_bar(ss, avg_e, avg_sum=avg_sum_e, func=als.entry,
                             ax=ax3, pts=pts)
        aplt.plot_single_bar(ss, avg_o, func=als.occupancy, ax=ax4, pts=pts
                             )
        ax3.set_xlabel('')
        ax3.set_xticklabels('')
        ax4.set_xlabel('')

    top_panel = 0.95
    left_panel = 0.04
    panel_font = 15

    fig_suppl.text(left_panel, top_panel, 'A', fontsize=panel_font)
    fig_suppl.text(left_panel, 0.75, 'B', fontsize=panel_font)
    fig_suppl.text(0.46, 0.77, 'C', fontsize=panel_font)

    fig_suppl.savefig('../figures/figure_s2.png', dpi=dpi)


def figure_5s2(dpi=100):

    f_w, f_h = 8, 5.5
    scale = 11/f_h
    fig_suppl = plt.figure(figsize=(f_w, f_h))
    ss_list = [md.ssa_replace_c]
    cf = aplt.CF['BORDERS']

    schema_h = 0.13

    half_h = 1-(cf['t_margin']+cf['b_margin'])*scale
    slide_h = (half_h-schema_h-scale*(cf['gap_h']+cf['gap_m']))/2
    slide_w = (1 - cf['r_margin']-cf['l_margin']-cf['gap_w']*6)*0.5
    box_w = (1-cf['r_margin']-slide_w-cf['l_margin']-6*cf['gap_w'])

    seq = ['vxi']
    names = ['']
    # names = ['Control for the change\nto a neutral pattern']
    for i, ss in enumerate(ss_list):
        ss = ss.loc[ss.ss_type != 'CONDIT']
        left_i = 2*cf['gap_w'] + cf['l_margin']

        aplt.plot_protocol(fig_suppl, names[i], seq[i], left_i - cf['gap_w'],
                           (cf['b_margin']+cf['gap_h']+cf['gap_m'])*scale +
                           2*slide_h,
                           slide_w + 2*cf['gap_w'], schema_h, replace=True,
                           dots_control=True, cont=False)

        ax1 = fig_suppl.add_axes([left_i, scale*(cf['b_margin']+cf[
            'gap_m'])+slide_h, slide_w, slide_h])
        ax2 = fig_suppl.add_axes([left_i, cf['b_margin']*scale, slide_w,
                                  slide_h])
        ax3 = fig_suppl.add_axes([left_i+slide_w+4*cf['gap_w'],
                                  scale*(cf['b_margin']+cf['gap_m'])+slide_h,
                                  box_w, slide_h])
        ax4 = fig_suppl.add_axes([left_i + slide_w + 4*cf['gap_w'],
                                  cf['b_margin']*scale, box_w, slide_h])

        avg_e, avg_sum_e = als.moving_avg(ss, als.entry,
                                          exclude_overstayers=False,
                                          **{'win_size': 300,
                                              'sliding_overlap': 270})
        avg_o, avg_sum_o = als.moving_avg(ss, als.occupancy,
                                          exclude_overstayers=False,
                                          **{'val': [0, 1, 2, 3], 'win_size':
                                              300,
                                              'sliding_overlap': 270})

        aplt.plot_effects(ss, avg_e, 'EF score', avg_sum=avg_sum_e,
                          show=False, ax=ax1,
                          se=True, **{'val': [0, 1, 2], 'win_size': 300,
                                              'sliding_overlap': 270})
        aplt.plot_effects(ss, avg_o, 'OC score', show=False, ax=ax2,
                          se=True, **{'val': [0, 1, 2, 3], 'win_size': 300,
                                              'sliding_overlap': 270})
        ax1.set_xticks([0, 30, 60])
        ax2.set_xticks([0, 30, 60])

        ax1.set_xlabel('')
        ax1.set_xticklabels([])

        ax1.text(1, 1,
                 'n = {}'.format(int(ss.shape[0] / len(ss.ss_no.unique()))),
                 transform=ax1.transAxes, va='bottom', ha='right')

        e_ind = avg_e[0] - (avg_e[1]+avg_e[2])/2
        o_ind = avg_o[0] - (avg_o[1] + avg_o[2]) / 2

        u_e, pi_e = mannwhitneyu(e_ind.iloc[49], e_ind.iloc[60],
                                 alternative='two-sided')
        u_o, pi_o = mannwhitneyu(o_ind.iloc[49], o_ind.iloc[60],
                                 alternative='two-sided')

        ax3.set_ylim(-0.55, 1.05)
        sns.swarmplot(data=pd.DataFrame({'habituation\nlast 5\'': e_ind.iloc[
            49],
                                         'test\nfirst 5\'': e_ind.iloc[60]}),
                      palette=['#444444'], ax=ax3, size=3)
        ax3.hlines([e_ind.iloc[49].mean(), e_ind.iloc[60].mean()],
                   [-0.25, 0.75], [0.25, 1.25], color='k', lw=2)
        ax3.set_ylabel('EF score')
        ax3.set_xticklabels([''])
        lvl = 1
        ax3.plot([0, 0, 1, 1], [-0.02 + lvl, lvl, lvl, -0.02 + lvl],
                 lw=1, c=(0.25, 0.25, 0.25), clip_on=False)
        ax3.text(0.5, lvl+0.02, 'p = {:.02f}'.format(pi_e),
                 ha='center', va='bottom', )
        ax3.set_yticks([-0.5, 0, 0.5, 1])

        ax4.set_ylim(-0.55, 1.05)
        sns.swarmplot(data=pd.DataFrame({'habituation\nlast 5\'': o_ind.iloc[49],
                                         'test\nfirst 5\'': o_ind.iloc[60]}),
                      palette=['#444444'], ax=ax4, size=3)
        ax4.hlines([o_ind.iloc[49].mean(), o_ind.iloc[60].mean()],
                   [-0.25, 0.75], [0.25, 1.25], color='k', lw=2)

        ax4.set_ylabel('OC score')
        ax4.plot([0, 0, 1, 1], [-0.02 + lvl, lvl, lvl, -0.02 + lvl],
                 lw=1, c=(0.25, 0.25, 0.25), clip_on=False)
        ax4.text(0.5, lvl+0.02, 'p = {:.02f}'.format(pi_o),
                 ha='center', va='bottom')
        ax4.set_yticks([-0.5, 0, 0.5, 1])

    top_panel = 0.95
    left_panel = 0.04
    panel_font = 15

    fig_suppl.text(left_panel, top_panel, 'A', fontsize=panel_font)
    fig_suppl.text(left_panel, 0.75, 'B', fontsize=panel_font)
    fig_suppl.text(0.53, 0.75, 'C', fontsize=panel_font)

    fig_suppl.savefig('../figures/figure_s3.png', dpi=dpi)
    im = Image.open('../figures/figure_s3.png'.format(5))
    im.save('../figures/figure_s3.tiff'.format(5), compression='tiff_lzw')


if __name__ == '__main__':
    # figure_5(dpi=300)
    # figure_5s(dpi=600)
    figure_5s2(dpi=300)
    # figure_s2(dpi=300)
