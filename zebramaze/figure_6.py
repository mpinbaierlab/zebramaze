import pandas as pd
import matplotlib.pyplot as plt

import zebramaze.md as md
import zebramaze.figure_4 as f4
import zebramaze.plotting as aplt
import zebramaze.analysis as als


def figure_6(dpi=100):
    strategy_names = ['Does not\navoid', 'Ignores\nrotation',
                      'Follows\nrotation', 'Ignores\nrotation']
    session_names = ['Conditioning', 'Test (after rotation)']
    arm_names = [['Conditioned\narm/pattern', 'Preferred safe\narm/pattern',
                  'Other safe arm', 'Center'],
                 ['Conditioned pattern\n(preferred safe arm)',
                  'Conditioned arm', 'Preferred safe\npattern', 'Center']]
    ss_list = [md.ssa.loc[md.ssa.xp_id == x] for x in
               ['200617-06-01', '130817-10-01', '240617-08-01',
                '290917-08-01']]
    seq_list = ['ivx', 'xiv']
    ss_turn = md.ssa_turn[md.xp_new]
    conflict = []  # smallest chance of confusion?

    for g, gg in ss_turn.groupby('xp_id'):
        co_1 = pd.read_hdf(gg.iloc[1].ss_coords_path)
        co_1 = co_1.iloc[-18000:]
        all_arms = [0, 1, 2]
        all_arms.remove(gg.iloc[1].stim_at)
        co_1_0 = (co_1.armfix == all_arms[0]).sum()
        co_1_1 = (co_1.armfix == all_arms[1]).sum()
        if co_1_0 > co_1_1:
            pref_arm = all_arms[0]
        else:
            pref_arm = all_arms[1]
        if gg.iloc[2].stim_at == pref_arm:
            conflict.append(g)

    ss_turn = ss_turn.loc[ss_turn.xp_id.isin(conflict)]
    fig = plt.figure(figsize=[8, 6])

    f4.heat_figure(fig, ss_list, strategy_names, arm_names, session_names, ss_turn,
                   seq_list, push_high_shocked=False, invert=True)
    panel_font = 15
    panel_top = 0.93
    fig.text(0.13, panel_top, 'A', fontsize=panel_font)
    fig.text(0.35, panel_top, 'B', fontsize=panel_font)
    fig.text(0.63, panel_top, 'C', fontsize=panel_font)

    fig.savefig('../figures/figure_6.png', dpi=600)


def figure_6s(dpi=100):
    cf = aplt.CF['BORDERS']
    fig = plt.figure(figsize=[8, 10])

    half_h = (1-cf['t_margin']-cf['b_margin']-2*cf['gap_h'])/2
    slide_h = (half_h-3*cf['gap_h'])/2
    slide_w = (1 - cf['r_margin']-cf['l_margin']-5*cf['gap_w'])*0.4
    box_w = 1-cf['r_margin']-slide_w-cf['l_margin']-cf['gap_w']*6

    ss_list = [md.ssa.loc[md.ssa.xp_id.isin(
        ['300817-01-01', '310817-01-01', '240617-08-01', '081017-03-01',
         '160817-06-01', '290917-05-01', '120917-02-01', '290917-01-01',
         '240617-03-01'])],
               md.ssa.loc[md.ssa.xp_id.isin(
        ['290917-06-01', '290917-08-01', '220617-08-01', '150917-06-01',
         '160917-04-01', '130817-10-01', '270817-05-01',
         '081017-05-01'])]]

    names = ['Fish that follow the rotation', 'Fish that ignore the rotation']

    for i, ss in enumerate(ss_list):
        left_i = cf['l_margin'] + cf['gap_w']
        bottom_i = cf['b_margin']+(len(ss_list)-i-1) * (half_h + 2*cf['gap_h'])

        if i == 0:
            turn_orig = True
        else:
            turn_orig = False
        fig.text(left_i+cf['gap_w'], bottom_i+2*slide_h+2.5*cf['gap_h'],
                 names[i],
                 va='center')
        ax1 = fig.add_axes([left_i+cf['gap_w'], bottom_i+slide_h+1.5*cf[
            'gap_h'],
                            slide_w, slide_h])
        ax2 = fig.add_axes([left_i+cf['gap_w'], bottom_i, slide_w, slide_h])
        ax3 = fig.add_axes([left_i+slide_w+5*cf['gap_w'],
                            bottom_i+slide_h+1.5*cf['gap_h'],
                            box_w, slide_h])
        ax4 = fig.add_axes([left_i+slide_w+5*cf['gap_w'], bottom_i,
                            box_w, slide_h])

        avg_e, avg_sum_e = als.moving_avg(ss, als.entry, pref=True,
                                          turn_orig=turn_orig,
                                          exclude_overstayers=False,
                                          **{'win_size': 300,
                                              'sliding_overlap': 270})
        avg_o, avg_sum_o = als.moving_avg(ss, als.occupancy, pref=True,
                                          turn_orig=turn_orig,
                                          exclude_overstayers=False,
                                          **{'val': [0, 1, 2, 3],
                                             'win_size': 300,
                                             'sliding_overlap': 270
                                             })

        aplt.plot_effects(ss, avg_e, 'EF score', avg_sum=avg_sum_e,
                          show=False, ax=ax1, ymax=1,
                          se=True, **{'val': [0, 1, 2], 'win_size': 300,
                                      'sliding_overlap': 270})
        aplt.plot_effects(ss, avg_o, 'OC score', show=False, ax=ax2, ymax=1,
                          se=True, **{'val': [0, 1, 2, 3], 'win_size': 300,
                                      'sliding_overlap': 270})
        ax1.set_xticks([0, 30, 90, 120])
        ax2.set_xticks([0, 30, 90, 120])
        ax1.set_xlabel('')
        ax1.set_xticklabels([])

        ax1.text(1, 1,
                 'n = {}'.format(int(ss.shape[0] / len(ss.ss_no.unique()))),
                 transform=ax1.transAxes, va='bottom', ha='right')

        pts = [49, 169, 180, 190, 230]

        aplt.plot_single_bar(ss, avg_e, avg_sum=avg_sum_e, func=als.entry,
                             ax=ax3, pts=pts, pref=True, n=10**5)
        aplt.plot_single_bar(ss, avg_o, func=als.occupancy, ax=ax4, pts=pts,
                             pref=True, n=10**5)
        ax3.set_xlabel('')
        ax4.set_xlabel('')

    top_panel = 0.92
    left_panel = 0.04
    panel_font = 15

    fig.text(left_panel, top_panel, 'A', fontsize=panel_font)
    fig.text(left_panel, 0.42, 'B', fontsize=panel_font)

    fig.savefig('../figures/figure_6_supplement1.png', dpi=dpi)


if __name__ == '__main__':
    figure_6(dpi=600)
    # figure_6s(dpi=600)
