import numpy as np
import pandas as pd
from scipy import signal

from matplotlib import pyplot as pl


def lowpass(Fs, cutoff, stop, ntaps):
    """Return lowpass filter kernel with given cutoff.
    
    scipy.signal.remez uses the Parks-McClellan optimal FIR filter
    design algorithm.

    """

    return signal.remez(ntaps, [0, cutoff, stop, Fs/2], [1, 0], Hz=Fs)
    
    
def show_filter_response(kernel, Fs):
    """Display the frequency response of a filter kernel."""
    
    freq, response = signal.freqz(kernel)
    ampl = np.abs(response)
    freq = freq*Fs
    fig = pl.figure()
    ax1 = fig.add_subplot(111)
    ax1.plot(freq/(2*np.pi), 10*np.log10(ampl), 'b-')
    pl.setp(ax1, xlabel='Frequency (Hz)', ylabel='Power (dB1)')
    pl.show()


def do_filter(sig, kernel):
    """Return signal filtered and trimmed to original length.
    """
    ntaps = len(kernel)
    
    try:
        # faster
        # https://sourceforge.net/motorola/upfirdn/home/Home/
        from upfirdn import upfirdn
        filtered = upfirdn(sig, kernel, 1, 1, all_samples=True)
    except ImportError:
        # quite slow
        # only scipy>=0.18.1
        from scipy.signal import upfirdn
        print("WARNING: the scipy.signal implementation of upfirdn"
              " is extremely slow.")
        print("You're advised to install the original Motorola module.")
        filtered = upfirdn(sig, kernel, 1, 1)
    
    return filtered[int(ntaps/2)-1:-int(ntaps/2)]


def my_argrelextr(data, comp):
    """Return loc of minima/maxima
    comp = 1 if minima, -1 if maxima"""
    if comp == 'max':
        val = -1
    elif comp == 'min':
        val = 1
    else:
        assert "wrong comparison value"
    d = np.diff(data)
    b = np.zeros_like(d)
    with np.errstate(invalid='ignore'):
        b[d < 0] = 0
        b[d > 0] = 1
        b[d == 0] = 2
    b_i = np.where(b != 2)[0]
    b_diff = np.where(np.diff(b[b_i]) == val)[0]
    return ((b_i[b_diff] + b_i[b_diff+1])/2.0 + 1).astype(int)


def bouts(original, cutoff=4, stop=6, Fs=60., ntaps=72):
    """Takes original trace and creates a dataframe with bout info, including
    start/end, distance traveled, argmax
    Filtering with bandwidth [cutoff, stop]Hz
    """
    bout_filter = lowpass(Fs=Fs, cutoff=cutoff, stop=stop, ntaps=ntaps)
    # show_filter_response(bout_filter, Fs=Fs)

    filtered = do_filter(original, bout_filter)

    minlocs = my_argrelextr(filtered, 'min')
    maxlocs = my_argrelextr(filtered, 'max')

    assert len(maxlocs) - len(minlocs) < 3

    if minlocs[0] > maxlocs[0]:
        # print("Min-max-min-max...; discard first max")
        maxlocs = maxlocs[1:]
    else:
        pass
        # print("Max-min-max-min; everything fine")

    if minlocs[-1] < maxlocs[-1]:
        # print("..max-min-max; discard last max")
        maxlocs = maxlocs[:-1]
    else:
        pass
        # print("..max-min-max-min; everything fine")

    bouts = np.split(filtered, minlocs)
    bouts = bouts[1:-1]  # not interested in run up to first min, or after last
    bouts_raw = np.split(original, minlocs)
    bouts_raw = bouts_raw[1:-1]  # not interested in run up to first min,
    # or after last

    df = pd.DataFrame(dict(
        pre_idx=minlocs[:-1],
        post_idx=minlocs[1:],
        max_idx=maxlocs,
        distance=np.array(list(map(np.sum, bouts_raw))),
        distance_filt=np.array(list(map(np.sum, bouts))),
        max_filt=filtered[maxlocs]))
    return df, filtered
