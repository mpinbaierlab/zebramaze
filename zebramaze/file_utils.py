import os
import configparser


def check_folder(root, name):
    dist_folder = os.path.join(root, name)
    if not os.path.exists(dist_folder):
        os.mkdir(dist_folder)

    return dist_folder


def load_config():
    conf = configparser.ConfigParser()
    conf.read('figstyle.INI')
    sections = {sect: {k: float(v) for k, v in conf[sect].items()} for sect
                in conf.sections()}
    return sections
