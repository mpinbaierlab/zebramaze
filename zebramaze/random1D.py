import numpy as np
from scipy.stats import gamma
import pandas as pd
from matplotlib import pyplot as plt
import matplotlib.colors as mcolors
import matplotlib.patches as mpatch
import matplotlib.colorbar as mcolorbar
import zebramaze.plotting


S = 5  # length of shocked arm
dt = 1  # time step


def step1D(x0, arm0, sigma, prob, arm_lengths=(S, S, S), refl=False):
    """
    Return updated position in a simulation
    Receives an array of positions, corresponding arms, and step sizes.
    For each position update by adding the corresponding step. If an updated
    position falls out of boundaries of the starting arm, update the arm:
    from an arm to a center; from a center to a random arm.

    :param x0: float array_like, current position
    :param arm0: int array_like, current arm
    :param sigma: float array_like, directed size (by + or - sign) of next
                    step for each position
    :param arm_lengths: tuple-like, boundaries arms + center
    :param refl: bool, True if reflective boundary to be used,
                        False if sticky boundary (more realistic?)
    :param prob: array, probabilities of entry to each arm

    :return x1: float array_like, updated position
    :return arm1: int array_like, arms corresponding to the updated
    position
    """

    arm_num = len(arm_lengths)  # number of arms

    x1 = x0 + sigma  # update coordinates
    arm1 = arm0.copy()
    arm_limits = arm0.map(lambda x: arm_lengths[x]).values

    center = arm0.values == arm_num-1

    # first deal with the center
    boundary_max = x1.values - arm_limits > 0
    boundary_min = x1.values - 0 < 0
    overshoot = (boundary_min | boundary_max) & center

    prob_c = np.cumsum(prob[:, overshoot], 0)
    rand_arms = (np.random.rand(prob_c.shape[1], 1).T < prob_c).argmax(0)
    arm1.loc[overshoot] = rand_arms
    x1.loc[boundary_min & center] *= -1
    x1.loc[boundary_max & center] = \
        (x0 + sigma - arm_lengths[-1]).values[boundary_max & center]

    arm_limits[overshoot] = arm1[overshoot].map(lambda x:
                                                arm_lengths[x]).values

    # second deal with arm entries
    # if next step crosses left boundary, it can switch to another arm!
    overshoot = x1.values < -arm_lengths[-1]  # overshoot over center
    boundary_min = (x1.values - 0 < 0) & ~overshoot  # overshoot into center
    arm1.loc[boundary_min] = arm_num-1
    x1.loc[boundary_min] *= -1
    x1.loc[overshoot] = (-arm_lengths[-1] - x1).values[overshoot]

    prob_c = np.cumsum(prob[:, overshoot], 0)
    rand_arms = (np.random.rand(prob_c.shape[1], 1).T < prob_c).argmax(0)
    arm1.loc[overshoot] = rand_arms
    arm_limits[overshoot] = arm1[overshoot].map(lambda x:
                                                arm_lengths[x]).values

    # finally deal with the arm ends
    boundary_max = x1.values - arm_limits > 0
    if refl:
        # that's unrealistic, should drop it
        x1.loc[boundary_max] = \
            (2 * arm_limits - (x0 + sigma).values)[boundary_max]
    else:
        x1.loc[boundary_max] = arm_limits[boundary_max]

    return x1, arm1


def update_prob(prob, arm_old, arm_new, learn=True, learn_rate=1.):
    prob_upd = prob.copy()
    arm_change = (arm_old-arm_new) != 0
    arm_shock = arm_new == 0
    if learn:
        shock_change = (0.1-prob_upd[0, arm_change & arm_shock]) * learn_rate

    else:
        shock_change = (1/3-prob_upd[0, arm_change & arm_shock]) * learn_rate
    prob_change = [shock_change, -shock_change / 2, -shock_change / 2]
    prob_upd[:, arm_change & arm_shock] += prob_change

    return prob_upd


def rel_occupancy(arms, arm_num, s, r):
    """
    Plots relative occupancy of each arm

    :param arms: df, columns contain arm sequence of a single simulation
                    rows contain several samples per 'r' value
                    r1          r2          ... rk
                    s1 s2 .. sn s1 s2 .. sn ... s1 s2 .. sn
    :param arm_num: int, number of arms
    :param s: int, sample size per each 'r' value
    :param r: array-like, 'r' values tested (not unique!!)

    """
    color_map = ['r', 'b', 'g', 'k']
    for a in range(arm_num):
        arm_samples = (arms == a).mean(0).reshape((int(r.shape[0]/s), s))
        plt.errorbar(np.arange(r.shape[0]/s), arm_samples.mean(1),
                     yerr=arm_samples.std(1),  # /np.sqrt(arm_samples.shape[1]),
                     c=color_map[a])
    plt.xlabel('Speed ratio', fontsize=20)
    plt.xticks(np.arange(arms.shape[1]/s), r.unique())
    plt.ylabel('Arm occupancy', fontsize=20)
    xmin, xmax = np.arange(r.shape[0]/s).min()-1, \
        np.arange(r.shape[0]/s).max()+1
    plt.xlim([xmin, xmax])
    plt.yticks(np.arange(0, 0.6, 0.1))
    plt.title("n = {} steps, "
              "sample = {} per 'r' value".format(arms.shape[0], s))

    plt.show()


def effect_sliding(arm_list, arm_num, s, out_list, entry=False, ax1=None,
                   colorbar=True, arr=None):
    r = out_list[1].columns.get_level_values(0)
    arms = pd.concat(arm_list, ignore_index=True).values
    dur = arms.shape[0]
    win_size = int(dur / 24)
    sliding_overlap = int(win_size * 9 / 10)
    r_num = len(set(r))
    num = int((dur - win_size) / (win_size - sliding_overlap) + 1)
    color_map = np.arange(50, 255, (255 - 50) / r_num) / 255
    as_strided = np.lib.stride_tricks.as_strided

    # apply sliding window with overlap using strides and reshaping
    arms_T = arms.T.copy()
    stri0, stri1 = arms_T.strides
    a_resh = as_strided(arms_T, (len(r), num, win_size),
                        (stri0, stri1 * (win_size - sliding_overlap), stri1))
    if entry:
        arm_num -= 1  # we don't look at the center in case of entry freq
        ax_name = 'EF score'
    else:
        ax_name = 'OC score'

    cb_w = 0.015
    ax1_axi = 0.02
    panel_w = 0.1
    panel_lvl = 0.52
    if ax1 is None:
        ax1_left = 0.1
        axi_right = 0.1
        ax1_bottom = 0.11
        ax1_h = 0.77
        ax1_w = 1 - cb_w - ax1_axi - axi_right - ax1_left
        f1 = plt.figure(figsize=(6.2 / ax1_w, 8))  # to match experimental plots
        ax1 = f1.add_axes([ax1_left, ax1_bottom, ax1_w, ax1_h])
    else:
        f1 = ax1.get_figure()
        ax_wind = ax1.get_position()
        ax1_left = ax_wind._points[0, 0]
        ax1_bottom = ax_wind._points[0, 1]
        ax1_right = 1 - ax_wind._points[1, 0]
        ax1_h = ax_wind.height
        ax1_w = ax_wind.width
        cb_w = min(ax1_right - 0.05 - ax1_axi, cb_w)

    all_sliding = []
    for a in range(arm_num):

        if entry:
            # count changes of arms, normalize by number of exits to center
            arm_sliding = (np.diff((a_resh == a), 2) == 1).sum(2)
            arm_sliding = np.divide(arm_sliding,
                                    (np.diff((a_resh == arm_num),
                                             2) == 1).sum(2) + 1)
        else:
            arm_sliding = (a_resh == a).mean(2)
        all_sliding.append(arm_sliding)

    all_sliding = np.array(all_sliding)
    eff = all_sliding[0] - (all_sliding[1]+all_sliding[2])/2

    # reshape to separate samples of different r values
    arm_samples = eff.reshape((int(r.shape[0] / s), s, num))
    for i, arm_s in enumerate(arm_samples):
        # color intensifies for higher values of r
        if r_num == 1:
            alpha = 1
        else:
            alpha = color_map[i]
        ax1.plot(np.arange(num) + 5, arm_s.mean(0),
                 c=np.append((0, 0, 0), alpha))
        if r_num == 1:
            err = arm_s.std(0) / np.sqrt(arm_samples.shape[1])
            ax1.fill_between(np.arange(num) + 5, arm_s.mean(0) - err,
                             arm_s.mean(0) + err, color=(0, 0, 0),
                             alpha=0.4)
    if arr is not None:
        for xi in arr:
            ax1.annotate("", xy=(xi, 0.1), xytext=(xi, 0.12),
                         annotation_clip=False, xycoords='data',
                         arrowprops=dict(arrowstyle="-|>",
                                         mutation_scale=15, color='k'))

    patch_borders = []
    tri_patch = []
    sh_start, sh_end, sess_start = 0, 999, 0
    cmap = plt.get_cmap('Blues')
    cmap = cmap(np.linspace(0, 0.05, cmap.N // 20))
    cmap[0] = [1, 1, 1, 1]
    panel_col = plt.get_cmap('Blues')(100)

    # Create a new colormap from those colors
    cmap = mcolors.LinearSegmentedColormap.from_list('Upper Half', cmap)
    tshock = 0

    # calculate plotting borders of sessions
    for i, arm in enumerate(arm_list):
        tshock += len(arm)
        nshock = (tshock - win_size) / (win_size - sliding_overlap) + 5
        if i == 0:
            sh_start = nshock + win_size / (win_size - sliding_overlap)
            patch_borders.append((sess_start, nshock, (0.8, 0.8, 0.8)))

            tri_patch.append(
                ([[nshock, panel_lvl], [nshock, panel_lvl - panel_w],
                  [sh_start, panel_lvl - panel_w]], (0.8, 0.8, 0.8)))
            tri_patch.append(([[nshock, panel_lvl],
                               [sh_start, panel_lvl - panel_w],
                               [sh_start, panel_lvl]], panel_col))
        elif i == 1:
            sh_end = nshock
            patch_borders.append((sess_start, nshock - sess_start, panel_col))
        elif i == 2:
            patch_borders.append((sess_start, num - sess_start + 10,
                                  (0.8, 0.8, 0.8)))
            tri_patch.append(
                ([[sh_end, panel_lvl], [sh_end, panel_lvl - panel_w],
                  [sess_start, panel_lvl - panel_w]], panel_col))
            tri_patch.append(([[sh_end, panel_lvl],
                               [sess_start, panel_lvl - panel_w],
                               [sess_start, panel_lvl]], (0.8, 0.8, 0.8)))
            continue
        if len(arm_list) != 1:
            ax1.vlines(
                [nshock, nshock + win_size / (win_size - sliding_overlap)],
                -panel_lvl, panel_lvl, panel_col, '--')
        sess_start = nshock + win_size / (win_size - sliding_overlap)

    # plot session titles on top of sliding time scale
    patch_names = [r'$\alpha = $1', r'$\alpha \geq $1', r'$\alpha = $1']
    for pb, pn in zip(patch_borders, patch_names):
        rect = mpatch.Rectangle((pb[0], panel_lvl - panel_w), pb[1], panel_w,
                                facecolor=pb[2])
        ax1.add_artist(rect)
        rx, ry = rect.get_xy()
        cx = rx + rect.get_width() / 2
        cy = ry + rect.get_height() / 2
        scale = ax1.get_window_extent().transformed(
            f1.dpi_scale_trans.inverted()).height
        ax1.annotate(pn, (cx, cy), color='k', ha='center', va='center')

    # draw session transition zones
    if len(arm_list) != 1:
        for tri in tri_patch:
            triag = mpatch.Polygon(np.array(tri[0]), facecolor=tri[1])
            ax1.add_artist(triag)
        ax1.fill_between([sh_start, min(sh_end, num)], -panel_lvl, panel_lvl,
                         color=cmap(100), alpha=0.5)
    plt.setp(ax1, xlabel='Simulation time [min]', ylabel=ax_name)

    xmin, xmax = 0, num + 10
    ax1.set_xlim(xmin, xmax)
    ax1.hlines([0], xmin, xmax, 'k', '--', linewidth=1)
    ax1.set_ylim(-0.52, 0.52)
    ax1.set_title(r"$n = {} steps, \n$"
                  r"$sample = {} per \alpha-value$".format(arms.shape[0], s))

    # colorbar
    if (r_num > 1) & colorbar:
        axi = f1.add_axes(
            [ax1_left + ax1_w + ax1_axi, ax1_bottom, cb_w, ax1_h])
        axi.axis('off')
        cmap = mcolors.ListedColormap(np.append(np.tile((0, 0, 0), (r_num, 1)),
                                                1 - color_map.reshape(
                                                    r_num, 1), axis=1))
        cb = mcolorbar.ColorbarBase(axi, cmap=cmap)

        axi.text(0, 1, r'$\alpha$ values', ha='left', va='bottom')
        axi.axis('on')
        cb.set_ticks(np.arange(0.125, 0.876, 0.25))
        cb.ax.tick_params(length=0)
        r_labels = list(set(r))
        r_labels.sort(reverse=True)
        cb.set_ticklabels(r_labels)
        cb.outline.set_visible(False)


def occupancy_sliding(arm_list, arm_num, s, out_list, entry=False, ax1=None,
                      colorbar=True):
    """
    Plot occupancy/entry to arms in sliding window plot

    :param arm_list: list, each element is a DataFrame with arm sequences of a
                        simulated session
    :param arm_num: int, number of arms (including the center)
    :param s: int, sample size for each value of r
    :param out_list: list, each element is a DataFrame with coordinate
                        sequences of a simulated session
    :param entry: bool, True if plotting entry frequences

    """
    r = out_list[1].columns.get_level_values(0)
    arms = pd.concat(arm_list, ignore_index=True).values
    dur = arms.shape[0]
    win_size = int(dur/12)
    sliding_overlap = int(win_size*9/10)
    r_num = len(set(r))
    num = int((dur - win_size) / (win_size - sliding_overlap) + 1)
    color_map = np.arange(50, 255, (255-50)/r_num)/255
    as_strided = np.lib.stride_tricks.as_strided
    color_table = np.eye(arm_num)[:, :-1]
    colors = np.array([(212, 37, 19), (255, 165, 0), (146, 208, 80),
                       (96, 96, 96)]) / 255

    # apply sliding window with overlap using strides and reshaping
    arms_T = arms.T.copy()
    stri0, stri1 = arms_T.strides
    a_resh = as_strided(arms_T, (len(r), num, win_size),
                        (stri0, stri1 * (win_size - sliding_overlap), stri1))
    if entry:
        arm_num -= 1  # we don't look at the center in case of entry freq
        ax_name = 'Entry frequency'
    else:
        ax_name = 'Occupancy'

    cb_between = 0.005
    cb_w = 0.015
    ax1_axi = 0.02
    panel_w = 0.08
    panel_lvl = 0.7
    if ax1 is None:
        ax1_left = 0.1
        axi_right = 0.1
        ax1_bottom = 0.11
        ax1_h = 0.77
        ax1_w = 1-(arm_num-1)*cb_between-arm_num*cb_w-ax1_axi-axi_right-ax1_left
        f1 = plt.figure(figsize=(6.2/ax1_w, 8))  # to match experimental plots
        ax1 = f1.add_axes([ax1_left, ax1_bottom, ax1_w, ax1_h])
    else:
        f1 = ax1.get_figure()
        ax_wind = ax1.get_position()
        ax1_left = ax_wind._points[0, 0]
        ax1_bottom = ax_wind._points[0, 1]
        ax1_right = 1 - ax_wind._points[1, 0]
        ax1_h = ax_wind.height
        ax1_w = ax_wind.width
        cb_w = (ax1_right - 0.05 - ax1_axi) / arm_num - cb_between

    for a in range(arm_num-1, -1, -1):

        if entry:
            # count changes of arms, normalize by number of exits to center
            arm_sliding = (np.diff((a_resh == a), 2) == 1).sum(2)
            arm_sliding = np.divide(arm_sliding,
                                    (np.diff((a_resh == arm_num),
                                             2) == 1).sum(2) + 1)
        else:
            arm_sliding = (a_resh == a).mean(2)

        # reshape to separate samples of different r values
        arm_samples = arm_sliding.reshape((int(r.shape[0]/s), s, num))
        for i, arm_s in enumerate(arm_samples):
            # color intensifies for higher values of r
            if r_num == 1:
                alpha = 1
            else:
                alpha = color_map[i]
            ax1.plot(np.arange(num)+5, arm_s.mean(0),
                     c=np.append(colors[a], alpha))
            if r_num == 1:
                err = arm_s.std(0) / np.sqrt(arm_samples.shape[1])
                ax1.fill_between(np.arange(num)+5, arm_s.mean(0)-err,
                                 arm_s.mean(0)+err, color=colors[a], alpha=0.4)

    patch_borders = []
    tri_patch = []
    sh_start, sh_end, sess_start = 0, 999, 0
    cmap = plt.get_cmap('Blues')
    cmap = cmap(np.linspace(0, 0.05, cmap.N // 20))
    cmap[0] = [1, 1, 1, 1]
    panel_col = plt.get_cmap('Blues')(100)

    # Create a new colormap from those colors
    cmap = mcolors.LinearSegmentedColormap.from_list('Upper Half', cmap)
    tshock = 0

    # calculate plotting borders of sessions
    for i, arm in enumerate(arm_list):
        tshock += len(arm)
        nshock = (tshock - win_size) / (win_size - sliding_overlap) + 1 + 5
        if i == 0:
            sh_start = nshock + win_size / (win_size - sliding_overlap)
            patch_borders.append((sess_start, nshock, (0.8, 0.8, 0.8)))

            tri_patch.append(([[nshock, panel_lvl], [nshock, panel_lvl-panel_w],
                               [sh_start, panel_lvl-panel_w]], (0.8, 0.8, 0.8)))
            tri_patch.append(([[nshock, panel_lvl],
                               [sh_start, panel_lvl-panel_w],
                               [sh_start, panel_lvl]], panel_col))
        elif i == 1:
            sh_end = nshock
            patch_borders.append((sess_start, nshock - sess_start, panel_col))
        elif i == 2:
            patch_borders.append((sess_start, num - sess_start + 10,
                                  (0.8, 0.8, 0.8)))
            tri_patch.append(([[sh_end, panel_lvl], [sh_end, panel_lvl-panel_w],
                               [sess_start, panel_lvl-panel_w]], panel_col))
            tri_patch.append(([[sh_end, panel_lvl],
                               [sess_start, panel_lvl-panel_w],
                               [sess_start, panel_lvl]], (0.8, 0.8, 0.8)))
            continue
        if len(arm_list) != 1:
            ax1.vlines([nshock, nshock + win_size/(win_size - sliding_overlap)],
                       0, panel_lvl, panel_col, '--')
        sess_start = nshock + win_size / (win_size - sliding_overlap)

    # plot session titles on top of sliding time scale
    patch_names = [r'$\alpha = 1$', r'$\alpha \geq 1$', r'$\alpha = 1$']
    for pb, pn in zip(patch_borders, patch_names):
        rect = mpatch.Rectangle((pb[0], panel_lvl-panel_w), pb[1], panel_w,
                                facecolor=pb[2])
        ax1.add_artist(rect)
        rx, ry = rect.get_xy()
        cx = rx + rect.get_width() / 2
        cy = ry + rect.get_height() / 2
        scale = ax1.get_window_extent().transformed(
            f1.dpi_scale_trans.inverted()).height
        ax1.annotate(pn, (cx, cy), color='k', ha='center', va='center')

    # draw session transition zones
    if len(arm_list) != 1:
        for tri in tri_patch:
            triag = mpatch.Polygon(np.array(tri[0]), facecolor=tri[1])
            ax1.add_artist(triag)
        ax1.fill_between([sh_start, min(sh_end, num)], 0, panel_lvl,
                         color=cmap(100), alpha=0.5)
    plt.setp(ax1, xlabel='Simulation time', ylabel=ax_name)

    xmin, xmax = 0, num+10
    ax1.set_xlim(xmin, xmax)
    ax1.set_ylim(0, panel_lvl)
    ax1.set_title(r"$n = {} steps, \n$"
                  r"$sample = {} per \alpha-value$".format(arms.shape[0], s))

    # colorbar
    if (r_num > 1) & colorbar:
        for a in range(arm_num):
            axi = f1.add_axes([ax1_left+ax1_w+ax1_axi+a*(cb_between+cb_w),
                               ax1_bottom, cb_w, ax1_h])
            axi.axis('off')
            cmap = mcolors.ListedColormap(np.append(np.tile(colors[a],
                                                            (r_num, 1)),
                                                    1-color_map.reshape(
                                                        r_num, 1), axis=1))
            cb = mcolorbar.ColorbarBase(axi, cmap=cmap)
            if a == arm_num-1:
                axi.text(1, 1, r'$\alpha$ values', ha='right', va='bottom')
                axi.axis('on')
                cb.set_ticks(np.arange(0.125, 0.876, 0.25))
                r_labels = list(set(r))
                r_labels.sort(reverse=True)
                cb.set_ticklabels(r_labels)
            cb.outline.set_visible(False)


def relabel_preference(arms, arm_num):
    """
    Relabel arms so that 0th arm is the preferred one

    :param arms: DataFrame, row i contains arm on ith step of simulation
                    column j contains a simulated sequence of arm positions
                    for a particular value of r
    :param arm_num: int, number of arms (excluding center)

    :return: DataFrame, relabeled original arm sequnces
    """
    arms1 = arms.copy()
    arms1[arms == 3] = np.nan  # exclude centers from preference calculation
    preferred_arm = arms1.mode(0).iloc[0].values.astype(int)

    is_center = arms.values == arm_num
    relabeled = arms.values
    relabeled -= preferred_arm
    relabeled[is_center] = arm_num
    relabeled[relabeled < 0] += arm_num

    return pd.DataFrame(relabeled)


def list_pos(a_list, a):
    """
    Return a value in a_list at the position 'a'

    :param a_list: list
    :param a: position in the list
    :return: list[position a]
    """
    # Needed for future vectorization and calculation of arm lengths
    # in brownian1D
    return a_list[a]


def calc_x0(x0, out_size, arm_lengths, arm0, x_limits):
    """
    Return starting positions, random if x0 is NaN

    :param x0: NaN or array-like
                starting points in the session, random if NaN
    :param out_size: int, size of output df = (number of different r values)
                        times (number of samples per r value)
    :param arm0: array_like, each element corresponding
                    to a starting arm of a trajectory to be simulated
    :param arm_lengths: tuple-like, arm lengths (including center)
    :param x_limits: tuple-like of size 2, 0 <= x_limits[0] < x_limits[1] <= 1,
                        limits showing which part of the arms can
                        a random coordinate be picked from (for arm [0, Si]
                        it will be [x_limits[0]*Si, x_limits[1]*Si]

    :return x0: array-like, an array of starting positions, corresponding to
                    arm0

    If different arms have different sizes, it will influence selection of
    random position in each of the arms; for that we calculate arms_limits -
    an array of arm0.size, elements of which correspond to elements
    of arm0 and contain the arm size. This way a random starting coordinate
    will be picked from an interval of appropriate length for each trajectory
    to be simulated.
    """

    if np.isnan(x0).all():
        # Pick a random starting position for all arms
        vfunc = np.vectorize(list_pos, excluded=['a_list'])
        arm_limits = vfunc(a_list=arm_lengths, a=arm0)
        x0_size = arm0.shape[0]
        if x_limits:
            if (x_limits[0] < 0) or (x_limits[1] > 1):
                raise NameError('x_limits [{}, {}] are out of allowed interval\
                                  [0, 1]!'.format(x_limits[0], x_limits[1]))
            x0 = (np.random.random_sample(x0_size) *
                  (x_limits[1] - x_limits[0]) + x_limits[0]) * arm_limits
        else:
            x0 = np.random.random_sample(x0_size) * arm_limits
    else:
        if x0.shape[0] != out_size:
            raise Exception("number of starting coordinates doesn't match "
                            "the number of trajectories to be simulated!")

    return np.asarray(x0)


def calc_arm0(arm0, arm_num, out_size):
    """
    Return starting arms, random if arm0 is NaN

    :param arm0: NaN or array-like, starting arms in the session,
                    should be random if NaN
    :param arm_num: int, number of different arms (including center)
    :param out_size: int, size of output df = (number of different r values)
                        times (number of samples per r value)

    :return : array-like, an array of starting arms, corresponding to arm0
    """
    if np.isnan(arm0).all():
        # assign random arms
        arm0 = np.random.choice(arm_num, size=out_size)
    else:
        if arm0.shape[0] != out_size:
            raise Exception("the number of starting arms doesn't match \
                             the number of trajectories to be simulated!")
    return arm0


def random1D(n=1000, x0=np.array([np.NaN]), refl=False, x_limits=None,
             arm0=np.array([np.NaN]), r_range=np.arange(1, S, 0.1), sample=1,
             alpha=0.065, arm_lengths=(S, S, S), learn=False,
             prob=np.ones([2, np.arange(1, S, 0.1).shape[0]*1])*0.5):
    """
    Simulate a session

    :param n: int, number of steps in the simulation
    :param x0: NaN or array-like, starting coordinates in the session,
                random if NaN
    :param arm0: NaN or array-like, starting arms in the session, random if NaN
    :param refl: bool, False if reflecting boundary to be used,
                    True if the boundary is sticky (better?)
    :param x_limits: None or tuple, limits to where a starting position can be
    :param r_range: array-like, a vector with r values
    :param sample: int, sampling of each r value
    :param alpha: float < 1, fraction of shock arm length that is the maximal
                    step size
    :param arm_lengths: tuple-like, boundaries of all arms (including center),
                            defines how many arms there are
    :param prob: array, probabilities of entry to each arm for each simulated
                    agent

    :return out: df, simulated coordinates, each column is a simulated
                    sequence for a particular sample of r value, each row has
                    coordinates of all samples at a particular simulation step
    :return arms: df, arms corresponding to coordinates

    Basics of the model
    -------------------
    At each time point a fish makes a step. The size and direction of the
    step are contained in 'sigma' variable. The step size is drawn from a
    gamma distribution, the directionality is randomly picked (+ or -).

    The intuition is that when a fish receives a shock, it moves faster.
    Thus its speed is higher. Because of that it will get out of the shocked
    arm faster and will spend more time in non-shocked ones.
    We call the ratio of (speed in shocked arm) to (speed in non-shocked
    arm) as 'r'. Relative occupancy of shocked arm as a function of 'r'
    should be decreasing monotonically.

    Example usage
    -------------
    Simulation of 1000 steps, in a 3-armed maze with a center, arms of equal
    size, smaller center.
    'r' values are in the range (1, 5, 0.1), for each 'r' value 100 samples
    are simulated. The output is (1) a dataframe of shape (101, 4000), where
    101 is number of positions simulated (initial + 100 steps), and
    4000 is 100x40 - 100 samples for 40 values of 'r' (see
    notebooks\random1d.ipynb for more details); (2) a dataframe of shape
    (101, 4000) corresponding to 'out', and containing information about which
    arm number was the fish in at a given simulation step.

    out, arms = random1D(n=100, sample=100, arm_lengths=(5,5,5, 1))

    """

    arm_num = len(arm_lengths)

    # calculate indices for multiple indexing of output dataframe columns
    r_ind = np.repeat(r_range, sample)  # r values repeat 'sample' times
    s_ind = np.tile(np.arange(1, sample+1), r_range.shape[0])

    out_size = r_range.size*sample  # size of output df

    arm0 = calc_arm0(arm0, arm_num, out_size)
    x0 = calc_x0(x0, out_size, arm_lengths, arm0, x_limits)

    # output dataframe for coordinates with multiple indexed columns,
    # level 0 corresponds to 'r' values, level 1 - to sample numbers
    out = pd.DataFrame(np.zeros([n+1, out_size]), columns=[r_ind, s_ind])
    out.columns = out.columns.set_names(['r', 'sample'])
    out.loc[:] = np.NaN
    out.loc[0] = x0

    # output dataframe for arms
    arms = pd.DataFrame(np.zeros([n+1, out_size], dtype=np.int8))
    arms.loc[0] = arm0

    i = 0
    r = np.ones(out_size)
    step_mean = arm_lengths[0]*alpha
    while i < n:
        i += 1
        # set the 'speed' in shock arm to corresponding 'r' values
        shock = arms.loc[i-1] == 0
        r[np.where(shock)] = out.columns.get_level_values(0)[shock]
        r[np.where(~shock)] = 1

        # step size
        # sigma = truncnorm.rvs(-step_mean, step_mean, size=(out_size,)) + \
        #        step_mean
        sigma = gamma.rvs(1.96, scale=step_mean/1.96)

        # step direction
        sigma *= np.sign(np.random.random_sample(out_size)-0.5)

        x1, arm1 = step1D(out.loc[i-1], arms.loc[i-1], np.multiply(sigma, r),
                          arm_lengths=arm_lengths, refl=refl, prob=prob)
        out.loc[i] = x1
        arms.loc[i] = arm1
        prob = update_prob(prob, arms.loc[i], arms.loc[i-1], learn=learn,
                           learn_rate=0.03)

    return out, arms, prob


def experiment1d(sess=('control', 'training', 'test'), s=100, preferred=True,
                 sess_len=(500, 1000, 500), maze=(S, S, S, S), refl=True,
                 learn=False, r_range=None):
    """
    Simulates an experiment of several sessions

    :param sess: tuple-like, a tuple of sessions to run
    :param s: int, sample size for each of the 'r' values (see random1D docs)
    :param preferred: bool, True if 'preferred' arm (defined from first
                        control session) is shocked
    :param sess_len: tuple-like, a tuple of session durations (i.e. number of
                        steps to simulate), MUST correspond to length of sess
    :param maze: tuple-like, a tuple of arm lengths, [-1] corresponds to center
    :param refl: bool, True if sticky boundary to be used, False if
                    reflective boundary is used
    :return res_out: list, each element is a DataFrame with simulated
                        coordinates in a corresponding session
    :return res_arm: list, each element is a DaraFrame with simulated arms in
                        a corresponding session

    zip(res_out, res_arm) will produce pairs (out_i, arm_i), which are
    in fact the outputs of random1D (a single session simulation)
    and correspond to sessions in 'sess'.

    Last rows of output dataframes from each session simulation serve
    as input first rows to simulation of the next session.
    """

    # initial conditions of first coordinate and first arm are randomly picked
    x0 = np.array([np.NaN])
    arm0 = np.array([np.NaN])
    if r_range is None:
        r_range = np.arange(1, S, 0.5)
    prob = np.ones([len(maze)-1, r_range.shape[0]*s])/(len(maze)-1)

    res_out = []
    res_arm = []

    for i in range(len(sess)):
        if sess[i] == 'training':
            r = r_range  # 'r' > 1 for sessions with 'shocks'
            l = learn
        else:
            r = np.ones_like(r_range)  # 'r' = 1 everywhere
            l = False
        out, arms, prob = random1D(n=sess_len[i], r_range=r, arm_lengths=maze,
                                   refl=refl, x0=x0, arm0=arm0, sample=s,
                                   prob=prob, learn=l)
        if preferred & (sess[i] == 'control'):
            # relabel arms so that preferred arm is 0
            arms = relabel_preference(arms, len(maze)-1)
        x0 = out.iloc[-1].values
        arm0 = arms.iloc[-1].values
        res_arm.append(arms)
        res_out.append(out)

    return res_out, res_arm


if __name__ == '__main__':

    s = 200  #sample size per 'r' value
    s_dur = 5000  # number of steps in control session
    # out, arms, prob = random1D(n=s_dur, refl=True, sample=s,
    #                            arm_lengths=[S, S, S], xcent=S/3)
    out_list, arm_list = experiment1d(sess_len=(s_dur, s_dur*2, s_dur),
        s=s, refl=False, maze=[S, S, S, S*0.3], learn=False)
    # arm_cent = define_center(arms, out, S/3)
    # arms = pd.concat(arm_list, ignore_index=True)
    occupancy_sliding(arm_list, 4, s, out_list, entry=False)
    occupancy_sliding(arm_list, 4, s, out_list, entry=True)
    # rel_occupancy(arms, 3, 100, arms.columns.get_level_values(0))
    plt.show()
