from matplotlib import pyplot as plt
import matplotlib.patches as mpatch
from PIL import Image

import zebramaze.md as md
import zebramaze.plotting as aplt
import zebramaze.analysis as als


def figure_2(dpi=100):
    cf = aplt.CF['BORDERS']
    f_w, f_h = 8, 10
    fig = plt.figure(figsize=(f_w, f_h))  # for occupancy analysis
    fig_suppl = plt.figure(figsize=(f_w, f_h))  # for EF analysis

    # each experiment occupies a third of the plot (vertically)
    third_h = (1-cf['b_margin']-cf['t_margin']-4*cf['gap_h'])/3
    # xp schematic occupies a quarter of the xp height
    schema_h = (third_h - cf['gap_m'])/4
    movavg_h = (third_h - cf['gap_m'])*3/4
    movavg_w = (1 - cf['r_margin']-cf['l_margin']-5*cf['gap_w'])*0.4
    box_w = movavg_w / 0.4 * 0.6

    # prepare the list with three experiments to plot
    sss = [md.ssa_test_3.loc[md.xp_new & (md.ssa_test_3.xp_age > 20)],
           md.ssa_delay_5, md.ssa_test_1[md.xp_new]]
    ss_list = []
    for ss_sub in sss:
        ft = als.fish_types(ss_sub)
        over = ft['Overstayer']
        print("{} fish with {} overstayers".format(
            int(ss_sub.shape[0] / len(ss_sub.ss_no.unique())), len(over)))
        ss_sub = ss_sub.loc[ss_sub.xp_id.isin(ft['Center'] + ft[
            'Non-responder'] + ft['One arm'] + ft['Two arms'])]
        ss_list.append(ss_sub)

    seq = ['vxi', 'vxi', 'ddd']
    # names = ['', '', '']  # for naming the experiments

    for f_i in [fig, fig_suppl]:
        ax_legend = f_i.add_axes([0.75, 0.92, 0.1, 0.01])
        ax_legend.text(0.5, 0.5, 'Null-distribution', va='center')
        p1 = mpatch.Rectangle((0.2, 0.), 0.2, 1, facecolor='#AAAAAA')
        ax_legend.add_patch(p1)
        ax_legend.axis('off')

    for i, ss in enumerate(ss_list):
        # for the xp with 'gray' 5-min long session window and step sizes
        # for moving average are different (5 min = 300 frames)
        if i == 0:
            minute = 0
            ws = 300
            ovr = 270
            pts = [49, 169, 180, 190, 230]
            grey = False
        elif i == 1:
            minute = 5
            ws = 300
            ovr = 270
            grey = True
            pts = [49, 169, 180 + (minute - 5), 180 + 2 * (minute - 5) + 10]
        else:
            minute = 0
            #ws = 600
            #ovr = 540
            ws = 300
            ovr = 270
            pts = [49, 169, 180]
            grey = False
            #pts = [19, 79, 90]  # points for significance calculations

        # ----CALCULATE EF AND OC AVERAGES FOR THE CURRENT XP----
        avg_e, avg_sum_e = als.moving_avg(ss, als.entry,
                                          exclude_overstayers=False,
                                          **{'val': [0, 1, 2],
                                              'win_size': ws,
                                              'sliding_overlap': ovr})
        avg_o, avg_sum_o = als.moving_avg(ss, als.occupancy,
                                          exclude_overstayers=False,
                                          **{'val': [0, 1, 2, 3],
                                              'win_size': ws,
                                              'sliding_overlap': ovr})

        left_i = cf['l_margin'] + cf['gap_w']
        bottom_i = cf['b_margin'] + \
            (len(ss_list)-i-1)*(movavg_h+schema_h+2*cf['gap_h']+cf['gap_m'])

        # always plot everything for the main figure (OC) and supplement (EF)

        # -------------PROTOCOL SCHEMATIC---------------------
        aplt.plot_protocol(fig, '', seq[i], left_i - 0.5*cf['gap_w'],
                           bottom_i+movavg_h+cf['gap_m'],
                           movavg_w + cf['gap_w'], schema_h,
                           grey=grey, minute=minute, cont=False)
        aplt.plot_protocol(fig_suppl, '', seq[i], left_i - 0.5*cf['gap_w'],
                           bottom_i+movavg_h+cf['gap_m'],
                           movavg_w + cf['gap_w'], schema_h,
                           grey=grey, minute=minute, cont=False)

        # -----------MOVING AVERAGE AND BOXPLOTS--------------
        ax1 = fig_suppl.add_axes([left_i, bottom_i, movavg_w*(1+minute/120),
                                  movavg_h])
        ax2 = fig.add_axes([left_i, bottom_i, movavg_w*(1+minute/120),
                            movavg_h])

        ax3 = fig_suppl.add_axes([left_i+movavg_w+4*cf['gap_w'],
                                  bottom_i, box_w, movavg_h])
        ax4 = fig.add_axes([left_i+movavg_w+4*cf['gap_w'],
                            bottom_i, box_w, movavg_h])

        aplt.plot_effects(ss, avg_e, 'EF score', avg_sum=avg_sum_e, ax=ax1,
                          show=False,
                          win_size=ws, **{'val': [0, 1, 2],
                                          'sliding_overlap': ovr})
        if i == 1:
            ax1.set_xticks([0, 30, 95, 125])
            ax2.set_xticks([0, 30, 95, 125])
        else:
            ax1.set_xticks([0, 30, 90, 120])
            ax2.set_xticks([0, 30, 90, 120])
        ax1.text(1, 1, 'n = {}'.format(int(ss.shape[0]/len(ss.ss_no.unique()))),
                 transform=ax1.transAxes, va='bottom', ha='right')

        aplt.plot_effects(ss, avg_o, 'OC score', ax=ax2, show=False,
                          win_size=ws, **{'val': [0, 1, 2, 3],
                                          'sliding_overlap': ovr})
        ax2.text(1, 1, 'n = {}'.format(int(ss.shape[0]/len(ss.ss_no.unique()))),
                 transform=ax2.transAxes, va='bottom', ha='right')

        aplt.plot_single_bar(ss, avg_e, avg_sum=avg_sum_e, func=als.entry,
                             ax=ax3, pts=pts)
        aplt.plot_single_bar(ss, avg_o, func=als.occupancy, ax=ax4, pts=pts)
        ax3.set_xlabel('')
        ax4.set_xlabel('')

    left_panel = 0.03
    panel_font = 15

    fig.text(left_panel, 0.94, 'A', fontsize=panel_font)
    fig.text(left_panel, 0.61, 'B', fontsize=panel_font)
    fig.text(left_panel, 0.28, 'C', fontsize=panel_font)
    fig_suppl.text(left_panel, 0.94, 'A', fontsize=panel_font)
    fig_suppl.text(left_panel, 0.61, 'B', fontsize=panel_font)
    fig_suppl.text(left_panel, 0.28, 'C', fontsize=panel_font)

    fig.savefig('../figures/figure_3.png', dpi=dpi)
    fig_suppl.savefig('../figures/figure_s1.png', dpi=dpi)
    im = Image.open('../figures/figure_3.png'.format(5))
    im.save('../figures/figure_3.tiff'.format(5), compression='tiff_lzw')
    im = Image.open('../figures/figure_s1.png'.format(5))
    im.save('../figures/figure_s1.tiff'.format(5), compression='tiff_lzw')


if __name__ == '__main__':
    figure_2(dpi=300)
