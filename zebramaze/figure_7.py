from PIL import Image

im = Image.open('../figures/figure_7.png'.format(5))
w, h = im.size
w_max = 8.5*300
h_max = h/w*w_max
im1 = im.resize((int(w_max), int(h_max)), resample=Image.LANCZOS)
im1.save('../figures/figure_7.tiff'.format(5), compression='tiff_lzw')
