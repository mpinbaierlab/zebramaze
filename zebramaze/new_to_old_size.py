import os
import numpy as np
import pandas as pd
import cv2
import fishy_analysis.analyse_video as fav

import zebramaze.md as md

vid_folder = ['G:/vids']

for i, ss in md.ssa.loc[md.xp_new & (md.ssa.ss_no == 0)].iterrows():
    if i < 442+480*3:
        continue
    try:
        co = pd.read_hdf(ss.ss_coords_path)
        v_bool, v_name = fav.check_vid(vid_folder, ss.xp_id+'.avi')
        if not v_bool:
            print("ERROR: Can't find the video for session {}".format(ss.ss_id))
            quit()

        vid = cv2.VideoCapture(os.path.join(v_name, ss.xp_id + '.avi'))
        if not vid.isOpened():
            print("ERROR: Can't open the video for session {}".format(ss.ss_id))
            quit()

        dur = 0
        vid.set(cv2.CAP_PROP_POS_FRAMES, dur)  # if ss_no != 0
        bg = fav.open_bg(ss, vid)

        ind = [64811]

        # half-length of the clipped window where I look for fish's precise
        # position centered around x,y position from coordinate file
        ws = 60
        l_list = []
        for pos in range(108000):
            # vid.set(cv2.CAP_PROP_POS_FRAMES, pos)
            v = vid.read()[1]
            x, y = co.loc[pos, ['x', 'y']]

            # slices to take only the clipped part around x,y position
            xy_clip = [slice(max(0, x-ws), x+ws), slice(max(0, y-ws), y+ws)]
            fish = np.clip((bg[xy_clip[0], xy_clip[1]] -
                            v[xy_clip[0], xy_clip[1]])[:, :, 0], 0, 255)

            # look only at pixels from top 66% of intensity to get rid of noise
            fish[np.where(fish <= fish.max() / 3)] = 0
            cnt = cv2.findContours(fish.astype(np.uint8), cv2.RETR_LIST,
                                   cv2.CHAIN_APPROX_SIMPLE)
            epsilon = 0.04 * cv2.arcLength(cnt[1][0], True)
            cnt_approx = cv2.approxPolyDP(cnt[1][0], epsilon, True)
            l_list.append(cv2.contourArea(cnt[1][0]))
        print(ss.xp_id, np.median(l_list), ss.length,
              np.median(l_list)/ss.length)
    except:
        continue
