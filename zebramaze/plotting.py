import os
import numpy as np
import pandas as pd
import cv2

import scipy.stats as stats

from matplotlib import pyplot as plt
from matplotlib.patches import Polygon
from matplotlib.collections import PatchCollection
from matplotlib.colors import LinearSegmentedColormap
import matplotlib.patches as mpatch
import seaborn as sns

import zebramaze.file_utils as file_utils
import zebramaze.maze_utils as mu
import zebramaze.md as md
import zebramaze.trans as trans
import zebramaze.analysis as als
import zebramaze.create_any_maze as cam

style_dir = os.path.dirname(os.path.abspath(__file__))
plt.style.use(os.path.join(style_dir, 'paper.mplstyle'))
CF = file_utils.load_config()  # load configuration file for figure parameters

COLMAP = {'r': '#D42513', 'b': '#222222', 'g': '#92D050', 'c': '#606060',
          'n': 'grey', 'rr': '#888888'}
num2arm = {0: 'r', 1: 'rr', 2: 'g', 3: 'c'}

file_utils.check_folder('../', 'figures')


def adjust_box_widths(ax, fac):
    """
    Taken from https://stackoverflow.com/questions/31498850/
    set-space-between-boxplots-in-python-graphs-generated-nested-box-plots-with-seab

    Adjust the withs of a seaborn-generated boxplot.
    """

    # iterating through axes artists:
    for c in ax.get_children():

        # searching for PathPatches
        if isinstance(c, mpatch.PathPatch):
            # getting current width of box:
            p = c.get_path()
            verts = p.vertices
            verts_sub = verts[:-1]
            xmin = np.min(verts_sub[:, 0])
            xmax = np.max(verts_sub[:, 0])
            xmid = 0.5*(xmin+xmax)
            xhalf = 0.5*(xmax - xmin)

            # setting new width of box
            xmin_new = xmid-fac*xhalf
            xmax_new = xmid+fac*xhalf
            verts_sub[verts_sub[:, 0] == xmin, 0] = xmin_new
            verts_sub[verts_sub[:, 0] == xmax, 0] = xmax_new

            # setting new width of median line
            for l in ax.lines:
                if np.all(l.get_xdata() == [xmin, xmax]):
                    l.set_xdata([xmin_new, xmax_new])


def plot_protocol(fig, name, seq, left, bottom, w, h, flip=False, grey=False,
                  rot=False, minute=0, control=False, dots_control=False,
                  ages=False, orig=False, replace=False, arr=False, cont=True):
    clrs = np.array([(212, 37, 19), (34, 34, 34), (136, 136, 136)])/255
    gap = 0.01
    if grey:
        panel_num = 4
    elif control or orig:
        panel_num = 1
    elif ages or dots_control:
        panel_num = 2
    else:
        panel_num = 3
    ax_w = (w-(panel_num-1)*gap)/panel_num
    ax1 = fig.add_axes([left, bottom, ax_w, h])
    if orig:
        seq = seq[1:] + seq[0]
    maze = plot_maze(cont, arr, clrs, seq)
    ax1.imshow(maze)
    ax1.axis('off')
    if control or orig:
        return

    ax1.set_title('30 min')

    if not dots_control:
        ax = fig.add_axes([left+ax_w+gap, bottom, ax_w, h])
        maze = plot_maze(cont, arr, clrs, seq, shock=True)
        ax.imshow(maze)
        ax.set_title('60 min', color=plt.get_cmap('Blues')(200))
        ax.axis('off')

    if ages:
        pass
    else:
        ax = fig.add_axes([left + (panel_num-1)*(ax_w + gap), bottom, ax_w, h])
        if flip:
            seq = seq[0] + seq[2] + seq[1]
        elif rot:
            seq = seq[1:] + seq[0]
            clrs = np.vstack((clrs[1:], clrs[0]))
        elif replace:
            seq = 'd' + seq[1:]

        maze_test = plot_maze(cont, arr, clrs, seq)
        ax.imshow(maze_test)
        ax.set_title('30 min')
        ax.axis('off')

    if grey:
        clrs_g = np.array([(0.5, 0.5, 0.5), (0.5, 0.5, 0.5), (0.5, 0.5, 0.5)])
        maze_g = plot_maze(cont, arr, clrs_g, 'ccc')
        ax = fig.add_axes(
            [left + (panel_num - 2) * (ax_w + gap), bottom, ax_w, h])
        ax.imshow(maze_g)
        ax.set_title('{} min'.format(minute), color='k')
        ax.axis('off')

    return ax1


def plot_maze(cont, arr, clrs, seq, shock=False):
    sz = 800
    maze, _, maze_pts = cam.create_rounded_maze(sz, ratio=(1, 1), patt=True,
                                                cont=cont, seq=seq, outer=0,
                                                inter=False, e_ratio=5, arr=arr,
                                                expand_center=0, colors=clrs,
                                                bg=(1, 1, 1))
    if shock:
        shock, shock_mask = cam.draw_shock(int(sz * 0.75), True)
        cam.add_shock(maze, shock, shock_mask, maze_pts[0].mean(0).astype(int))
    return maze


def create_mpl_ax(ax=None, ax_kw=None, **fig_kw):
    """
    Create matplotlib figure and axis

    :param ax: None if newly create axis
    :param ax_kw: Axis keyword parameters
    :param fig_kw: Figure keyword parameters (e.g. figsize)
    :return: Figure and Axis objects
    """
    if ax_kw is None:
        ax_kw = {}
    if ax is None:
        fig = plt.figure(**fig_kw)
        ax = fig.add_subplot(111, **ax_kw)
    else:
        fig = ax.figure

    return fig, ax


def plot_env(ss, ang=0, pad_fac=0.07, arm_col=(0.95, 0.95, 0.95),
             edge=True, center_col=(0.95, 0.95, 0.95), show_shock=True,
             shock_col=(0.59, 0.78, 0.87, 1), noshock_col=(0.8, 0.8, 0.8),
             ax=None, center_offset=False, calib=True, scale=67, arm_set=-1):
    """
    Plots the maze or its parts

    :param ss: a Series with columns |env_geometry|stim_abs|
    :param pad_fac: size of padding around the maze as fraction of maze size
    :param arm_col: face color of arms
    :param center_col: face color of central area
    :param shock_col: face color of shocked arm if show_shock is True
    :param center_offset: True if center of coordinates is moved to maze center
    :param show_shock: True if set a different color for shocked arm
    :param calib: True if show scale bar for real size in cm
    :param scale: number of pixels in a cm
    :param arm_set: a list of arm indices in env_geometry to be plotted
                    -1 if all arms are to be plotted

    :returns ax: axis with plotted maze
    :returns offset: offset for coordinates, [0,0] if center_offset=False
    """
    # XXX add options to style borders

    fig, ax = create_mpl_ax(ax=ax)
    env = ss.env_geometry.copy()
    env[:, :, 0] = env[:, :, 0] - 2*(env[:, :, 0] - env[:, :, 0].mean())

    # add padding of size BUF around the maze
    xmin, ymin = env.min(axis=1).min(axis=0)
    xmax, ymax = env.max(axis=1).max(axis=0)
    buf = max(xmax-xmin, ymax-ymin) * pad_fac
    plt.setp(ax, xlim=(xmin - buf, xmax + buf), ylim=(ymin - buf, ymax + 2*buf))

    M = cv2.getRotationMatrix2D(tuple(mu.maze_center(ss)), ang, 1)

    new_pts = env.copy().astype(np.float32)
    new_pts[:, :, 0] = env[:, :, 0] * M[0, 0] + env[:, :, 1] * M[0, 1] + M[0, 2]
    new_pts[:, :, 1] = env[:, :, 0] * M[1, 0] + env[:, :, 1] * M[1, 1] + M[1, 2]
    env = new_pts
    env[:, :, 0] = env[:, :, 0] - 2 * (env[:, :, 0] - env[:, :, 0].mean())

    edge_colors = {'shock_id': '#D42513', 'left_id': '#92D050',
                   'center': 'black', 'right_id': 'orange'}
    center = mu.get_center(env)  # get points marking the center

    if center_offset:  # reset coordinates relative to maze center
        offset = center.mean(axis=0)
        env = env - offset
        center = center - offset
    else:
        offset = np.array([0, 0])

    if arm_set != -1:  # select the arms to be plotted
        env = env[arm_set]
        # shock id has to be relative to selected arms
        shock_id = arm_set.index(ss.stim_at)
    else:
        shock_id = ss.stim_at
        left_id = ss.stim_at-1
        if left_id < 0:
            left_id += 3
        right_id = ss.stim_at+1
        if right_id == 3:
            right_id -= 3

    patches = []
    for i in range(len(env)):
        # XXX could be better to change line color, not face color?
        if show_shock & (shock_id == i):
            if ss.ss_type == "CONDIT":
                col = shock_col  # red for shocked arm
            else:
                col = noshock_col
        else:
            col = arm_col
        if edge:
            if i == shock_id:
                edge_col = edge_colors['shock_id']
            elif i == right_id:
                edge_col = edge_colors['right_id']
            elif i == left_id:
                edge_col = edge_colors['left_id']
        else:
            edge_col = (0, 0, 0)
        patches.append(Polygon(env[i], True, facecolor=col, linewidth=0.4,
                               edgecolor=edge_col))

    # draw central area
    patches.append(Polygon(center, True, fc=center_col, ec=edge_colors[
        'center'], linewidth=0.4))
    p = PatchCollection(patches, match_original=True)
    ax.add_collection(p)

    # add scale bar
    if calib:
        ax = add_scalebar(ax, xmin, ymin - buf*2)

    ax.set_aspect('equal')

    return ax, offset


def add_scalebar(ax, x, y, txt='1 cm', scale=70):
    """
    Plot a scale bar on the axis

    :param ax:
    :param x:
    :param y:
    :param txt:
    :param scale:
    :return ax: axis with drawn scale bar
    """
    fig = ax.get_figure()

    # get size of axis and scale font
    bbox = ax.get_window_extent().transformed(fig.dpi_scale_trans.inverted())
    width, height = bbox.width, bbox.height
    # 4.8 is window height for font 15
    # 6.2 is window width for font 15
    h_scale = np.sqrt(height / 4.8)
    w_scale = width / 6.2

    ax.plot([x, x + scale], [y, y], 'k-', lw=1, clip_on=False)
    ax.text(x + scale + 7 / w_scale, y - 2 * h_scale, txt, va='center',
            fontdict={'size': 15 * h_scale, 'color': '#555555'})
    return ax


def plot_traj(xp_id, ax1=None, save=True, split_test=False, split_shock=False,
              join=False, split_control=False, save_folder='fish_plots'):
    """
    Plot trajectory for each session of an experiment and save it

    :param xp_id: experiment id
    :param save: True if plots are to be saved
    :param split_test: True if test sessions are to be split in two time
                    intervals (to see whether there's a dynamic process
                    (extinction/forgetting))
    :param join: True if all session trajectories are to be in one joined plot
    :param save_folder: name of the folder in md.DATA_DIR
    """

    # XXX right now test splitting is too arbitrary; need to think of a better
    #  way to split any session into any number of parts, and also be able to
    #  join into one plot

    # XXX need to add sorting of sub_ssa by ss_no, session must be ordered!

    # check if folder exists and create one if not
    traj_folder = file_utils.check_folder(md.DATA_DIR, save_folder)

    ssa = md.ss_xp_sj()
    # ssa = pd.read_csv('C:\Users\yashina\data\shock_5mm.csv')
    sub_ssa = ssa.loc[ssa.xp_id == xp_id]

    if join:
        if save:
            im_address = os.path.join(traj_folder, xp_id + '.png')
            if os.path.exists(im_address):
                return
        images = []  # list for images which will be joined

    j = 0
    all_ss = sub_ssa.shape[0]
    for i, ss in sub_ssa.iterrows():
        if (ss.ss_no == 0) & split_control:
            parts = 4.0
        elif (ss.ss_no == all_ss-1) & split_test:
            parts = 2.0  # two plots if we split test phase
        elif (ss.ss_no == 1) & split_shock:
            parts = 4.0
        else:
            parts = 1.0
        for p_id in range(int(parts)):
            if (not join) & save:
                image_folder = file_utils.check_folder(traj_folder, id)
                im_address = os.path.join(image_folder,
                                          ss.ss_id + str(p_id) + '.png')
                if os.path.exists(im_address):
                    continue

            # creates plots with trajectory in small black dots (s=1)
            # it's possible to create other sizes and transparencies of dots,
            #  but then this has to be taken into account when join=True
            for s, a, n in [(1, 1, '')]:
                # plot maze layout and an individual session
                ax, offset = plot_env(sub_ssa.iloc[j])
                fig, ax = plot_ss(ss, ax=ax, x0=p_id/parts, x1=(p_id + 1)/parts,
                                  **{'s': s, 'alpha': 1})

                if join:
                    fig.set_facecolor('white')
                    buf = plot2array(fig)

                    # crop x-axis, so joined image is more compact
                    buf_x = max(100,
                                int((buf.shape[1] - sum(ax.get_xlim())) / 2))
                    buf = buf[:, buf_x:-buf_x, :]

                    images.append(buf)
                elif save:
                    fig.savefig(im_address)
                else:
                    plt.show()
                plt.close(fig)
        j += 1

    if join:
        im = np.hstack(images)
        plt.axis('off')
        if save:
            plt.imsave(im_address, im)
            return None
        else:
            if not ax1:
                ax1 = plt.subplot('111')
            ax1.imshow(im)
            return ax1


# from http://www.icare.univ-lille1.fr/node/1141
def plot2array(f):
    """
    Convert figure into an RGB array

    :param f: Figure object

    :returns buf: array RGB image from f
    """
    w, h = f.canvas.get_width_height()
    f.canvas.draw()

    buf = np.array(f.canvas.renderer._renderer)

    #buf = np.fromstring(f.canvas.get_renderer().tostring_rgba(),
    # dtype=np.uint8)
    #buf.shape = (h, w, 4)
    # canvas.tostring_argb give pixmap in ARGB mode.
    #  Roll the ALPHA channel to have it in RGBA mode
    # buf = np.roll(buf, 3, axis=2)
    return buf


def plot_ss(ss, ang=0, ax=None, x_n=None, y_n=None, x0=0.0, x1=1.0,
            **scatter_kw):
    """
    Plot (part of a) trajectory from a session

    :param ss: dict-like with information about session
    :param ax:
    :param x0: float 0 <= x0 <= 1 relative starting position in coordinates
    :param x1: float 0 <= x1 <= 1 relative ending position in coordinates

    :returns ax:
    :returns fig:
    """

    # XXX need to resize figure to (10, 10) if ax != None

    fig, ax = create_mpl_ax(ax, **{'figsize': (10, 10)})
    coords = pd.read_hdf(ss.ss_coords_path)
    if x_n is None:
        x_n, y_n = 'x', 'y'

    M = cv2.getRotationMatrix2D(tuple(mu.maze_center(ss)), ang, 1)
    xy_new = coords[[x_n, y_n]]
    xy_new.loc[:, x_n] = coords[x_n] * M[0, 0] + coords[y_n] * M[0, 1] + M[0, 2]
    xy_new.loc[:, y_n] = coords[x_n] * M[1, 0] + coords[y_n] * M[1, 1] + M[1, 2]
    coords.loc[:, [x_n, y_n]] = xy_new

    co_sh = coords.shape[0]
    coords = coords.iloc[int(co_sh * x0):int(co_sh * x1)]
    x_array = coords[y_n].values
    y_array = coords[x_n].values

    if 'color' not in scatter_kw.keys():
        scatter_kw['color'] = 'k'

    ax.scatter(x_array, y_array, **scatter_kw)
    ax.axis('off')

    return fig, ax


def plot_windows(ssub, func=als.occupancy, ax=None, win_size=10, fps=60,
                 turn_orig=False, x_labels=None,
                 two_one=False, sgf=(), slide=None, kind='boxplot', l_e=False,
                 drop=(4,), pts=(19, 79, 109), ):
    windows = []
    ss_len = len(ssub.ss_no.unique())
    if x_labels is None:
        if ss_len > 3:
            x_labels = ['habit.', 'condit.', 'no patterns', 'test']
        else:
            x_labels = ['habituation', 'conditioning', 'test']
    win_size *= 60*fps
    windows = pd.DataFrame(windows, columns=['val', 'session', 'arm', 'xp'])
    if slide is None:
        for rel in [0, 1, 2]:
            for g, gg in ssub.groupby('xp_id'):
                for i, ss in gg.iterrows():
                    if ss.ss_type == 'GREY':
                        continue
                    co = pd.read_hdf(ss.ss_coords_path)
                    if two_one:
                        patt_seq = mu.patt_2_1(ss.env_patterns, ss.stim_at)+[3]
                        co.arm = co.arm.apply(lambda x: patt_seq.index(x))
                    elif turn_orig & (ss.ss_no == 2):
                        co.arm = mu.relative(co.arm.values,
                                             int(gg.loc[gg.ss_no == 1,
                                                        'stim_at'].values[0]),
                                             arm_no=len(ss.env_patterns))
                    else:
                        co.arm = mu.relative(co.arm.values, int(ss.stim_at),
                                             arm_no=len(ss.env_patterns))
                    if ss.ss_no != 2:
                        co_slice = co.iloc[-win_size:]
                    else:
                        co_slice = co.iloc[:win_size]

                    if l_e:
                        stays = trans.raw_stays(co_slice)
                        merged = trans.merge_arms(stays, drop=drop)
                        merged = trans.merge_arms(trans.long_enough(merged),
                                                  drop=[])
                        arm_seq = trans.stays2sequence(merged)[np.newaxis, :]
                    else:
                        arm_seq = co_slice.arm.values[np.newaxis, :]
                    window_row = [func(arm_seq, rel), x_labels[ss.ss_no], rel, g]
                    windows.append(window_row)
        windows.loc[:, 'val'] = windows.val.astype(float)
    else:
        for i, sl in enumerate(slide):
            for j, sess in enumerate(pts):
                win = pd.DataFrame({'val': sl.iloc[sess], 'xp': sl.columns,
                                    'arm': np.ones([sl.shape[1]])*i,
                                    'session': x_labels[j]})
                windows = pd.concat([windows, win], ignore_index=True)
    fig, ax1 = create_mpl_ax(ax=ax, **{'figsize': (8, 8)})
    scale = ax1.get_window_extent().transformed(
        fig.dpi_scale_trans.inverted()).width
    if kind == 'violin':
        #sns.swarmplot(hue='arm', x='session', y='val', ax=ax1, data=windows,
        #              dodge=True, palette=['#000000', '#000000', '#000000'])
        ax1 = sns.violinplot(hue='arm', x='session', y='val', ax=ax1,
                             data=windows, inner='box', cut=0, bw=.2,
                             palette=['#D42513', 'orange', '#92D050', 'black'])
        import matplotlib.collections as coll
        for art in ax1.get_children():
            if isinstance(art, coll.PolyCollection):
                art.set_alpha(0.7)
    if kind == 'boxplot':
        sns.swarmplot(hue='arm', x='session', y='val', ax=ax1, data=windows,
                      dodge=True, palette=['#D42513', 'orange', '#92D050',
                                           'black'],
                      linewidth=0.5, edgecolor='black', size=3)
        sns.boxplot(hue='arm', x='session', y='val', ax=ax1, data=windows,
                    palette=['#000000', '#000000', '#000000'], linewidth=1.5,
                    showfliers=False)
        for i, artist in enumerate(ax1.artists):
            # Set the linecolor on the artist to the facecolor,
            #  and set the facecolor to None
            col = artist.get_facecolor()
            artist.set_edgecolor(col)
            artist.set_facecolor('#DDDDDD')

            # Each box has 6 associated Line2D objects (to make the whiskers, fliers, etc.)
            # Loop over them here, and use the same colour as above
            for j in range(i * 5, i * 5 + 5):
                line = ax1.lines[j]
                line.set_color(col)
                line.set_mfc(col)
                line.set_mec(col)
    print(windows.groupby(['session', 'arm']).mean())
    print(windows.groupby(['session', 'arm']).std() /
          np.sqrt(len(windows.xp.unique())))
    ax1.set_ylim(-0.05, 1.01)
    ax1.set_yticks(np.arange(0, 1.2, 0.2))
    ylvl = ax1.get_ylim()[1] + 0.1
    for i, sgf_i in enumerate(sgf):
        print(sgf[-i-1])
        ax1.plot((ss_len-i-1 - 0.25, ss_len-i-1 + 0.125), (ylvl, ylvl), lw=1,
                 c=(0.25, 0.25, 0.25), clip_on=False)
        ax1.plot((ss_len-i-1-0.25, ss_len-i-1-0.25), (ylvl, ylvl-0.05), lw=1,
                 c=(0.25, 0.25, 0.25), clip_on=False)
        ax1.plot((ss_len-i-1+0.125, ss_len-i-1+0.125), (ylvl, ylvl-0.025),
                 lw=1, c=(0.25, 0.25, 0.25), clip_on=False)
        ax1.plot((ss_len-i-1, ss_len-i-1+0.125, ss_len-i-1+0.25),
                 (ylvl-0.05, ylvl-0.025, ylvl-0.05),
                 lw=1, c=(0.25, 0.25, 0.25), clip_on=False)
        if sgf[-i-1] < 0.01:
            msg = '$p<0.01$'
        else:
            msg = '$p={:.02f}$'.format(sgf[-i-1])
        ax1.text(ss_len-i-1 - 0.0625, ylvl+0.02/0.8*ylvl,
                 msg, color=(0.25, 0.25, 0.25), ha='center')
    if func == als.occupancy:
        windows.loc[:, 'val'] /= win_size
        yax = 'Occupancy'
    else:
        yax = 'Entry frequency'
    ax1.set_ylabel(yax)
    ax1.legend_.remove()
    # plt.show()


def plot_sliding(ssub, func, avg=None, avg_sum=None, ax=None, excl=True,
                 show=True, name='', individ=False, sliding_overlap=540,
                 win_size=600, fg_sz=(8, 8), show_val=None, total_trans=False,
                 patch_names=None, arrow=False, arrow1=False, ymax=0.7, **kw):
    """
    Plot sliding window using a function for a subset of sessions

    :param ssub:
    :param func:
    :param ax:
    :param excl:
    :param show:
    :param name:
    :param individ:
    :param sliding_overlap:
    :param win_size: in seconds
    :param fg_sz:
    :param show_val:
    :param total_trans:
    :param kw:
    :return:
    """
    fr = 60
    if show_val is None:
        show_val = []

    patch_h = 0.08

    kw.update({'win_size': win_size, 'sliding_overlap': sliding_overlap})
    if avg is None:
        avg, avg_sum = als.moving_avg(ssub, func=func,
                                      exclude_overstayers=excl, **kw)
    fig, ax1 = create_mpl_ax(ax=ax, **{'figsize': fg_sz})

    # Evaluate an existing colormap from 0.5 (midpoint) to 1 (upper end)
    cmap = plt.get_cmap('Blues')
    colors = cmap(np.linspace(0, 0.05, cmap.N // 20))
    colors[0] = [1, 1, 1, 1]

    # Create a new colormap from those colors
    cmap2 = LinearSegmentedColormap.from_list('Upper Half', colors)
    sh_start, sh_end, sess_start, sess_end = 0, 999, 0, 0
    xmax = ssub.xp_duration.max() / fr / 60
    pts_sec = 60 / (win_size-sliding_overlap)
    win_offset = win_size/2/(win_size-sliding_overlap)

    if len(show_val) == 0:
        show_val = range(len(avg))
        name_arm = ''
    else:
        name_arm = '\nin arms {}'.format(show_val)

    for no in range(len(avg)-1, -1, -1):
        if no not in show_val:
            continue
        arm = num2arm[no]
        m = avg[no].mean(axis=1)
        s = avg[no].std(axis=1) / np.sqrt(avg[no].shape[1])
        ax1.plot((m.index[:-1] + win_offset)/pts_sec,
                 m.values[:-1], c=COLMAP[arm])

        if individ:
            for trace in avg[no].columns:
                ax1.plot((avg[no][trace].index[:-1] + win_offset)/pts_sec,
                         avg[no][trace].values[:-1], c=COLMAP[arm], alpha=0.3)
        else:
            # XXX fix x axis labeling, right now depends on sliding window
            # size!!!
            ax1.fill_between((m.index[:-1] + win_offset)/pts_sec,
                             m[:-1] - s[:-1], m[:-1] + s[:-1],
                             color=COLMAP[arm], alpha=0.4)

    if total_trans:
        sldng_sum_all = avg_sum / avg_sum.sum()
        m_sum = sldng_sum_all.mean(axis=1)
        s_sum = sldng_sum_all.std(axis=1) / np.sqrt(sldng_sum_all.shape[1])
        ax2 = ax1.twinx()
        ax2.plot((m_sum.index[:-1] + win_offset)/pts_sec,
                 m_sum.values[:-1], '--', c='k')
        ax2.fill_between((m_sum.index[:-1] + win_offset)/pts_sec,
                         m_sum[:-1] - s_sum[:-1], m_sum[:-1] + s_sum[:-1],
                         color='k', alpha=0.4)
        ax2.set_ylim([0, m_sum.max() + 0.01])

    # XXX add legend!!!

    if (kw in ['abs_num']) and kw['abs_num']:
        ymax = plt.gca().get_ylim()[1]

    patch_borders = []
    tri_patch = []
    # gray zone to show where sliding window was mixed - spanning over
    # different sessions
    tshock = 0
    all_ss = len(ssub.ss_no.unique())

    for i in range(all_ss):
        tshock += ssub.iloc[i]['ss_duration'].mean() / fr
        nshock = ((tshock - win_size) / (win_size - sliding_overlap) +
                  win_offset)/pts_sec
        if i == 0:
            sh_start = nshock + 2*win_offset/pts_sec
            patch_borders.append([sess_start, nshock, (0.8, 0.8, 0.8)])
        if i == 1:
            col1 = (0.8, 0.8, 0.8)
            if ssub.iloc[i].ss_type == 'CONDIT':
                sh_end = nshock
                col2 = cmap(100)
            else:
                col2 = (0.5, 0.5, 0.5)
            patch_borders.append([sess_start, nshock - sess_start, col2])

        if (ssub.iloc[i].ss_type == 'GREY') & (all_ss > 2):

            if ssub.iloc[i-1].ss_type == 'CONDIT':
                col1 = cmap(100)
            else:
                col1 = (0.8, 0.8, 0.8)
            col2 = (0, 0, 0)
            patch_borders.append([sess_start, nshock - sess_start, col2])
        if (i == all_ss-1) & (all_ss > 2):
            patch_borders.append(
                [sess_start, ssub.iloc[0].xp_duration / fr / 60 - sess_start,
                 (0.8, 0.8, 0.8)])
            col2 = 0.8, 0.8, 0.8
            if ssub.iloc[i-1].ss_type == 'CONDIT':
                col1 = cmap(100)
            else:
                col1 = (0, 0, 0)

        if i != 0:
            tri_patch.append(([[sess_end, ymax], [sess_end, ymax-patch_h],
                               [sess_start, ymax-patch_h]], col1))
            tri_patch.append(([[sess_end, ymax], [sess_start, ymax-patch_h],
                               [sess_start, ymax]], col2))
            ax1.vlines([sess_end, sess_start], 0, ymax, cmap(100), '--')
            if (all_ss == 4) & (i == 3):
                ax1.vlines([sess_end], 0, ymax, (0, 0, 0), '--')
        if i == all_ss - 1:
            patch_borders[-1][1] = xmax - patch_borders[-1][0]
        sess_start = nshock + 2*win_offset/pts_sec
        sess_end = nshock

    if patch_names is None:
        if ssub.iloc[0].xp_type == 'control':
            patch_names = ['control']
        elif ssub.iloc[1].ss_type == 'CONDIT':
            if all_ss == 4:
                patch_names = ['habit.', 'conditioning', '', 'test']
            else:
                patch_names = ['habit.', 'conditioning', 'test']
        else:
            patch_names = ['habit.', 'test']
    for pb, pn in zip(patch_borders, patch_names):
        rect = mpatch.Rectangle((pb[0], ymax-patch_h), pb[1], patch_h,
                                facecolor=pb[2])
        ax1.add_artist(rect)
        rx, ry = rect.get_xy()
        cx = rx + rect.get_width() / 2
        cy = ry + rect.get_height() / 2
        scale = ax1.get_window_extent().transformed(
            fig.dpi_scale_trans.inverted()).height
        ax1.annotate(pn, (cx, cy), color='k', ha='center', va='center')
        if arrow & (pn == 'test'):
            annot_point = avg[0].mean(axis=1)[int(pb[0] - win_offset)]
            ax1.annotate("", xy=(pb[0], annot_point),
                         xytext=(pb[0] + pb[1], annot_point/2),
                         arrowprops=dict(color='k', headlength=5, headwidth=5,
                                         width=1.5))
        if arrow1 & (pn == 'test'):
            annot_point = avg[1].mean(axis=1)[int(pb[0] - win_offset)]
            ax1.annotate("", xy=(pb[0], annot_point),
                         xytext=(pb[0] + pb[1], annot_point/2),
                         arrowprops=dict(color='k', headlength=5, headwidth=5,
                                         width=1.5))
    if ssub.iloc[0].xp_type != 'control':
        for tri in tri_patch:
            triag = mpatch.Polygon(np.array(tri[0]), facecolor=tri[1])
            ax1.add_artist(triag)
        if ssub.iloc[1].ss_type == 'CONDIT':
            ax1.fill_between([sh_start, min(sh_end, xmax)], 0, ymax,
                             color=cmap2(100), alpha=0.5)

    ax1.set_ylim(0, ymax)
    ax1.set_xlim(0, xmax)
    if not ax:
        ax1.set_title(name + name_arm)
    if func == als.entry:
        if (kw in ['abs_num']) and kw['abs_num']:
            ylbl = 'Number of entries'
        else:
            ylbl = 'Entry frequency'
    else:
        ylbl = 'Occupancy'
    plt.setp(ax1, xlabel='Experiment duration [min]', ylabel=ylbl)

    ttl = ax1.title
    ttl.set_position([.5, 1.03])
    ax1.grid(False)
    print(ax1.get_window_extent().transformed
          (fig.dpi_scale_trans.inverted()).height)

    if show:
        plt.show()


def plot_effects(ssub, avg, y_name, avg_sum=None, ax=None, show=True,
                 sliding_overlap=540,
                 win_size=600, fg_sz=(8, 8), se=True,
                 patch_names=None, arrow=False, arrow1=False, ymax=0.5,
                 ymin=-0.5, **kw):
    """
    Plot sliding window using a function for a subset of sessions

    :param ssub:
    :param ax:
    :param show:
    :param name:
    :param sliding_overlap:
    :param win_size: in seconds
    :param fg_sz:
    :param show_val:
    :param kw:
    :return:
    """
    fr = 60
    patch_h = 0.1

    kw.update({'win_size': win_size, 'sliding_overlap': sliding_overlap})
    fig, ax1 = create_mpl_ax(ax=ax, **{'figsize': fg_sz})

    # Evaluate an existing colormap from 0.5 (midpoint) to 1 (upper end)
    cmap = plt.get_cmap('Blues')
    colors = cmap(np.linspace(0, 0.05, cmap.N // 20))
    colors[0] = [1, 1, 1, 1]

    # Create a new colormap from those colors
    cmap2 = LinearSegmentedColormap.from_list('Upper Half', colors)
    sh_start, sh_end, sess_start, sess_end = 0, 999, 0, 0
    xmax = ssub.xp_duration.max() / fr / 60
    pts_sec = 60 / (win_size-sliding_overlap)
    win_offset = win_size/2/(win_size-sliding_overlap)

    eff = avg[0] - (avg[1] + avg[2])/2
    if avg_sum is not None:
        m = eff.multiply(avg_sum.divide(avg_sum.sum(1), 0), 0).sum(1)
    else:
        m = eff.mean(axis=1)
    s = eff.std(axis=1)
    if se:
        s /= np.sqrt(eff.shape[1])
    ax1.plot((m.index[:-1] + win_offset)/pts_sec, m.values[:-1], c='k')
    ax1.fill_between((m.index[:-1] + win_offset)/pts_sec, m[:-1] - s[:-1],
                     m[:-1] + s[:-1], color='k', alpha=0.4)

    # XXX add legend!!!

    if (kw in ['abs_num']) and kw['abs_num']:
        ymax = plt.gca().get_ylim()[1]

    patch_borders = []
    tri_patch = []
    # gray zone to show where sliding window was mixed - spanning over
    # different sessions
    tshock = 0
    all_ss = len(ssub.ss_no.unique())

    for i in range(all_ss):
        tshock += ssub.iloc[i]['ss_duration'].mean() / fr
        nshock = ((tshock - win_size) / (win_size - sliding_overlap) +
                  win_offset)/pts_sec
        if i == 0:
            sh_start = nshock + 2*win_offset/pts_sec
            patch_borders.append([sess_start, nshock, (0.8, 0.8, 0.8)])
        if i == 1:
            col1 = (0.8, 0.8, 0.8)
            if ssub.iloc[i].ss_type == 'CONDIT':
                sh_end = nshock
                col2 = cmap(100)
            else:
                col2 = (0.5, 0.5, 0.5)
            patch_borders.append([sess_start, nshock - sess_start, col2])

        if (ssub.iloc[i].ss_type == 'GREY') & (all_ss > 2):

            if ssub.iloc[i-1].ss_type == 'CONDIT':
                col1 = cmap(100)
            else:
                col1 = (0.8, 0.8, 0.8)
            col2 = (0, 0, 0)
            patch_borders.append([sess_start, nshock - sess_start, col2])
        if (i == all_ss-1) & (all_ss > 2):
            patch_borders.append(
                [sess_start, ssub.iloc[0].xp_duration / fr / 60 - sess_start,
                 (0.8, 0.8, 0.8)])
            col2 = 0.8, 0.8, 0.8
            if ssub.iloc[i-1].ss_type == 'CONDIT':
                col1 = cmap(100)
            else:
                col1 = (0, 0, 0)

        if i != 0:
            tri_patch.append(([[sess_end, ymax], [sess_end, ymax-patch_h],
                               [sess_start, ymax-patch_h]], col1))
            tri_patch.append(([[sess_end, ymax], [sess_start, ymax-patch_h],
                               [sess_start, ymax]], col2))
            ax1.vlines([sess_end, sess_start], ymin, ymax, cmap(100), '--')
            if (all_ss == 4) & (i == 3):
                ax1.vlines([sess_end], ymin, ymax, (0, 0, 0), '--')
        if i == all_ss - 1:
            patch_borders[-1][1] = xmax - patch_borders[-1][0]
        sess_start = nshock + 2*win_offset/pts_sec
        sess_end = nshock

    if patch_names is None:
        if ssub.iloc[0].xp_type == 'control':
            patch_names = ['control']
        elif ssub.iloc[1].ss_type == 'CONDIT':
            if all_ss == 4:
                patch_names = ['habit.', 'conditioning', '', 'test']
            else:
                patch_names = ['habit.', 'conditioning', 'test']
        else:
            patch_names = ['habit.', 'test']
    for pb, pn in zip(patch_borders, patch_names):
        rect = mpatch.Rectangle((pb[0], ymax-patch_h), pb[1], patch_h,
                                facecolor=pb[2])
        ax1.add_artist(rect)
        rx, ry = rect.get_xy()
        cx = rx + rect.get_width() / 2
        cy = ry + rect.get_height() / 2
        scale = ax1.get_window_extent().transformed(
            fig.dpi_scale_trans.inverted()).height
        ax1.annotate(pn, (cx, cy), color='k', ha='center', va='center')
        if arrow & (pn == 'test'):
            annot_point = avg[0].mean(axis=1)[int(pb[0] - win_offset)]
            ax1.annotate("", xy=(pb[0], annot_point),
                         xytext=(pb[0] + pb[1], annot_point/2),
                         arrowprops=dict(color='k', headlength=5, headwidth=5,
                                         width=1.5))
        if arrow1 & (pn == 'test'):
            annot_point = avg[1].mean(axis=1)[int(pb[0] - win_offset)]
            ax1.annotate("", xy=(pb[0], annot_point),
                         xytext=(pb[0] + pb[1], annot_point/2),
                         arrowprops=dict(color='k', headlength=5, headwidth=5,
                                         width=1.5))
    if ssub.iloc[0].xp_type != 'control':
        for tri in tri_patch:
            triag = mpatch.Polygon(np.array(tri[0]), facecolor=tri[1])
            ax1.add_artist(triag)
        if ssub.iloc[1].ss_type == 'CONDIT':
            ax1.fill_between([sh_start, min(sh_end, xmax)], ymin, ymax,
                             color=cmap2(100), alpha=0.5)
    ax1.hlines([0], 0, xmax, 'k', '--', linewidth=1)
    ax1.set_ylim(ymin, ymax)
    ax1.set_xlim(0, xmax)
    ylbl = y_name
    plt.setp(ax1, xlabel='Experiment duration [min]', ylabel=ylbl)

    ttl = ax1.title
    ttl.set_position([.5, 1.03])
    ax1.grid(False)
    print(ax1.get_window_extent().transformed
          (fig.dpi_scale_trans.inverted()).height)

    if show:
        plt.show()


def plot_single_bar(ssub, avg, avg_sum=None, func=als.occupancy, ax=None,
                    x_labels=None, pts=(19, 79, 109), pref=False,
                    n=10 ** 6):

    windows = []
    ss_len = len(ssub.ss_no.unique())
    cmap = plt.get_cmap('Blues')
    if x_labels is None:
        if ss_len == 4:
            x_labels = ['habit.\nlast 5\'', 'condit.\nlast 5\'',
                        'no patterns\n5\'', 'test\nfirst 5\'']
        elif len(pts) > 3:
            x_labels = ['habit.\nlast 5\'', 'condit.\nlast 5\'',
                        'test\nfirst 5\'', 'test\nsecond 5\'',
                        'test\nlast 5\'']
        else:
            x_labels = ['habituation\nlast 5\'', 'conditioning\nlast 5\'',
                        'test\nfirst 5\'']

    windows = pd.DataFrame(windows, columns=['val', 'session', 'xp'])
    eff = avg[0] - (avg[1] + avg[2]) / 2

    avg_array = []
    for a in avg:
        avg_array.append(a.values)
    avg_array = np.array(avg_array)

    sign_list = []
    shuff_list = []
    avg_swap = avg_array.swapaxes(0, 1)
    for p in pts:
        if avg_sum is not None:
            shuff, sign = als.calc_signif(avg_swap[p], avg_sum=avg_sum.iloc[p],
                                          n=n)
        else:
            shuff, sign = als.calc_signif(avg_swap[p], n=n)
        sign_list.append(sign)
        shuff_list.append(shuff)

    for j, sess in enumerate(pts):
        win = pd.DataFrame({'val': eff.iloc[sess], 'xp': 'Experiment',
                            'session': x_labels[j]})
        windows = pd.concat([windows, win], ignore_index=True)
        shuff = shuff_list[j]
        win = pd.DataFrame({'val': shuff, 'xp': 'Shuffle',
                            'session': x_labels[j]})
        windows = pd.concat([windows, win], ignore_index=True)

    fig, ax1 = create_mpl_ax(ax=ax, **{'figsize': (8, 8)})
    ax1.set_ylim(-0.55, 1.1)

    ax1 = sns.violinplot(x='session', y='val', ax=ax1, inner=None,
                         data=windows.loc[windows.xp == 'Shuffle'],
                         cut=0, bw=.2, color='#888888')
    import matplotlib.collections as coll
    for art in ax1.get_children():
        if isinstance(art, coll.PolyCollection):
            art.set_alpha(0.7)

    sns.swarmplot(x='session', y='val', ax=ax1, data=windows.loc[
        windows.xp == 'Experiment'],
                  dodge=True, palette=['#555555', cmap(100), '#555555',
                                       '#555555', '#555555', '#555555'],
                  linewidth=0.5, edgecolor='black', size=3)

    for xi in range(len(pts)):
        if avg_sum is not None:
            m = (windows.loc[(windows.session == x_labels[xi]) &
                             (windows.xp == 'Experiment'), 'val'] *
                 avg_sum.iloc[pts[xi]].divide(avg_sum.iloc[pts[xi]].
                 sum()).values).sum()
        else:
            m = windows.loc[(windows.session == x_labels[xi]) &
                            (windows.xp == 'Experiment'), 'val'].mean()
        shuff = windows.loc[(windows.session == x_labels[xi]) &
                            (windows.xp == 'Shuffle'), 'val'].values
        # sign = (m > shuff).sum()/shuff.shape[0]
        sign = als.sign_gaus(m, shuff, pref=pref)
        if sign < 0.05:
            col = 'red'
        else:
            col = 'k'
        if sign == 0:
            msg = '$p = $0 ({:.0e})'.format(als.sign_gaus(m, shuff))
        elif sign < 0.01:
            msg = '$p = ${:.0e}'.format(sign)
        else:
            msg = '$p = ${:.2f}'.format(sign)
        ax1.hlines(m, xi-0.25, xi+0.25, col, lw=2)
        ax1.text(xi, 1.1, msg, ha='center', va='bottom')

    ax1.set_yticks([-0.5, 0, 0.5, 1])
    if func == als.occupancy:
        yax = 'OC score'
    else:
        yax = 'EF score'
    ax1.set_ylabel(yax)


def plot_occu_bar(ssub, ax=None, rel=True, versus=None, patt=False,
                  two_one=False, ax_title='', omit=(4,)):
    """
    Plot occupancy bar plot: a bar for each arm of interest

    :param ssub: a DataFrame to iterate through, with information about sessions
    :param ax: Axis object
    :param rel: True if relative arm numbering to be used
    :param versus: None or int, if int plot relative occupancy of this arm
                    against relative occupancy in the rest of the arms
    :param patt: True if arms are grouped by their patterns
    :param two_one: True if pattern-based numeration in experiments with 2-1
                    patterns is used
    :param ax_title: title of the plot
    :param omit: a list of arm to be omitted (and thus occupancies of the
                    shown arms are summing to 1)
    :return:
    """

    if omit is None:
        omit = []

    if patt and (versus is not None):
        raise ValueError("Plotting by pattern (patt=True) is incompatible with "
                         "plotting against an arm (versus!=None)")

    if patt:
        # XXX need to check that only 3 patterns are used in experiments
        # patt_list = list(set(ssub.env_patterns))
        patt_list = ssub.env_patterns.values[0]
    else:
        patt_list = None
    hist_all, bins, f_p = als.occupancy_bar(ssub, versus=versus, patt=patt,
                                            rel=rel, two_one=two_one,
                                            input_patt=patt_list, omit=omit)
    # set tick values for plotting
    if versus is not None:
        # if we plot one arm against others, there're only two columns
        ticks = [versus, 'other']
    elif patt:
        ticks = list(patt_list) + ['center']
    else:
        ticks = np.append(((bins+0.25)[:-2]).astype(int), 'center')

    # x_ticks = (bins[:-1].min() + np.arange(bins.shape[0] - 1)).tolist()

    fig, ax = create_mpl_ax(ax, **{'figsize': (10, 8)})
    flierprops = dict(marker='o', markersize=1)
    sns.swarmplot(data=hist_all, ax=ax, palette=['k'], size=2)
    sns.boxplot(data=hist_all, ax=ax, palette=['silver']*4,
                flierprops=flierprops)

    plt.setp(ax, xlabel='Arms of the maze', ylabel='Occupancy')
    ax.set_title(ax_title)
    ax.set_xticklabels(ticks)
    lvl = 1.05
    ax.plot([0, 0, 2, 2], [-0.02 + lvl, lvl, lvl, -0.02 + lvl],
            lw=1, c=(0.25, 0.25, 0.25), clip_on=False)
    ax.plot([1, 1], [-0.02 + lvl, lvl], lw=1, c=(0.25, 0.25, 0.25),
            clip_on=False)
    ax.text(1, lvl + 0.02, '$p$ = {:.02f}'.format(f_p[1]),
            ha='center', va='bottom')


def plot_reac_bar(reac_dict, dx, ax=None):

    if ax is None:
        f = plt.figure(figsize=(3, 5))
        ax = f.add_axes([0.2, 0.35, 0.7, 0.6])
    reac_df = pd.DataFrame()
    types = [1, 1, 3, 3, 5, 5]
    box_names = ['Aligned', 'Misaligned', 'To anode', 'To cathode',
                 'Into arm', 'Out of arm']
    for i, n in enumerate(box_names):
        reac_i = pd.DataFrame()
        reac_i['reac_val'] = np.array(reac_dict[n])*dx/CF['BORDERS']['scale']
        reac_i['type'] = types[i]
        reac_i['reac_name'] = i % 2
        reac_df = pd.concat([reac_df, reac_i], 0, ignore_index=True)
    ax2 = sns.boxplot(x='type', y='reac_val', hue='reac_name', data=reac_df,
                      ax=ax, palette=['grey'], showfliers=False,
                      linewidth=1, order=np.arange(7))
    adjust_box_widths(ax2, 0.7)

    ax2.set_xlabel('Orientation')
    ax2.set_xticks([0.75, 1.25, 2.75, 3.25, 4.75, 5.25])
    ax2.set_xticklabels(box_names, rotation=40, ha='right')
    ax2.set_ylabel('Bout amplitude [cm]')
    ax2.legend_.remove()
    ymax = ax2.get_ylim()[1]

    for i, n in enumerate([1, 3, 5]):
        reac_i = reac_df.loc[reac_df.type == n]
        reac_0 = reac_i.loc[reac_i.reac_name == 0, 'reac_val']
        reac_1 = reac_i.loc[reac_i.reac_name == 1, 'reac_val']
        _, pi = stats.ttest_ind(reac_0, reac_1, nan_policy='omit')
        if pi < 0.01:
            msg = '$p<$0.01'
        else:
            msg = '$p=${:.02f}'.format(pi)
        print('{}: {}; {}: {}, p-value = {}'.format(box_names[i*2],
              reac_0.mean(), box_names[i*2+1], reac_1.mean(), pi))
        ax2.plot([i*2+0.75, i*2+0.75, i*2+1.25, i*2+1.25],
                 [ymax, ymax+0.02, ymax+0.02, ymax], lw=1, color='#888888',
                 clip_on=False)
        ax2.text(i*2+1, ymax+0.02, msg, va='bottom', ha='center', )

    ax2.set_xlim(0, 6)


def plot_radial_prob(prob_dict, ax, ax_extra=None):

    if ax is None:
        ax = plt.subplot(polar=True)

    ax.bar((prob_dict['ang_bins']+2.5)*np.pi/180,
           np.nanmean(prob_dict['all'], 0), width=0.15, color='gray')
    ax.fill_between((prob_dict['ang_bins']+2.5)*np.pi/180, 0,
                    np.nanstd(prob_dict['all'], 0) /
                    np.sqrt(prob_dict['all'][~np.isnan(prob_dict['all'])].shape[
                                 0])+np.nanmean(prob_dict['all'], 0),
                    color='gray', alpha=0.3)
    ax.set_title('Probability of response as a \nfunction of orientation in '
                 'the arm', pad=20)
    ax.set_xticklabels([])
    ax.text(0, 1.1, 'Into arm', va='center', ha='center', rotation=-90)
    ax.text(np.pi/2, 1.1, 'To anode (+)', va='center', ha='center', rotation=0)
    ax.text(np.pi, 1.1, 'Out of arm', va='center', ha='center', rotation=90)
    ax.text(np.pi*1.5, 1.15, 'To cathode (-)', va='center', ha='center',
            rotation=0)
    r_ticks = [0.5, 1]
    ax.set_rticks(r_ticks)
    ax.set_rlabel_position(215)
    ax.set_rgrids(ax.get_yticks(), va='top', ha='right')

    if ax_extra is not None:
        perp = prob_dict['perp']
        parr = prob_dict['parr']
        ax_extra.bar(((prob_dict['ang_bins']+2.5) * np.pi / 180),
                     np.nanmean(perp, 0), width=0.15, bottom=0, color='grey')
        ax_extra.bar(((prob_dict['ang_bins']+2.5) * np.pi / 180),
                     np.nanmean(parr, 0), width=0.15, bottom=0, color='k')
        ax_extra.fill_between((prob_dict['ang_bins']+2.5) * np.pi/180, 0,
                              np.nanstd(perp, 0) /
                              np.sqrt(perp[~np.isnan(perp)].shape[0]) +
                              np.nanmean(perp, 0), color='grey', alpha=0.3)
        ax_extra.fill_between((prob_dict['ang_bins']+2.5) * np.pi/180, 0,
                              np.nanstd(parr, 0) /
                              np.sqrt(parr[~np.isnan(parr)].shape[0]) +
                              np.nanmean(parr, 0), color='k', alpha=0.3)
        ax_extra.set_facecolor((1, 1, 1))
        ax_extra.grid(color=(0, 0, 0, 0.3), ls=':')

        ax_extra.set_title('Shock reactivity as a function\nof orientation in '
                           'the arm', pad=20)
        ax_extra.set_xticklabels([])

        ax_extra.tick_params(axis='x', pad=-5)
        r_max = ax_extra.get_rmax()
        r_step = r_max/2 - np.mod(r_max/2, 10)
        r_ticks = np.arange(0, 2*r_step+1, r_step)
        ax_extra.set_rticks(r_ticks[1:])
        ax_extra.set_rlabel_position(190)
        ax_extra.set_rgrids(ax_extra.get_yticks(), va='top', ha='right')


if __name__ == '__main__':
    ss_sub = md.ssa_turn.loc[md.xp_new]
    new_shock = []  # confusion because fish is safe and suddenly starts getting shocks
    for g, gg in ss_sub.groupby('xp_id'):
        co = pd.read_hdf(gg.iloc[2].ss_coords_path)
        if co.loc[0, 'arm'] == gg.iloc[2].stim_at:
            new_shock.append(g)

    ft = als.fish_types(ss_sub)
    over = ft['Overstayer']
    center = ft['Center']
    print("{} fish with {} overstayers and {} overcentrals".format(
          int(ss_sub.shape[0]/len(ss_sub.ss_no.unique())),
          len(over), len(center)))
    ss_sub = ss_sub.loc[~ss_sub.xp_id.isin(over)]

    avg_e, avg_sum_e = als.moving_avg(ss_sub.loc[ss_sub.xp_id.isin(new_shock)],
                                      als.entry, exclude_overstayers=False,
                                      **{'val': [0, 1, 2]})
    avg_o, avg_sum_o = als.moving_avg(ss_sub.loc[ss_sub.xp_id.isin(new_shock)],
                                      als.occupancy, exclude_overstayers=False,
                                      **{'val': [0, 1, 2, 3]})
    plot_sliding(ss_sub, als.entry, avg=avg_e, avg_sum=avg_sum_e,
                excl=True,  # sign=sign, # individ=True,
                name='Entry frequency in sliding window',
                # show_val=[0],
                fg_sz=(6, 8), **{'val': [0, 1, 2]})
