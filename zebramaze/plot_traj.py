import matplotlib.pyplot as plt

import zebramaze.plotting as aplt
import zebramaze.md as md

ss = md.ssa.loc[md.ssa.ss_id == '120418-15-01-02'].iloc[0]

fig = plt.figure(figsize=(5, 5))
axi = fig.add_axes([0, 0, 1, 1])

env = ss.env_geometry.copy()
env[:, :, 0] = env[:, :, 0] - 2 * (env[:, :, 0] - env[:, :, 0].mean())

# add padding of size BUF around the maze
xmin, ymin = env.min(axis=1).min(axis=0)
xmax, ymax = env.max(axis=1).max(axis=0)

#axi, _ = aplt.plot_env(ss, ax=axi, calib=False, edge=False)

if ss.ss_no == 1:
    f, axi = aplt.plot_ss(ss, ax=axi, x0=11/12, x1=1.0,
                          **{'s': 2, 'alpha': 0.1})
elif ss.ss_no == 0:
    f, axi = aplt.plot_ss(ss, ax=axi, x0=2/3, x1=1.,
                          **{'s': 2, 'alpha': 0.1})
else:
    f, axi = aplt.plot_ss(ss, ax=axi, x0=0.0, x1=1/3,
                          **{'s': 2, 'alpha': 0.1})

axi.set_xlim(xmin, xmax)
axi.set_ylim(ymin, ymax)

axi.set_aspect('equal')

fig.savefig('{}.png'.format(ss.ss_id), dpi=300)
