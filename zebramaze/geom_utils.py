import numpy as np
from numba import jit


@jit(nopython=True, cache=True)
def dist(p1, p2):
    """
    Find Euclidian distance between points p1 and p2

    :param p1: point 1 x,y
    :param p2: point 2 x,y
    :return: distance between points p1 and p2
    """
    return np.sqrt((p1[0]-p2[0])**2+(p1[1]-p2[1])**2)


@jit(nopython=True)
def find_ang(p1, p2):
    """
    Find angle of vector p1->p2

    :param p1: starting point of the vector
                has to be in format np.array([x, y]) with shape (2, )
    :param p2: end point of the vector
                has to be in format np.array([x, y]) with shape (2, )
    :return: angle a (in degrees)
    """
    if (p2.shape[0] != 2) or (p1.shape[0] != 2):
        raise TypeError("Wrong vector format!")
    if (len(p1.shape) != 1) and (len(p2.shape) != 1):
        raise TypeError("One of the inputs has to be a point!")
    dx = p2[0] - p1[0]
    dy = p2[1] - p1[1]

    a = np.arctan2(dy, dx) * 180 / np.pi

    return a


def vect_ang(v1, v2):
    """
    Find angle between vectors v1 and v2 using dot product

    :param v1: vector 1 2x1
    :param v2: vector 2 2x1
    :return: angle between two vectors in radians
    """
    return np.dot(v1, v2)/np.sqrt(np.dot(v1, v1)+np.dot(v2, v2))


def norm(vect):
    """
    Find a norm to vector

    :param vect: vector 2x1
    :return: norm to vector
    """
    return -vect[1], vect[0]


def minmax(arr):
    """Return min, max of an array and their difference."""

    return np.nanmin(arr), np.nanmax(arr), np.nanmax(arr) - np.nanmin(arr)


def displacement(co, x='x', y='y', smooth=False):
    # transforms coordinates to euclidean distance traveled between frames
    co['x'], co['y'] = co[x].astype(float), co[y].astype(float)

    def tri(xi, yi): return np.sqrt(xi ** 2 + yi ** 2)
    disp = tri(co['x'].diff(), co['y'].diff())
    if smooth:
        disp = disp.rolling(window=5, center=False).median()
    return disp
