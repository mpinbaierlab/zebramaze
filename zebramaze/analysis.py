import numpy as np
import pandas as pd
from matplotlib import pyplot as plt
import itertools
from scipy.stats import f_oneway, pearsonr, norm
import scipy.signal as sg
from scipy.cluster import hierarchy
import pickle
import sklearn.preprocessing as prepro

import zebramaze.md as md
import zebramaze.geom_utils as gu
import zebramaze.maze_utils as mu
import zebramaze.trans as trans

from gen.intv import rle

as_strided = np.lib.stride_tricks.as_strided
num2user = {0: 'shock', 1: 'left2shock', 2: 'right2shock', 3: 'center'}
hmm_folder = 'C:/Users/yashina/projects/zebramaze/zebramaze/'

names = ['Overstayer', 'Non-responder', 'Center', 'One arm', 'Two arms']
type_col = dict(zip(names, ['red', 'royalblue', 'orange', 'seagreen', 'gold']))

unstable = ['070218-02-01', '070218-03-01', '110917-02-01', '120418-13-01',
            '140817-01-01', '160817-05-01', '160817-08-01', '200617-01-01',
            '220617-05-01', '220817-04-01', '230817-06-01', '240517-04-01',
            '290318-07-01', '290517-02-01', '290517-07-01', '300517-01-01',
            '300517-10-01', '201015-10-08']
froze = ['190318-02-01', '110118-06-01', '240617-01-01', '010917-04-01',
         '290917-03-01', '170617-02-01', '260218-10-01', '130817-03-01',
         '200318-02-01', '210817-01-01', '240118-04-01', '011017-06-01',
         '310817-03-01', '081017-02-01', '080318-02-01',
         '220617-02-01', '250118-02-01', '310517-09-01', '220318-04-01',
         '160617-02-01', '170617-06-01', '290118-04-01', '080318-04-01',
         '210617-02-01', '110118-07-01', '130617-07-01', '170118-04-01',
         '270817-06-01', '290118-02-01', '220118-10-01', '310817-04-01',
         '150917-04-01', '130817-02-01', '200318-01-01', '120817-04-01',
         '150717-14-01', '110917-07-01', '170418-02-01', '011017-08-01',
         '120418-04-01', '210817-04-01', '100118-02-01', '010917-03-01',
         '230617-03-01', '040917-06-01', '130218-02-01', '070318-01-01',
         '120917-01-01', '120418-12-01', '260218-06-01', '190318-01-01',
         '170617-04-01', '240118-07-01', '290917-02-01', '011017-02-01',
         '240118-10-01', '150717-16-01', '170617-05-01', '210817-08-01',
         '010917-06-01', '170418-01-01', '290917-04-01', '240118-02-01',
         '120218-08-01', '160717-05-01']


def fish_types(ssub, center_th=0.6, resp_th=0.2, x0=0., x1=1.,
               fps=60, shock_th=60):
    center, over, non_resp, resp_1, resp_2 = [], [], [], [], []
    print('Unstable: n = {}; Froze: n = {}'.format(len(ssub.loc[ssub.xp_id.isin(
        unstable)].xp_id.unique()),
        len(ssub.loc[ssub.xp_id.isin(froze)].xp_id.unique())))
    ssub = ssub.loc[~ssub.xp_id.isin(unstable+froze)]
    for i, ss in ssub.iterrows():
        if ss.ss_no == 1:
            co = pd.read_hdf(ss.ss_coords_path)
            # we have to estimate overstayers using ARM, not ARMFIX, because
            # ARM indicates how long the fish were actually receiving shocks
            stays = trans.raw_stays(co, arm='arm')
            merge = trans.merge_arms(stays, drop=[4])
            merge['rel'] = mu.relative(merge.arm, arm_ref=int(ss.stim_at))
            zero = merge.loc[merge.rel == 0, 'ds']
            if (zero > shock_th * fps).any():
                over.append(ss.xp_id)
                continue

            co_size = co.shape[0]
            co = co.iloc[int(x0 * co_size):int(x1 * co_size)].reset_index(
                drop=True)
            if (co.arm == 3).sum() / co.shape[0] > center_th:
                center.append(ss.xp_id)
                continue

            if (co.arm == ss.stim_at).sum() / co.loc[co.arm != 3].shape[0] > \
                    resp_th:
                non_resp.append(ss.xp_id)
                continue

            nonsh_arm = [x for x in [0, 1, 2] if
                         (x not in [ss.stim_at])]
            ratio_list = []
            for j in range(int(co.shape[0]/36000)):
                co_i = co.iloc[j*36000:(j+1)*36000]
                safe1 = co_i[co_i.arm == nonsh_arm[0]].shape[0]/36000
                safe2 = co_i[co_i.arm == nonsh_arm[1]].shape[0]/36000
                if safe1 > safe2:
                    ratio_list.append(safe1-safe2)
                else:
                    ratio_list.append(safe2-safe1)

            if np.mean(ratio_list) > 0.2:
                resp_1.append(ss.xp_id)
                continue

            resp_2.append(ss.xp_id)

    return dict(zip(names, [over, non_resp, center, resp_1, resp_2]))


def arm_occupancy(ss, x1, x2, bins, rel=True, two_one=False,
                  patt=False, start=-1, omit=None, versus=None,
                  input_patt=None):
    """
    Calculate occupancy histogram

    :param ss: a Series with session information
    :param x1, x2: <1 a selection in coordinates of each session to consider
    :param bins: array with bins for histogram
    :param rel: True if relative arm numbering to be used
    :param omit: which arms to omit in calculation
    :param versus: None or int, if int plot relative occupancy of this arm
                    against relative occupancy in the rest of the arms
    :param start: which arm should the occupancy be started counted from
                    (important for test session, should be 3 then)
                    should already be corresponding to rel value!!

    :returns: histogram containing arm occupancy information
    """

    if omit is None:
        omit = []
    coords = pd.read_hdf(ss.ss_coords_path)
    coords = coords.iloc[int(coords.shape[0]*x1):int(coords.shape[0]*x2)]

    # start counting occupancy from arm with value 'start'
    start_ind = coords.loc[coords.arm == start].index.values
    if start_ind.size > 0:
        coords = coords.loc[start_ind[0]:]

    arms = coords.loc[~coords.arm.isin(omit), 'arm']

    if versus is not None:
        arms_versus = arms.copy()
        arms_versus[arms == versus] = 0  # arm of interest is assigned to 0
        arms_versus[arms != versus] = 1  # all other arms are mixed together and
        #  assigned to 1
        arms = arms_versus
    elif patt:
        if input_patt is None:
            print("Need a pattern sequence to align to")
        patt_seq = mu.map_patt(input_patt, ss.env_patterns) + [3]
        arms = arms.apply(lambda x: patt_seq[x])
    elif two_one:
        patt_seq = mu.patt_2_1(ss.env_patterns, ss.stim_at)+[3]
        arms = arms.apply(lambda x: patt_seq.index(x))
    elif rel:
        arms = mu.relative(arms, int(ss.stim_at), arm_no=len(ss.env_patterns))
    elif ss.env_layout == 'blr':
        arms = arms.astype(int)
        arms.loc[arms.isin([0, 1, 2])] -= 1
        arms.loc[arms == -1] = 2

    hist = np.histogram(arms, bins=bins)[0]
    return hist


def calc_orient(disp_meta, disp_raw, bout_thresh=5):

    disp_meta = pd.DataFrame(disp_meta, columns=['sh_ind', 'xp_id', 'ang'])
    prob_dict = {k: [] for k in ['all', 'parr', 'perp']}
    reac_dict = {k: [] for k in ['Aligned', 'Misaligned', 'To anode',
                                 'To cathode', 'Into arm', 'Out of arm']}

    a_bins = np.linspace(0, 360, 41)[:-1]
    for g, gg in disp_meta.groupby('xp_id'):
        ang = gg.ang.values.astype(float)
        reac = disp_raw[gg.index].sum(1)

        anode = (ang > 45) & (ang <= 135)
        cathode = (ang > 225) & (ang <= 315)
        outwards = (ang > 135) & (ang <= 225)
        inwards = (ang <= 45) | (ang > 225)

        bout = np.max(disp_raw[gg.index], 1) > bout_thresh
        parr_ang = anode | cathode

        parr_bins = []
        perp_bins = []
        reac_prob = []

        for b in a_bins:

            a_i = (ang >= b) & (ang < b + 9)
            r_bout = reac[a_i & bout].shape[0]
            r_nobout = reac[a_i & ~bout].shape[0]
            r_parr = reac[a_i & parr_ang & bout]
            r_perp = reac[a_i & ~parr_ang & bout]
            if r_parr.shape[0] == 0:
                parr_mean = np.NaN
            else:
                parr_mean = r_parr.mean()
            if r_perp.shape[0] == 0:
                perp_mean = np.NaN
            else:
                perp_mean = r_perp.mean()
            parr_bins.append(parr_mean)
            perp_bins.append(perp_mean)
            if r_bout+r_nobout == 0:
                reac_prob.append(np.NaN)
            else:
                reac_prob.append(r_bout/(r_bout+r_nobout))

        prob_dict['all'].append(reac_prob)
        prob_dict['parr'].append(parr_bins)
        prob_dict['perp'].append(perp_bins)

        reac_dict['Aligned'].append(reac[parr_ang & bout].mean())
        reac_dict['Misaligned'].append(reac[~parr_ang & bout].mean())
        reac_dict['To anode'].append(reac[anode & bout].mean())
        reac_dict['To cathode'].append(reac[cathode & bout].mean())
        reac_dict['Into arm'].append(reac[inwards & bout].mean())
        reac_dict['Out of arm'].append(reac[outwards & bout].mean())

    prob_dict = {k: np.array(v) for k, v in prob_dict.items()}
    prob_dict['ang_bins'] = a_bins

    return prob_dict, reac_dict


def moving_avg(ssub, func=None, exclude_overstayers=True, two_one=False,
               turn_orig=False, flip_orig=False, l_e=False, **kw):
    """
    Plot average entry frequency/occupancy for a selected experiment type

    :param func: function which takes a selection of sessions to run
                    a sliding window
    :param exclude_overstayers : True if experiments in which fish stayed too
                                    long in the shocked arm and stimulation
                                    was switched off
    :param l_e: True if analyse only long enough stays (see trans.py)
    """

    if exclude_overstayers:
        over = fish_types(ssub)['Overstayer']
        ssub = ssub.loc[~ssub.xp_id.isin(over)]
        if ssub.shape[0] == 0:
            print("Selection is empty or contained only overstayers:\n"
                  " fish who stayed in the shocked arm for longer than 60 sec!")
            return

    all_ss = len(ssub.ss_no.unique())  # how many sessions
    print(ssub.shape[0]/all_ss)
    avg = []
    sldng_sum_all = pd.DataFrame()
    # an element for each arm
    if 'val' in kw.keys():
        val_iter = kw['val']
    else:
        val_iter = range(ssub.env_no_arms.max())
    for _ in val_iter:
        avg.append(pd.DataFrame())

    for xp_id, xp in ssub.groupby('xp_id'):
        sldng, sldng_sum = xp_freq(xp, func, long_enough=l_e, two_one=two_one,
                                   turn_orig=turn_orig, flip_orig=flip_orig,
                                   **kw)
        for no in sldng.columns:
            avg[no] = avg[no].join(pd.DataFrame({xp_id: sldng.loc[:, no]}),
                                   how='outer')
        sldng_sum_all = sldng_sum_all.join(pd.DataFrame({xp_id: sldng_sum}),
                                           how='outer')

    return avg, sldng_sum_all


def boot_corr(x1, x2, n=5000, a=0.05):
    """
    Bootstrap CI for correlation between x1 and x2

    :param x1:
    :param x2:
    :param n:
    :param a:
    :return:
    """
    res = []
    x1, x2 = np.array(x1), np.array(x2)
    sh = x1.shape[0]
    for i in range(n):
        ind = np.random.choice(sh, sh, replace=True)
        corr_s = pearsonr(x1[ind], x2[ind])[0]
        res.append(corr_s)
    res.sort()
    i1, i2 = res[int(len(res)*a/2)], res[int(len(res)*(1 - a/2))]
    return i1, i2


def calc_overlap_hist(ss_pair, arm=None, cell=10):
    """
    Calculate a 2D histogram from trajectory data

    :param ss_pair: a df with information about sessions, for which
                    histograms are calculated
    :param arm: arm, for which histogram is calculated
                if None, histogram is calculated for whole trajectory
    :param cell: size of bins for histogram (cell x cell)

    :returns a: a list of histograms, one for a session
    :returns a_len: a list of session lengths
    """
    a = []
    a_len = []
    a_co = []
    axmin, axmax, aymin, aymax = 9999, -9999, 9999, -9999
    for i, ss in ss_pair.iterrows():
        ss_part = pd.read_hdf(ss.ss_coords_path)
        if arm is not None:
            ss_part.loc[ss_part.arm != arm] = np.NaN
        a_co.append(ss_part)

        # find boundaries of trajectories for bin calculation
        xmin, xmax, dx = gu.minmax(ss_part.x.values)
        ymin, ymax, dy = gu.minmax(ss_part.y.values)
        axmin = min(xmin, axmin)
        aymin = min(ymin, aymin)
        axmax = max(xmax, axmax)
        aymax = max(ymax, aymax)

    xbins = np.arange(axmin - 20, axmax + 20, cell)
    ybins = np.arange(aymin - 20, aymax + 20, cell)
    if (len(xbins) == 0) or (len(ybins) == 0):
        return None, None

    for ss_part in a_co:
        # calculate histogram and clip it to threshold value
        cohist = np.histogram2d(ss_part['x'], ss_part['y'],
                                bins=(xbins, ybins))[0]
        # co_clipped = cohist.clip(max=thresh)
        a.append(cohist.copy())
        a_len.append(ss_part.shape[0])
    return a, a_len


def corr_thigmo(ssub, wins=1200, ovrlp=1140, sno=(1,)):
    """

    :param ssub:
    :param wins:
    :param ovrlp:
    :param sno:
    :return:
    """
    corr = []
    th_all = []
    occu_all = []
    entr_all = []
    for xp_id, xp in ssub.groupby('xp_id'):
        for i, s in xp.iterrows():
            if s.ss_no not in sno:
                continue
            sid = s.ss_id
            co = pd.read_hdf(s.ss_coords_path)
            th = sliding(co.thigmo.values, win_thigmo, sliding_overlap=ovrlp,
                         win_size=wins, abs_num=True, val=[0], min_abs=0)[0]
            occu = xp_freq(s.to_frame().T, occupancy, long_enough=False,
                           **{'sliding_overlap': ovrlp, 'win_size': wins,
                              'val': [0, 1, 2, 3]})[0]
            entr = xp_freq(s.to_frame().T, entry, long_enough=False,
                           **{'sliding_overlap': ovrlp, 'win_size': wins,
                              'val': [0, 1, 2]})[0]

            corr.append(pearsonr(th[0], entr[0]))
            th_all += th[0].tolist()
            occu_all += occu[0].tolist()
            entr_all += entr[0].tolist()
    occu_corr = pearsonr(th_all, occu_all)

    wh = np.where(~np.isnan(entr_all))[0]  # & (np.array(th_all) < 70))[0]
    entr_corr = pearsonr(np.array(th_all)[wh], np.array(entr_all)[wh])

    boot_corr(th_all, occu_all, n=10000)
    boot_corr(np.array(th_all)[wh], np.array(entr_all)[wh], n=10000)

    return th_all, occu_all, entr_all, occu_corr, entr_corr


def entry(a, v):
    """ Calculate entries to v within each row of a
    Parameters:
    ----------
        a        : array with at least one row
        v        : value, which frequency we calculate
    """
    return (np.diff((a == v).astype(int), axis=1) == 1).sum(1)


def fill_hmm_input(ssub, samp, win=60):

    th_all = [[]] * len(samp)  # for values of distance-to-wall
    len_all = [[]] * len(samp)  # for length of distance arrays for each fish

    for i, ss in ssub.iterrows():
        sid = ss.ss_id
        co = pd.read_hdf(ss.ss_coords_path)
        th_sg = sg.savgol_filter(co.thigmo.values, win*60+1, 3)
        for sa_i, sa in enumerate(samp):
            th = th_sg[::win*sa].reshape(-1, 1)
            th_all[sa_i].append(th)
            len_all[sa_i].append(th.shape[0])
    return th_all, len_all


def heat_shuff(X, depth, method='ward', metric='euclidean'):
    Z = hierarchy.linkage(X, method=method, metric=metric)
    y_pred = hierarchy.cut_tree(Z, n_clusters=2)

    occu_sample = []
    to_permute = np.tile(X, (10000, 1, 1))
    permuted_df = permute_columns(to_permute)
    for occu_s in permuted_df:
        Z_s = hierarchy.linkage(occu_s, method=method, metric=metric)
        occu_sample.append(Z_s[-1, 2])

    p_v = 0.05 / depth
    p_e = (np.array(occu_sample) >= Z[-1, 2]).sum() / len(occu_sample)

    print(p_e, p_v, X.shape[0])

    if (np.sum(y_pred == 0) < 10) or (np.sum(y_pred == 1) < 10):
        return np.zeros([len(y_pred), 1])
    if p_e < p_v:
        y_c = y_pred.copy()
        y_c[np.isin(y_pred, 0)[:, 0]] = heat_shuff(
            X[np.isin(y_pred, 0)[:, 0]], depth+1)
        max_ind = max(y_c[np.isin(y_pred, 0)[:, 0]])
        y_c[np.isin(y_pred, 1)[:, 0]] = heat_shuff(
            X[np.isin(y_pred, 1)[:, 0]], depth+1) + max_ind + 1
        y_pred = y_c
    else:
        y_pred = np.zeros([len(y_pred), 1])

    return y_pred


def heat_shuff_simple(X, method='ward', metric='euclidean'):
    Z = hierarchy.linkage(X, method=method, metric=metric)
    y_pred = hierarchy.cut_tree(Z, n_clusters=2)

    return y_pred


def heat_ss(ss, win=36000, rot=False):
    ss_num = len(ss.ss_no.unique())
    occu = np.zeros([int(ss.shape[0]/ss_num), 4*3])
    entr = np.zeros([int(ss.shape[0]/ss_num), 3*3])
    xp_ind = []
    j = 0
    for g, gg in ss.groupby('xp_id'):
        xp_ind.append(g)
        for i in range(3):
            s = gg.iloc[i]
            co = pd.read_hdf(s.ss_coords_path)

            if i == 2:
                co_win = co.iloc[:win]
            else:
                co_win = co.iloc[-win:]
            co_win['rel'] = mu.relative(co_win.armfix, int(s.stim_at),
                                        arm_no=len(s.env_patterns))
            occu[j, i*4+3] = (co_win.rel == 3).sum()
            arms = win - occu[j, i*4+3]
            occu[j, i*4] = (co_win.rel == 0).sum() - arms / 3
            occu[j, i*4+1] = (co_win.rel == 1).sum() - arms / 3
            occu[j, i*4+2] = (co_win.rel == 2).sum() - arms / 3
            if g == '310817-01-01':
                print('wrong')
            if (i == 2) & (s.xp_type == 'turn'):
                occu[j, i*4+1], occu[j, i*4+2] = occu[j, i*4+2], occu[j, i*4+1]

            entr[j, i*3] = ((co_win.rel == 0).astype(int).diff() == 1).sum()
            entr[j, i*3+1] = ((co_win.rel == 1).astype(int).diff() == 1).sum()
            entr[j, i*3+2] = ((co_win.rel == 2).astype(int).diff() == 1).sum()
            entr_sum = entr[j, i*3:(i+1)*3].sum()
            if entr_sum >= 6:
                entr[j, i*3:(i+1)*3] /= entr_sum
                entr[j, i*3:(i+1)*3] -= 1/3
            else:
                entr[j, i*3:(i+1)*3] = 0

        if occu[j, 5] < occu[j, 6]:
            for i in range(gg.shape[0]):
                occu[j, i*4+1], occu[j, i*4+2] = occu[j, i*4+2], occu[j, i*4+1]
                entr[j, i*3+1], entr[j, i*3+2] = entr[j, i*3+2], entr[j, i*3+1]
        occu[j, :] /= win
        j += 1
    np.save('heat_occu.npy', occu)
    np.save('heat_entr.npy', entr)
    np.save('heat_xp.npy', xp_ind)
    return np.array(xp_ind), occu, entr


def load_heat():
    try:
        xp_ind, occu = np.load('heat_xp.npy'), np.load('heat_occu.npy')
    except:
        ss_all = pd.concat([md.ssa_swap, md.ssa_replace,
                            md.ssa_test_3.loc[md.xp_new & (md.ssa.xp_age > 20)],
                            md.ssa_turn.loc[md.xp_new]])
        ft_all = fish_types(ss_all)
        for n, r in ft_all.items():
            print("{} ({:.2f}%):\n{}\n".format(n, len(r)/ss_all.shape[0]*3, r))
        xp_ind, occu, _ = heat_ss(ss_all.loc[ss_all.xp_id.isin(
            ft_all['Non-responder'] + ft_all['Center'] +
            ft_all['One arm'] + ft_all['Two arms'])], win=18000)

    method = 'ward'
    metric = 'euclidean'

    try:
        y_pred = np.load('heat_y.npy')
    except:
        y_pred = heat_shuff(prepro.scale(np.hstack([occu[:, 4:6],
                                                    occu[:, 7:8]]), 0),
                            1, method=method, metric=metric)
        np.save('heat_y.npy', y_pred)

    all_y = list(set(y_pred[:, 0]))
    occu_y = []
    for yi in all_y:
        occu_y.append(occu[np.isin(y_pred, [yi])[:, 0], 4:8].mean(0))
    occu_y = np.array(occu_y)

    # Here we combine significant clusters into groups.
    # Center-Non-responders consist of two clusters
    # Responders consist of four clusters
    clust_dict = {}
    clust_names = ['Center', 'Non-responder', 'Responder']
    clust_dict['Center'] = np.argmax(occu_y[:, 3])
    all_y.remove(np.argmax(occu_y[:, 3]))
    # find second highest non-avoiding cluster
    clust_dict['Non-responder'] = all_y[np.argmax(occu_y[all_y, 0])]
    all_y.remove(all_y[np.argmax(occu_y[all_y, 0])])
    #
    # clust_dict['Non-responder'] = all_y[np.argmax(occu_y[all_y, 0])]
    # all_y.remove(all_y[np.argmax(occu_y[:, 0])])
    clust_dict['Responder'] = all_y

    return occu, xp_ind, y_pred, clust_dict, clust_names


def hmm_thigmo(ssub, est=None, st_num=2, sampl=(30,),
               plot=False, save=False):
    """
    :param sampl: pass a list of values to sampl to compare quality of
                    smoothing on state plausibility
                    sampling rate of 30 seems to be adequate
    :param ssub: a training set for hmm
    :param est: if not None, prediction is made for fish from est, NOT from
                    the training set ss_sub
    :param st_num: number of states
    :param save: True is save hmm model
    :param plot: True if plot modelling results
    """
    l, X, plt_X, z = [], [], [], []
    for _ in sampl:
        l.append(0)
        X.append([])
        plt_X.append([])
        z.append([])
    th_all, len_all = fill_hmm_input(ssub, sampl)

    if est is not None:
        len_est, est_all = fill_hmm_input(est, sampl)

    for sa_i, sa in enumerate(sampl):
        X[sa_i] = np.concatenate(th_all[sa_i])
        remodel = hmm.GaussianHMM(n_components=st_num, n_iter=100)
        remodel.fit(X[sa_i], len_all[sa_i])
        if save:
            pickle.dump(remodel, open('hmm.p', 'wb'))
        if est is not None:
            z[sa_i] = remodel.predict(np.concatenate(est_all[sa_i]))
            plt_X[sa_i] = est_all[sa_i]
            len_all[sa_i] = len_est[sa_i]
        else:
            z[sa_i] = remodel.predict(X[sa_i])
            plt_X[sa_i] = th_all[sa_i]
        z[sa_i] = map_state_order(z[sa_i], remodel.means_)

    if plot:
        for i in range(len(len_all[0])):
            for z_i, zi in enumerate(z):
                zx = zi[l[z_i]:l[z_i]+len_all[z_i][i]]
                plt.plot(np.arange(zx.shape[0])*sampl[z_i],
                         zx*4-z_i*10, c='k', lw=2)
                plt.plot(np.arange(zx.shape[0])*sampl[z_i], plt_X[z_i][i])
                l[z_i] += len_all[z_i][i]

            plt.ylim([-len(z)*10, 65])
            plt.show()
    return z


def map_state_order(z, means):
    z_mapped = []
    state_order = means[:, 0].argsort()
    for zi in z:
        z_mapped.append(np.where(state_order == zi)[0][0])
    return np.array(z_mapped)


def norm_overlap_hist(a, a_len):
    """ Normalize values in each bin of histogram
    in correspondence to duration of a session

    Parameters:
    ----------
    a     : a list with histograms
    a_len : a list with durations of histograms,
            which correspond to histograms in a

    Returns:
    -------
    a_norm  : list of normalized histograms
    """
    # max_stay = max(a_len)
    a_norm = []
    for l, aa in zip(a_len, a):
        # aa = aa*max_stay/l
        aa = aa / l
        a_norm.append(aa)
    return a_norm


def occupancy(a, v):
    """ Calculate occurrence of v within each row of a
    Parameters:
    ----------
        a        : array with at least one row
        v        : value, which frequency we calculate
    """
    return (a == v).sum(1)


def overlap_ind(pair, ab, arm=None, norm=True, cell=10):
    """ Calculate overlap index for trajectories of a pair of sessions

    Parameters:
    ---------
    pair  : a df with information about sessions, for which
            overlap is calculated
    ab   : True if absolute values of difference in trajectories are calculated
    arm   : arm for which overlap is calculated
            if None, overlap is calculated for whole trajectory
    norm  : True if trajectories are normalized to session durations
    cell  : size of bins - coarseness of analysis

    Returns:
    -------
    ov_ind : overlap index of the pair
    adiv   : a 2D array with individual indices in each bin
    """
    a, a_len = calc_overlap_hist(pair, arm=arm, cell=cell)
    if a is None:
        return np.NaN, None  # if there was no movement for this arm value

    if norm:
        a = norm_overlap_hist(a, a_len)
    is0 = np.where((a[0] + a[1]) == 0)  # this deals later with NaN values
    a_sum = a[0] + a[1]
    a_sum[is0] = 1

    if ab:
        # adiv = np.divide(abs(a[1]-a[0]),a_sum)
        adiv = abs(a[1] - a[0])
    else:
        # adiv = np.divide((a[0]-a[1]),a_sum)
        adiv = a[0] - a[1]
    adiv[is0] = np.NaN
    ov_ind = np.nanmean(adiv)
    if ab:
        # for plotting purposes (colormap looks better)
        adiv = - adiv

    return ov_ind, adiv


def occupancy_bar(ssub, x1=0, x2=0.5, rel=True, two_one=False, norm=True,
                  omit=(4,), versus=None, start=-1, patt=False,
                  input_patt=None):
    """Plot relative arm occupancy histogram
    Parameters:
    -----------
    ssub -- a subset of ssa
    x1:x2  -- < 1, a selection in coordinates of each session to consider
    rel    -- True if relative arm numbering to be used
    two_one-- True if pattern-based numeration in experiments with 2-1
              patterns is used
    norm   -- True if occupancy to be normalized by total amount of time
              in the arm
    omit   -- which arms to omit in calculation
    versus -- None or int, if int plot relative occupancy of this arm
              against relative occupancy in the rest of the arms
    start  -- which arm should the occupancy be started counted from
              (important for test session, should be 3 then)
              should already be corresponding to rel value!!
    patt   -- True if group arms according to their patterns

    Returns:
    --------
    axis and an array which rows for each session containing occupancy
    information
    """

    if omit is None:
        omit = []
    if patt:
        # pattern occupancy is done for all arms, including center
        omit = [4]
        versus = None

    # select arm numbers for occupancy calculation
    full_arm = np.array([0, 1, 2, 3, 4])  # expected arms in the maze
    a = full_arm[~np.in1d(full_arm, omit)]  # exclude omitted arms
    a.sort()

    # set bins for plotting
    if versus is not None:
        # if we plot one arm against others, there're only two columns
        bins = np.array([0, 1, 2]) - 0.25
    else:
        bins = np.append(a, a.max() + 1) - 0.25

    hist_all = pd.DataFrame()
    for i, ss in ssub.iterrows():
        bars = arm_occupancy(ss, x1, x2, bins, rel, two_one, patt, start, omit,
                             versus, input_patt)
        if norm:
            bars = bars.astype(np.float32) / bars.sum()  # relative occupancy

        hist_all = pd.concat([hist_all, pd.DataFrame([bars])],
                             ignore_index=True)

    b = list(np.array(hist_all).T)
    if versus is not None:
        anova = f_oneway(b[0], b[1])
    else:
        anova = f_oneway(b[0], b[1], b[2])
    hist_m = np.nanmean(hist_all, axis=0)
    hist_err = np.nanstd(hist_all, axis=0) / np.sqrt(len(hist_all))

    print("There's {} difference among group means: one-way ANOVA F {}, "
          "p-value {}".format(['no', 'a'][int(anova[1] < 0.05)], anova[0],
                              anova[1]))
    print(hist_m)
    print(hist_err)

    return hist_all, bins, anova


def permute_columns(x):
    ix_j = np.random.sample(x.shape).argsort(axis=1)
    ix_k = np.swapaxes(
        np.tile(np.arange(x.shape[0]), (x.shape[2], x.shape[1], 1)), 0, 2)
    ix_i = np.tile(np.arange(x.shape[2]), (x.shape[0], x.shape[1], 1))
    return x[ix_k, ix_j, ix_i]


def sign_gaus(val, dist, pref=False):
    m, s = norm.fit(dist)
    if pref:
        return 1 - norm.cdf(val, m, s)
    else:
        return norm.cdf(val, m, s)


def calc_signif(sample_df, avg_sum=None, n=10000, alpha=0.05):
    """
    Perform a permutation test on a set of samples (e.g. arm occupancies)
    (inspired by http://www2.stat.duke.edu/~ar182/rr/examples-gallery
    /PermutationTest.html)

    :param sample_df: DataFrame with samples, each row corresponds to a fish,
                        each column corresponds to an arm, arm 0 is shocked
                        permutation is within each row
    :param n: number of permutations
    :param alpha: significance level
    :return: True of False if H0 is rejected
    """

    def diff_est(df): return np.nanmean(df[0]) - \
                (np.nanmean(df[1])+np.nanmean(df[2]))/2
    diff_hat = diff_est(sample_df)
    to_permute = np.tile(sample_df, (n, 1, 1))
    permuted_df = permute_columns(to_permute)
    diff_dist = permuted_df[:, 0] - (permuted_df[:, 1]+permuted_df[:, 2])/2
    if avg_sum is None:
        diff_dist = np.nanmean(diff_dist, 1)
    else:
        diff_dist = np.nansum(diff_dist*avg_sum.divide(avg_sum.sum()).values, 1)
    # diff_dist = diff_dist[:, 0] - (diff_dist[:, 1]+diff_dist[:, 2])/2

    return diff_dist, (diff_dist < diff_hat).sum()/n


def sh_orient(ss):

    m_center = ss.env_geometry.mean(1).mean(0)
    arm_orient = gu.find_ang(m_center, ss.env_geometry.mean(1).T)

    co = pd.read_hdf(ss.ss_coords_path)
    disp = gu.displacement(co)

    ang = []
    reac = []

    sh_ind = trans.sh_index(co)
    for pos in sh_ind:
        a = co.loc[pos, 'ang'] - arm_orient[co.loc[pos, 'arm']]
        if a < 0:
            a += 360
        if a >= 360:
            a -= 360
        ang.append(a)
        reac.append(disp[pos:pos+10].sum())

    ang, reac = np.array(ang), np.array(reac)

    a_bins = np.linspace(0, 360, 37)[:-1]
    reac_bins = []
    num_bins = []
    for b in a_bins:
        r = reac[(ang >= b) & (ang < b + 10)]
        if r.shape[0] == 0:
            rm = 0
        else:
            rm = r.mean()
        reac_bins.append(rm)
        num_bins.append(r.shape[0])
    reac_bins = np.array(reac_bins)
    cond = ((a_bins > 45) & (a_bins <= 135)) |\
           ((a_bins > 225) & (a_bins <= 315))

    return a_bins, reac_bins, num_bins, cond


def sh_reactivity(ss, winlen=6, overhang=0):
    """
    Calculate reactivity of fish

    """

    m_center = ss.env_geometry.mean(1).mean(0)
    arm_orient = gu.find_ang(m_center, ss.env_geometry.mean(1).T)

    co = pd.read_hdf(ss.ss_coords_path)
    sh_index = trans.sh_index(co)

    a = co.loc[sh_index, 'ang'].values - arm_orient[ss.stim_at]
    a[a < 0] += 360
    a[a >= 360] -= 360

    disp = gu.displacement(co)

    # split at shock times, and stack for averaging
    split = [disp.values[s:s+winlen+overhang] for s in sh_index]
    if len(split[-1]) != winlen+overhang:
        split = split[:-1]
        a = a[:-1]

    combine = np.vstack(split).reshape((-1, winlen + overhang))  # / ss.length

    a_cond = ((a > 45) & (a <= 135)) | ((a > 225) & (a <= 315))
    c_cond = combine.max(axis=1) > 10  # /ss.length

    return combine, a_cond, c_cond


def sliding(a, func, val=(0, 1, 2), sliding_overlap=540, win_size=600,
            abs_num=False, fps=60, min_abs=1):
    """ Calculates relative entry frequency for arm 'val' in a sliding window

    Parameters:
    ----------
    a                : arm sequence for a whole session
    func             : function to run on each of the windows
    val              : entries to which arm we are counting
    sliding_overlap  : overlap between two consecutive windows (in seconds)
    win_size         : size of sliding window (in seconds)
    abs_num          : False if entries to each arm within a window are to be
                       normalized by the total number of entries in the window
    fps              : recording resolution in frames per second

    Returns:
    -------
    out, win_size, sliding_overlap
    """
    stri = a.strides[0]
    win_size *= fps
    sliding_overlap *= fps
    dur = a.shape[0]

    # these calculations are for estimating overlap between windows given a
    # desirable number of windows for a session, could be implemented as an
    # alternative
    # num = a.shape[0] / 2000.0 * 4  # 4 points per 2000 points of simulation
    # ovrlp = win_size - (dur - win_size) / (num - 1)  # overlap of two
    # consecutive window slices
    num = int((dur - win_size)/(win_size - sliding_overlap) + 1)
    a_resh = as_strided(a, (num, win_size),
                        (stri * (win_size - sliding_overlap), stri))
    out = pd.DataFrame([])
    for v in val:
        out[v] = func(a_resh, v)

    out_sum = out.sum(1)
    if not abs_num:
        # denominator for normalization below
        out = out.divide(out_sum, axis=0)

    # ignore values where total number of transitions within a window is < 3
    if min_abs:
        out[out_sum < min_abs] = np.NaN

    return out, out_sum


def thigmo(ss, savgol=True, hmm_load=True):

    dur = 0
    z_all, th_all = pd.DataFrame(), pd.DataFrame(columns=['x', 'th', 'col'])

    for i, s in ss.iterrows():
        co = pd.read_hdf(s.ss_coords_path)
        if savgol:
            win = 60
            th = sg.savgol_filter(co.thigmo.values, win * 60 + 1, 3)
        else:
            th = co.thigmo.rolling(1800).mean()
        xr = np.arange(dur, dur+s.ss_duration)
        th_i = pd.DataFrame({'th': th, 'x': xr, 'col': s.ss_no})
        th_all = pd.concat([th_all, th_i], ignore_index=True)

        # if hmm_load and os.path.exists(hmm_folder+'hmm.p'):
        #     hmm_model = pickle.load(open(hmm_folder+'hmm.p', 'rb'))
        #     z = hmm_model.predict(th[::win*60].reshape([-1, 1]))
        #     z = map_state_order(z, hmm_model.means_)
        #     z_intv = to_intv(z, shock=False)
        #     z_intv.loc[:, ['a', 'b']] *= win*60
        #     z_intv.loc[:, ['a', 'b']] += dur
        #     z_all = pd.concat([z_all, z_intv], ignore_index=True)

        dur += s.ss_duration

    return th_all, z_all


def to_intv(mask, shock=True):
    df = pd.DataFrame(columns=['a', 'b', 'val'])
    arr = np.array(list(rle(mask)))
    if shock:
        arr = arr[np.where(arr[:, 2] == True)]
    df['a'] = arr[:, 0]
    df['b'] = arr[:, 1]
    df['val'] = arr[:, 2]
    return df


def win_thigmo(a):
    return a.mean(1)


def xp_freq(ss, func, drop=(4,), raw=False, long_enough=True, rel=True,
            two_one=False, turn_orig=False, flip_orig=False, patt=None,
            pref=False, flip_patt=False, **kw):
    """ Combines stays in different sessions and runs a sliding window
    Parameters:
    ----------
    ss  : selection of ssa with information for one experiment
    raw : True if no filtering is applied to arm sequence
    rel : True if arms re counted relative to shock (shock=0)

    Returns:
    -------
    an array with sliding window entry frequency for whole experiment
    """

    arm_seq = pd.DataFrame([])
    if flip_patt or flip_orig or pref:
        co_shock = pd.read_hdf(ss.iloc[1].ss_coords_path)
        co_shock = co_shock.iloc[int(co_shock.shape[0]*11/12):]
        # fish might not visit a second safe arm, in that case it won't
        # appear in value_counts() or in arm_list. To avoid an Exception,
        # the second safe arm is looked up in the all_arms as other than
        # shocked and preferred safe arm
        all_arms = [0, 1, 2]
        all_arms.remove(ss.iloc[1].stim_at)
        sh_arm = ss.iloc[1].stim_at
        co_sh_0 = (co_shock.armfix == all_arms[0]).sum()
        co_sh_1 = (co_shock.armfix == all_arms[1]).sum()
        if co_sh_0 > co_sh_1:
            pref_arm = all_arms[0]
        else:
            pref_arm = all_arms[1]
        all_arms.remove(pref_arm)
        other_arm = all_arms[0]
        sh_patt = ss.iloc[1].env_patterns[sh_arm]
        pref_patt = ss.iloc[1].env_patterns[pref_arm]
        other_patt = ss.iloc[1].env_patterns[other_arm]
    for i, s in ss.iterrows():

        # XXX what if corresponding sessions of different experiments had
        # slightly different number of frames, and as a result have one
        # point fewer for the sliding window? Need to pad then (padding
        # size has to be figured out before and passed to this function

        co = pd.read_hdf(s.ss_coords_path)
        if 'armfix' not in co.columns:
            co['armfix'] = co.arm
        if (s.ss_no == 1) and (co.shape[0] > 216000):
            co = co.iloc[:216000]
        if (s.ss_no == 2) and (co.shape[0] > 108000):
            co = co.iloc[:108000]
        if two_one:
            patt_seq = mu.patt_2_1(s.env_patterns, s.stim_at) + [3]
            co.armfix = co.armfix.apply(lambda x: patt_seq.index(x))
        elif patt:
            patt_seq = mu.map_patt(s.env_patterns, patt) + [3]
            co.armfix = co.armfix.apply(lambda x: patt_seq.index(x))
        elif flip_patt or flip_orig:
            if flip_patt:
                sh_arm = s.env_patterns.index(sh_patt)
                pref_arm = s.env_patterns.index(pref_patt)
                other_arm = s.env_patterns.index(other_patt)
            sh_ind = co.armfix == sh_arm
            pref_ind = co.armfix == pref_arm
            oth_ind = co.armfix == other_arm
            co.loc[sh_ind, 'armfix'] = 0
            co.loc[pref_ind, 'armfix'] = 1
            co.loc[oth_ind, 'armfix'] = 2
        elif pref:
            if (s.ss_no == 2) and turn_orig:
                co.armfix = mu.relative_pref(co.armfix, s.stim_at,
                                             (pref_arm-sh_arm+s.stim_at) % 3)
            else:
                co.armfix = mu.relative_pref(co.armfix, sh_arm, pref_arm)
        elif rel:
            if (s.ss_no == 2) and turn_orig:
                co.armfix = mu.relative(co.armfix, int(ss.iloc[0].stim_at),
                                        arm_no=len(s.env_patterns))
            # elif (s.ss_no == 2) and flip_orig:
            #     patt_seq = mu.map_patt(ss.iloc[0].env_patterns,
            #                            s.env_patterns) + [3]
            #     co.recorded_arm = co.recorded_arm.apply(lambda x:
            #                                             patt_seq.index(x))
            #     co.recorded_arm = mu.relative(co.recorded_arm, s.stim_at,
            #                                   arm_no=len(s.env_patterns))
            else:
                co.armfix = mu.relative(co.armfix, s.stim_at,
                                        arm_no=len(s.env_patterns))

        arm_seq = pd.concat([arm_seq, co], ignore_index=True)
    print(ss.iloc[0].xp_id)
    stays = trans.raw_stays(arm_seq, arm='armfix')
    if not raw:
        merged = trans.merge_arms(stays, drop=drop)
        if long_enough:
            merged = trans.merge_arms(trans.long_enough(merged), drop=[])
    else:
        merged = stays

    a = trans.stays2sequence(merged)

    return sliding(a, func, **kw)


def xp_overlap(xp_id, ab=False, cell=10):
    """ Calculate overlap index for pairs of sessions within one experiment

    Parameters:
    ----------
    xp_id : id of an experiment
    ab    : True if absolute values of difference in trajectories are calculated
    cell  : size of bins - coarseness of trajectory analysis

    Returns:
    -------
    """
    ssub = md.ssa.loc[md.ssa.xp_id == xp_id]
    ss_ids = ssub['ss_id'].values
    ss_pairs = itertools.combinations(ss_ids, 2)
    arms = [None, 0, 1, 2, 3]
    for p in ss_pairs:
        print(p)
        for arm in arms:
            ov_ind, adiv = overlap_ind(ssub.loc[ssub.ss_id.isin(p)], ab,
                                       arm=arm, cell=cell)
            h_max = np.nanmax(abs(adiv))
            # adiv /= h_max
            ov_ind /= h_max
            if arm is None:
                plt.figure(figsize=(8, 8))
                plt.grid(b=False)
                if ab:
                    m = 'viridis'
                else:
                    m = 'RdBu'
                cmap = plt.get_cmap(m)
                cmap.set_bad(color=(0.5, 0.5, 0.5))  # grey color for NaN values
                # transpose histogram because pyplot.imshow flips axes
                plt.imshow(adiv.T, cmap=m, interpolation='nearest')
                plt.show()
            print(arm, -ov_ind)


if __name__ == '__main__':
    ss_sub = md.ssa.loc[md.ssa.xp_id == '130817-05-01']
    over = fish_types(ss_sub, x0=0.5)['Overstayer']
    ss_sub = ss_sub.loc[~ss_sub.xp_id.isin(over)]
    avg_e, avg_sum_e = moving_avg(md.ssa.loc[md.ssa.xp_id == '081017-03-01'],
                                  entry, pref=True)
