import os
import pickle
import numpy as np
import pandas as pd

from matplotlib import pyplot as plt
import matplotlib as mpl
from PIL import Image

import seaborn as sns

from scipy.stats import mannwhitneyu

from matplotlib.offsetbox import OffsetImage, AnnotationBbox
import matplotlib.patches as mpatch
import matplotlib.collections as mcoll

import zebramaze.md as md
import zebramaze.plotting as aplt
import zebramaze.analysis as als
import zebramaze.random1D as rand1D
import zebramaze.find_bouts as fb


def fig_1a(fig, left, bottom, w, h):
    ax = fig.add_axes([left, bottom, w, h])
    arr_img = plt.imread('../plots/setup_arial.png', format='png')
    ax.imshow(arr_img, aspect='auto')
    ax.axis('off')

    return ax


def fig_1c(avg, ax, name):
    eff = avg[0] - (avg[1] + avg[2]) / 2
    u_o, pi_o = mannwhitneyu(eff.iloc[0], eff.iloc[-1])
    ax.set_ylim(-0.55, 1.05)
    sns.swarmplot(data=pd.DataFrame({'Start': eff.iloc[0],
                                     'End': eff.iloc[-1]}),
                  palette=['k'], ax=ax, size=2, order=['Start', 'End'])
    flierprops = dict(marker='o', markersize=1)
    sns.boxplot(data=pd.DataFrame({'Start': eff.iloc[0],
                                   'End': eff.iloc[-1]}), ax=ax,
                palette=['silver'] * 4, flierprops=flierprops,
                order=['Start', 'End'], width=0.5)
    ax.set_ylabel(name)
    ax.set_yticks([-0.5, 0, 0.5, 1])
    lvl = 1
    ax.plot([0, 0, 1, 1], [-0.02 + lvl, lvl, lvl, -0.02 + lvl],
            lw=1, c=(0.25, 0.25, 0.25))
    ax.text(0.5, lvl + 0.02, '$p$ = {:.02f}'.format(pi_o),
            ha='center', va='bottom')


def fig_1g(sim_arm, sim_pos, ax, cb=False, entry=True, arr=None):

    s = sim_pos[0].columns.levels[1].shape[0]
    rand1D.effect_sliding(sim_arm, 4, s, sim_pos, entry=entry, ax1=ax,
                          colorbar=cb, arr=arr)
    ax.set_title('')


def fig_1e(ssub, ax, scale):

    tri = lambda x, y: np.sqrt(x ** 2 + y ** 2)

    # calculation of all bouts take a long time, so the result is saved to disk
    if os.path.exists('swap_no_shock_bout_amp.npy'):
        nosh_amp = np.load('swap_no_shock_bout_amp.npy')
        sh_amp = np.load('swap_shock_bout_amp.npy')
    else:
        # calculate all bouts anew
        nosh_amp = np.array([])
        sh_amp = np.array([])
        for m, ss in md.ssa_swap.iterrows():
            co = pd.read_hdf(ss.ss_coords_path)
            co.x, co.y = co.x.astype(int), co.y.astype(int)
            sp = tri(co['x'].diff(), co['y'].diff())
            sp1 = sp.copy()
            cond = (sp1.rolling(3, center=True).mean() > 0.3332) & (
                        sp1.rolling(3, center=True).mean() < 0.3334)
            sp1[cond & (sp1 == 1)] = 0
            df_bouts, sf = fb.bouts(sp1)

            bout_intv = als.to_intv(sf >= 1)
            cond = bout_intv.apply(
                lambda x: (co.loc[x['a']-4:x['b'], 'shock'] == True).any(),
                axis=1)
            sh_bout = bout_intv[cond]
            nosh_bout = bout_intv[~cond]
            nosh_amp = np.concatenate([nosh_amp, nosh_bout.apply(
                lambda x: sp.iloc[x['a']:x['b']].sum(), axis=1).values])
            sh_amp = np.concatenate([sh_amp, sh_bout.apply(
                lambda x: sp.iloc[x['a']:x['b']].sum(), axis=1).values])
        np.save('swap_no_shock_bout_amp.npy', nosh_amp)
        np.save('swap_shock_bout_amp.npy', sh_amp)

    amp_bins = np.arange(0, 150/scale, 2/scale)
    ax.hist(nosh_amp/scale, color='k', alpha=0.5, density=True, bins=amp_bins)
    ax.hist(sh_amp/scale, color='r', alpha=0.5, density=True, bins=amp_bins)

    ax.set_xlabel('Bout amplitude [cm]')
    ax.set_ylabel('Frequency')
    ax.text(1, 1, 'Spontaneous', transform=ax.transAxes, va='top', ha='right')
    ax.text(1, 0.9, 'Shock-triggered', transform=ax.transAxes,
            color="r", va='top', ha='right', alpha=0.6)
    return ax


def figure_1(dpi=100):
    """
    Figure 1 with set-up description and an example of a learner
    :param ex_id:
    :return:
    """

    cf = aplt.CF['BORDERS']
    f_w, f_h = 8, 10
    fig = plt.figure(figsize=[f_w, f_h])
    # dimensions for setup (define the top row of the figure)
    setup_bottom = 0.7
    setup_w = 0.3
    maze_w = 0.08
    movavg_l = cf['l_margin'] + setup_w + 6*cf['gap_w'] + maze_w
    movavg_w = (1 - cf['r_margin'] - movavg_l)*0.4
    movavg_h = (1-setup_bottom-cf['t_margin']-cf['gap_h'])/2

    # dimensions for model schematic (define the second row in the figure)
    model_bottom = 0.45
    model_left = 0.65
    model_w = 1-model_left-cf['r_margin']
    model_h = setup_bottom-model_bottom-cf['gap_h']

    # dimensions for simulations (define the bottom row of the figure)
    sim_text_w = 0.26
    sim_movavg_l = cf['l_margin']+sim_text_w+cf['gap_w']
    sim_movavg_w = (1 - cf['r_margin'] - sim_movavg_l -
                    6*cf['gap_w'])/2  # leave space for the colorbar
    sim_movavg_h = (model_bottom-cf['b_margin']-cf['gap_m']-2.5*cf['gap_h'])/2

    # precalculate moving averages for EF and OC scores of control xp
    ss_sub = md.ssa_control.loc[md.xp_new]
    ss_num = int(ss_sub.shape[0] / len(ss_sub.ss_no.unique()))
    avg_e, avg_sum_e = als.moving_avg(ss_sub, als.entry,
                                      exclude_overstayers=False,
                                      **{'win_size': 300,
                                         'sliding_overlap': 270})
    avg_o, avg_sum_o = als.moving_avg(ss_sub, als.occupancy,
                                      exclude_overstayers=False,
                                      **{'val': [0, 1, 2, 3], 'win_size': 300,
                                         'sliding_overlap': 270})

    # --------------SETUP DRAWING---------------------------
    fig_1a(fig, cf['l_margin'], setup_bottom, setup_w,
           1-setup_bottom-cf['t_margin'])

    # -------------OC AND EF SCHEMATICS---------------------
    aplt.plot_protocol(fig, '', 'ivx', cf['l_margin']+setup_w+2*cf['gap_w'],
                       setup_bottom+1.5*movavg_h-0.5*maze_w+cf['gap_h'],
                       maze_w, maze_w, control=True)
    aplt.plot_protocol(fig, '', 'ivx', cf['l_margin']+setup_w+2*cf['gap_w'],
                       setup_bottom+0.5*movavg_h-0.5*maze_w, maze_w, maze_w,
                       control=True, arr=True, cont=False)

    # --------------OC/EF SCORE SCHEMATIC-------------------
    ax_index = fig.add_axes([cf['l_margin']+setup_w+1.5*cf['gap_w'],
                             setup_bottom+movavg_h-0.02, 0.15, 0.1])
    ax_index.text(0, 0.5, 'Score =', va='center')
    p_h = 0.07
    p1 = mpatch.Rectangle((0.4, 0.5-0.035), p_h, p_h, facecolor='#D42513')
    p2 = mpatch.Rectangle((0.7, 0.55), p_h, p_h, facecolor='#222222')
    p3 = mpatch.Rectangle((0.95-p_h, 0.55), p_h, p_h, facecolor='#888888')
    ax_index.plot([0.7, 0.95], [0.5, 0.5], c='k')
    ax_index.plot([0.53, 0.62], [0.5, 0.5], c='k', lw=0.5)
    ax_index.text(0.825, 0.38, '2', ha='center')
    ax_index.text(0.83, 0.55, '+', ha='center')
    ax_index.add_collection(mcoll.PatchCollection([p1, p2, p3],
                                                  match_original=True))
    ax_index.axis('off')
    ax_index.set_ylim(0, 1)
    ax_index.set_xlim(0, 1)

    # --------------MOVING AVERAGE FOR OC SCORE-------------
    ax = fig.add_axes([movavg_l, setup_bottom+movavg_h+cf['gap_h'],
                       movavg_w, movavg_h])
    aplt.plot_effects(ss_sub, avg_o, 'OC score', ax=ax, show=False,
                      **{'val': [0, 1, 2, 3], 'win_size': 300,
                         'sliding_overlap': 270})
    ax.set_xlabel('')
    ax.set_xticks([0, 60, 120])
    ax.set_xticklabels([])
    ax.text(1, 1, 'n = {}'.format(ss_num), transform=ax.transAxes,
            va='bottom', ha='right')

    # --------------MOVING AVERAGE FOR EF SCORE-------------
    ax = fig.add_axes([movavg_l, setup_bottom, movavg_w, movavg_h])
    aplt.plot_effects(ss_sub, avg_e, 'EF score', ax=ax, show=False,
                      **{'val': [0, 1, 2], 'win_size': 300,
                         'sliding_overlap': 270})
    ax.set_xticks([0, 60, 120])

    # --------OC DIFFERENCE START VS END OF CONTROL XP------
    swarm_w = 1 - cf['r_margin'] - 3*cf['gap_w'] - movavg_l - movavg_w
    ax1 = fig.add_axes([movavg_l+movavg_w+3*cf['gap_w'],
                        setup_bottom+movavg_h+cf['gap_h'], swarm_w, movavg_h])
    fig_1c(avg_o, ax1, 'OC score')
    ax1.set_xticklabels([''])

    ax2 = fig.add_axes([movavg_l+movavg_w+3*cf['gap_w'],
                        setup_bottom, swarm_w, movavg_h])
    fig_1c(avg_e, ax2, 'EF score')
    ax2.set_xticklabels(['First 5\'', 'Last 5\''])

    # ----------COMPARE OCCUPANCY OF DIFFERENT PATTERNS-----
    patt_w = 0.23
    ax = fig.add_axes([cf['l_margin']+cf['gap_w'], model_bottom, patt_w,
                       model_h-cf['gap_h']])
    ax.text(1, 1, 'n = {}'.format(ss_num), transform=ax.transAxes,
            va='bottom', ha='right')
    aplt.plot_occu_bar(ss_sub, ax=ax, rel=False, patt=True, omit=[4])
    # labels for x axis are shown as patterns
    for l, name in enumerate(['i', 'x', 'v']):
        im = plt.imread('../plots/{}.png'.format(name), format='png')
        oi = OffsetImage(im, zoom=0.06)
        ax.add_artist(AnnotationBbox(oi, (l, -0.13), pad=0, frameon=False,
                                     annotation_clip=False))
    ax.set_ylim(0, 1)
    ax.xaxis.set_label_coords(0.5, -0.27)
    ax.set_xlabel('Arm patterns')

    # ---HISTOGRAMS FOR SPONTANEOUS VS SHOCK-TRIGG SPEEDS---
    hist_l = cf['l_margin']+patt_w+4*cf['gap_w']
    ax = fig.add_axes([hist_l, model_bottom, model_left-hist_l-cf['gap_w'],
                       model_h-cf['gap_h']])
    fig_1e(ss_sub, ax, cf['scale'])

    # ----------------------MODEL SCHEMATIC-----------------
    ax = fig.add_axes([model_left, model_bottom-1.5*cf['gap_h'], model_w,
                       model_h])
    arr_img = plt.imread('../plots/model_arial.png',
                         format='png')
    ax.imshow(arr_img, aspect='equal')
    ax.set_title('1D model', pad=10)
    ax.axis('off')

    # create schematic for probabilities of entry (no learning)
    sim_l = cf['l_margin']+cf['gap_w']
    fig.text(sim_l, cf['b_margin']+sim_movavg_h*2+cf['gap_m'],
             'Model without learning', va='top')
    ax_bracket = fig.add_axes([sim_l+1.9*cf['gap_w'],
                               cf['b_margin'] + sim_movavg_h*1.28 +
                    cf['gap_m'], 0.02, 0.45*sim_movavg_h])
    bracket_img = plt.imread('../plots/bracket.png', format='png')
    ax_bracket.imshow(bracket_img, aspect='auto')
    ax_bracket.axis('off')

    ax.annotate(r" = 1/3", fontsize=10, color='#555555',
                xy=(sim_l+2.7*cf['gap_w'],
                    cf['b_margin'] + sim_movavg_h*1.48+cf['gap_m']),
                xycoords='figure fraction', annotation_clip=False)
    ax.annotate(r'${p_{entry}}$', xycoords='figure fraction',
                color='#D42513', fontsize=12, annotation_clip=False,
                xy=(sim_l, cf['b_margin']+sim_movavg_h*1.7+cf['gap_m']),
                fontweight='bold')
    ax.annotate(r'$p_{entry}$', xycoords='figure fraction', color='#888888',
                xy=(sim_l, cf['b_margin']+sim_movavg_h*1.5+cf['gap_m']),
                fontweight='bold', fontsize=12, annotation_clip=False)
    ax.annotate(r'$p_{entry}$', xycoords='figure fraction', color='#222222',
                xy=(sim_l, cf['b_margin'] + sim_movavg_h*1.3+cf['gap_m']),
                fontweight='bold', fontsize=12, annotation_clip=False)

    # create schematic for probabilities of entry (learning)
    fig.text(sim_l, cf['b_margin'] + sim_movavg_h,
             'Model with learning', va='top')
    ax_bracket = fig.add_axes([sim_l + 2.6 * cf['gap_w'],
                               cf['b_margin'] + sim_movavg_h * 0.35, 0.018,
                               sim_movavg_h * 0.35])
    bracket_img = plt.imread('../plots/bracket_left.png', format='png')
    ax_bracket.imshow(bracket_img, aspect='auto')
    ax_bracket.axis('off')

    ax.annotate(r'$p_{entry}$', xycoords='figure fraction', color='#D42513',
                xy=(sim_l, cf['b_margin']+sim_movavg_h*0.5),
                fontweight='bold', fontsize=12, annotation_clip=False)
    ax.annotate(r' $\leq$', xycoords='figure fraction',
                xy=(sim_l+1.65*cf['gap_w'], cf['b_margin']+sim_movavg_h*0.5),
                fontweight='bold', fontsize=10, annotation_clip=False)
    ax.annotate(r'$p_{entry}$', xycoords='figure fraction', color='#888888',
                xy=(sim_l+3.4*cf['gap_w'], cf['b_margin']+sim_movavg_h*0.65),
                fontweight='bold', fontsize=12, annotation_clip=False)
    ax.annotate(r'$p_{entry}$', xycoords='figure fraction', color='#222222',
                xy=(sim_l+3.4*cf['gap_w'], cf['b_margin']+sim_movavg_h*0.35),
                fontweight='bold', fontsize=12, annotation_clip=False)

    # ------------------NO LEARNING SIMULATION--------------
    s = 100  # sample size per 'r' value
    s_dur = 20000  # number of steps in control session
    sz = rand1D.S  # the size of the arm

    # simulation takes a long time, the results are saved to disk
    if os.path.exists('nolearn_coords.p'):
        out_list = pickle.load(open('nolearn_coords.p', 'rb'))
        arm_list = pickle.load(open('nolearn_arms.p', 'rb'))
    else:
        out_list, arm_list = rand1D.experiment1d(sess_len=(s_dur, s_dur*2,
                                                           s_dur),
                                                 s=s, refl=False, learn=False,
                                                 r_range=np.arange(1, 5, 1),
                                                 maze=[sz, sz, sz, sz * 0.3])
        pickle.dump(out_list, open('nolearn_coords.p', 'wb'))
        pickle.dump(arm_list, open('nolearn_arms.p', 'wb'))

    # moving average for EF score
    ax = fig.add_axes([sim_movavg_l, cf['b_margin']+cf['gap_m']+sim_movavg_h,
                       sim_movavg_w, sim_movavg_h])
    fig_1g(arm_list, out_list, ax, entry=True)
    ax.set_xlabel('')
    ax.set_xticks([0, 60, 180, 240])
    ax.set_xticklabels([])

    # moving average for OC score
    ax = fig.add_axes([sim_movavg_l+sim_movavg_w+cf['gap_w']*4,
                       cf['b_margin']+cf['gap_m']+sim_movavg_h, sim_movavg_w,
                       sim_movavg_h])
    fig_1g(arm_list, out_list, ax, entry=False, cb=True)
    ax.set_xlabel('')
    ax.set_xticks([0, 60, 180, 240])
    ax.set_xticklabels([])

    # --------------------LEARNING SIMULATION---------------
    if os.path.exists('learn_coords.p'):
        out_list = pickle.load(open('learn_coords.p', 'rb'))
        arm_list = pickle.load(open('learn_arms.p', 'rb'))
    else:
        out_list, arm_list = rand1D.experiment1d(sess_len=(s_dur, s_dur * 2,
                                                           s_dur),
                                                 s=s, refl=False, learn=True,
                                                 r_range=np.arange(1, 5, 1),
                                                 maze=[sz, sz, sz, sz * 0.3])
        pickle.dump(out_list, open('learn_coords.p', 'wb'))
        pickle.dump(arm_list, open('learn_arms.p', 'wb'))

    # moving average for EF score
    ax = fig.add_axes([sim_movavg_l, cf['b_margin'], sim_movavg_w,
                       sim_movavg_h])
    fig_1g(arm_list, out_list, ax, entry=True, cb=False, arr=[169, 191])
    ax.set_xticks([0, 60, 180, 240])
    ax.set_xticklabels([0, 30, 90, 120])

    # moving average for OC score
    ax = fig.add_axes([sim_movavg_l+sim_movavg_w+cf['gap_w']*4,
                       cf['b_margin'], sim_movavg_w, sim_movavg_h])
    fig_1g(arm_list, out_list, ax, entry=False, cb=False, arr=[191])
    ax.set_xticks([0, 60, 180, 240])
    ax.set_xticklabels([0, 30, 90, 120])

    # --------------------LABELS FOR THE PANELS--------------
    top_panel = 0.96
    middle_panel = 0.62
    left_panel = 0.02
    panel_font = 15

    fig.text(left_panel, top_panel, 'A', fontsize=panel_font)
    fig.text(0.38, top_panel, 'B', fontsize=panel_font)
    fig.text(movavg_l-2.5*cf['gap_w'], top_panel, 'C', fontsize=panel_font)
    fig.text(left_panel, middle_panel, 'D', fontsize=panel_font)
    fig.text(0.36, middle_panel, 'E', fontsize=panel_font)
    fig.text(0.67, middle_panel, 'F', fontsize=panel_font)
    fig.text(left_panel, 0.34, 'G', fontsize=panel_font)
    # fig.text(left_panel, 0.18, 'H', fontsize=panel_font)

    fig.savefig('../figures/figure_1.png', dpi=dpi)
    im = Image.open('../figures/figure_1.png'.format(5))
    im.save('../figures/figure_1.tiff'.format(5), compression='tiff_lzw')


def fig_s_1d(fig, ss_list, ss, left, bottom, w, h):
    ax = fig.add_axes([left, bottom, w, h])
    ax.set_ylim(0.3, max([x.length.max() for x in ss_list]) + 0.1)
    sns.swarmplot(x='xp_age', y='length', ax=ax, data=ss.iloc[::2],
                  dodge=True, palette=['black'], size=3)
    ax.set_xlabel('Age [dpf]')
    ax.set_ylabel('Fish size [cm]')
    print('{} +- {} cm'.format(ss.iloc[::2].length.mean(),
                               ss.iloc[::2].length.std()))
    age_i = [str(x) for x in ss.xp_age.unique().tolist()]
    ax.set_title('Age {} dpf'.format('-'.join(age_i)))
    return ax


def area2length(a, transform='linear'):
    # r is an area-to-length ratio
    # r is calculated using r-to-a relationship from experimental data
    if transform == 'linear':
        r = 0.0117*a + 0.5196
    elif transform == 'quadratic':
        r = -2e-5 * a ** 2 + 0.0167 * a + 0.1866
    else:
        raise '{} area-to-length transformation not defined'.format(transform)

    return a/r


def figure_1s(dpi=100):
    """
    Plot age comparisons
    :return:
    """
    cf = aplt.CF['BORDERS']
    f_w, f_h = 8, 10
    fig = plt.figure(figsize=(f_w, f_h))

    slide_bottom = 0.23
    schema_bottom = 0.91
    size_h = slide_bottom-cf['b_margin']-2*cf['gap_h']

    ages = [8, 15, 22]
    age_num = len(ages)
    # have 2*gap_w between the plots + 1 from the left margin
    slide_w = (1 - cf['l_margin'] - cf['r_margin'] -
               (3*age_num-2)*cf['gap_w']) / age_num
    # have a 1.5*gap_h between the plots + 1 before the maze schematic
    slide_h = (schema_bottom-slide_bottom-6*cf['gap_h'])/4

    # make a list of df with different age groups
    ss_list = []
    for i, age in enumerate(ages):
        ss_sub = md.ssa.loc[
            (md.ssa.xp_type == 'training') &
            ((md.ssa.xp_age >= age-1) & (md.ssa.xp_age <= age+1))]
        ft = als.fish_types(ss_sub)
        over = ft['Overstayer']
        print("{} fish with {} overstayers ".format(
            int(ss_sub.shape[0] / len(ss_sub.ss_no.unique())), len(over)))
        ss_sub = ss_sub.loc[ss_sub.xp_id.isin(ft['Center'] + ft[
            'Non-responder'] + ft['One arm'] + ft['Two arms'])]
        ss_sub.loc[:, 'length'] = \
            ss_sub.loc[:, 'length'].apply(area2length,
                                          **{'transform': 'linear'})
        ss_sub.loc[:, 'length'] = ss_sub.loc[:, 'length'] / cf['scale']
        ss_list.append(ss_sub)

    # ------------DRAW PROTOCOL SCHEMATIC-------------------
    aplt.plot_protocol(fig, '', 'vxi', cf['l_margin'], schema_bottom,
                       slide_w+cf['gap_w'], 1 - schema_bottom - cf['t_margin'],
                       ages=True, cont=False)

    for i, ss in enumerate(ss_list):
        left_i = cf['l_margin']+cf['gap_w']+i*(slide_w+3*cf['gap_w'])

        # draw axes for (EF mov avg + boxplots) and (OC mov avg + boxplots)
        ax1 = fig.add_axes([left_i, slide_bottom+3*(slide_h+1.5*cf['gap_h'])
                            + 0.5 * cf['gap_h'], slide_w, slide_h])
        ax2 = fig.add_axes([left_i, slide_bottom+2*(slide_h+1.5*cf['gap_h'])
                            + 0.5*cf['gap_h'], slide_w, slide_h])
        ax3 = fig.add_axes([left_i, slide_bottom+slide_h+1.5*cf['gap_h'],
                            slide_w, slide_h])
        ax4 = fig.add_axes([left_i, slide_bottom, slide_w, slide_h])
        fig_s_1d(fig, ss_list, ss, left_i, cf['b_margin'], slide_w, size_h)

        # l_e stands for long_enough: count only entries which were longer
        # than 60 frames (because otherwise shock didn't happen)
        # THIS APPLIES ONLY FOR THE OLD DATA!!!!
        # in new data shocks happened immediately, so every entry counts
        avg_e, avg_sum_e = als.moving_avg(ss, als.entry,
                                          exclude_overstayers=False,
                                          **{'l_e': True, 'win_size': 300,
                                             'sliding_overlap': 270})
        avg_o, avg_sum_o = als.moving_avg(ss, als.occupancy,
                                          exclude_overstayers=False,
                                          **{'val': [0, 1, 2, 3],
                                              'l_e': True, 'win_size': 300,
                                             'sliding_overlap': 270})

        # --------DRAW EF SCORE MOVING AVERAGE----------------
        aplt.plot_effects(ss, avg_e, 'EF score', avg_sum=avg_sum_e,
                          show=False, ax=ax1,
                          se=True, **{'val': [0, 1, 2], 'win_size': 300,
                                             'sliding_overlap': 270})
        ax1.text(1, 1, 'n = {}'.format(int(ss.shape[0]/len(ss.ss_no.unique()))),
                 transform=ax1.transAxes, va='bottom', ha='right')
        ax1.text(0.5, 1.15, '{}-week-old'.format(i+1),
                 transform=ax1.transAxes, va='bottom', ha='center')
        ax1.set_xticks([0, 30, 90])
        ax1.set_yticks([-0.5, 0, 0.5])

        # --------DRAW OC SCORE MOVING AVERAGE
        aplt.plot_effects(ss, avg_o, 'OC score', show=False, ax=ax3,
                          se=True, **{'val': [0, 1, 2, 3], 'win_size': 300,
                                             'sliding_overlap': 270})
        ax3.set_xticks([0, 30, 90])
        ax3.set_yticks([-0.5, 0, 0.5])

        # --------DRAW BOXPLOTS FOR EF AND OC SCORES----------
        #pts = [19, 79]
        pts = [49, 169]
        aplt.plot_single_bar(ss, avg_e, func=als.entry, avg_sum=avg_sum_e,
                             ax=ax2, pts=pts)
        aplt.plot_single_bar(ss, avg_o, func=als.occupancy, ax=ax4, pts=pts)

        if i != 0:
            ax2.set_ylabel('')
            ax1.set_ylabel('')
            ax3.set_ylabel('')
            ax4.set_ylabel('')
        ax2.set_xlabel('')
        ax4.set_xlabel('')

    top_panel = 0.97
    middle1_panel = 0.88
    middle2_panel = 0.53
    bottom_panel = 0.16
    left_panel = 0.02
    panel_font = 15

    fig.text(left_panel, top_panel, 'A', fontsize=panel_font)
    fig.text(left_panel, middle1_panel, 'B', fontsize=panel_font)
    fig.text(left_panel, middle2_panel, 'C', fontsize=panel_font)
    fig.text(left_panel, bottom_panel, 'D', fontsize=panel_font)

    fig.savefig('../figures/figure_2.png', dpi=dpi)
    im = Image.open('../figures/figure_2.png'.format(5))
    im.save('../figures/figure_2.tiff'.format(5), compression='tiff_lzw')


if __name__ == '__main__':
    figure_1(dpi=300)
    figure_1s(dpi=300)
