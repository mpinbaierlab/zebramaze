from __future__ import division
from __future__ import print_function

import ast
import os
import socket
import numpy as np
import pandas as pd
from gen.strings import Tmpl
from pdx.dfx import fill_string
import zebramaze.maze_utils as mu

hostname = socket.gethostname()
# print(hostname)

code_dir = os.path.dirname(os.path.abspath(__file__))
DATA_DIR = os.path.expanduser(code_dir+'/../raw_data/')
table_dir = os.path.join(DATA_DIR, 'tables')


def fish_csv(fname, types, converters, **kw):
    """Return metadata df with proper column types."""

    full_fname = os.path.join(table_dir, fname)
    df = pd.read_csv(full_fname, engine='c',
                     dtype=types, converters=converters,
                     **kw)

    return df


def sessions():
    """Return sessions as dataframe with proper column types."""

    ss_types = dict(ss_no=int, ss_duration=int)

    fname = 'sessions.csv'

    df = fish_csv(fname, ss_types, dict())

    df['env_patterns'] = df.env_patterns.astype('category')
    df['stim_at'] = df.stim_at.astype('category')
    df['ss_type'] = df.ss_type.astype('category')

    return df


# XXX rename xp_duration to xp_frames?
# XXX this is more truthful, and also otherwise we cannot guarantee
# XXX that the type will be integer
def experiments():
    """Return experiments as dataframe with proper column types."""

    fname = 'experiments.csv'
    xp_types = dict(xp_age=int, xp_duration=int, trigger=bool)

    xp_conv = dict(env_geometry=lambda x: np.asarray(ast.literal_eval(x)))

    df = fish_csv(fname, types=xp_types,
                  converters=xp_conv,
                  parse_dates=['xp_date'],
                  infer_datetime_format=True,
                  dayfirst=True)

    df.time = pd.to_datetime(df.time, format='%H:%M:%S').dt.time

    # Post-ingestion hooks
    # see http://pandas.pydata.org/pandas-docs/stable/categorical.html

    df['xp_type'] = df.xp_type.astype('category')
    df['env_layout'] = df.env_layout.astype('category')

    # XXX CHECK MAZE LAYOUT HERE? SEE UTILITIES
    return df


def subjects():
    """Return subjects as dataframe with proper column types"""

    df = fish_csv('subjects.csv', None, None,
                  parse_dates=['cross_date'],
                  infer_datetime_format=True,
                  dayfirst=True)

    return df


def ss_xp_sj(coords='hdf_center_coordinates'):

    ss_table = sessions()
    xp_table = experiments()

    xp_sessions = ss_table.groupby('xp_id').size()
    xp_table = xp_table.assign(env_centers=list(map(mu.arm_centers,
                                                xp_table.env_geometry.values)),
                               env_no_arms=list(map(len, xp_table.env_layout)),
                               xp_sessions=xp_sessions.values)

    sj_xp = subjects().merge(xp_table, on='sj_id')
    df = sj_xp.merge(ss_table, on='xp_id')

    # XXX we still equip unconditionally with a reference to coord data
    # previous solution with dfx broke with the new pandas version!!
    def func(x): return os.path.join(DATA_DIR, '{}.h5'.format(x.ss_id))
    df['ss_coords_path'] = df.apply(func, axis=1)
    for i, x in df.iterrows():
        assert os.path.exists(x.ss_coords_path), \
        'Coordinates not found for fish {}'.format(x.ss_id)

    return df


def random_xp(ssub):
    xp_id = ssub.xp_id.sample(1).values[0]
    return ssub.loc[ssub.xp_id == xp_id]


ssa = ss_xp_sj()
xp2015_early = (ssa.xp_date < '2015-12-01') & (ssa.xp_date >= '2015-08-30')
xp2015_late = (ssa.xp_date >= '2015-12-01') & (ssa.xp_date < '2016-03-16')
xp2015 = xp2015_early | xp2015_late
xp2016 = (ssa.xp_date > '2016-11-01') & (ssa.xp_date < '2017-01-01')
xp_new = ssa.xp_date > '2017-06-18'  # new scaled set-up

ssa2015 = ssa.loc[xp2015]
ssa2016 = ssa.loc[xp2016]
ssa_new = ssa.loc[xp_new]

patt_3 = ssa.env_patterns.apply(lambda x: len(set(x))) == 3
patt_2_1 = ssa.env_patterns.apply(lambda x: len(set(x))) == 2
patt_1 = ssa.env_patterns.apply(lambda x: len(set(x))) == 1
patt_g = ssa.env_patterns == 'ggg'
xp_test = ssa.xp_type == 'test'
xp_turn = ssa.xp_type == 'turn'
xp_swap = ssa.xp_type == 'swap'
xp_remove = ssa.xp_type == 'replace_gray'
xp_replace = ssa.xp_type == 'replace'
xp_turn_replace = ssa.xp_type == 'replace_turn'
xp_ok = ssa.success
xp_control = ssa.xp_type == 'control'
xp_delay_5 = ssa.xp_type == 'memory_5'
xp_carlos_high = ssa.comment.isin(['Carlos HIGH', 'Carlos group 2',
                                   'Carlos group 4'])
xp_carlos_low = ssa.comment.isin(['Carlos LOW', 'Carlos group 1',
                                  'Carlos group 3'])

# Normal test: CONTROL-SHOCK-TEST
ssa_test_3 = ssa.loc[xp_test & patt_3 & xp_ok]
# Test with 2 patterns (e.g. xxv)
ssa_test_2_1 = ssa.loc[xp_test & patt_2_1 & xp_ok]
# Test with gray instead of patterns
ssa_test_g = ssa.loc[xp_test & patt_g & xp_ok]
# Test with same pattern
ssa_test_1 = ssa.loc[xp_test & patt_1 & ~patt_g & xp_ok]
# Turn: CONTROL-SHOCK-TURN (+-)120
ssa_turn = ssa.loc[xp_turn & xp_ok]
# Remove pattern or change to dots: controls (where SHOCK was short and off)
ssa_remove_c = ssa.loc[xp_remove & (ssa.xp_duration < 300000) & xp_ok]
ssa_replace_c = ssa.loc[xp_replace & (ssa.xp_duration < 300000) & xp_ok]
# Change pattern and rotate
ssa_replace = ssa.loc[xp_replace & xp_ok & (ssa.xp_duration > 300000)]
ssa_turn_replace = ssa.loc[xp_turn_replace & xp_ok & (ssa.xp_duration > 300000)]
ssa_control = ssa.loc[xp_control & xp_ok]
ssa_swap = ssa.loc[xp_swap & xp_ok]
ssa_delay_5 = ssa.loc[xp_delay_5 & xp_ok]
