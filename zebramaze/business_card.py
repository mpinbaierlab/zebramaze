import os
import numpy as np

import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec

import zebramaze.md as md
import zebramaze.file_utils as fu
import zebramaze.analysis as als
import zebramaze.plotting as aplt
import zebramaze.trans as trans


def business_card(ss, save=False, traj_folder=None, comment=True,
                  t_o=False):
    """
    Plot business card for a fish

    :param ss:
    :param save:
    :param traj_folder:
    :param comment:
    :param t_o:
    :return:
    """
    if save:
        traj_folder = fu.check_folder(md.DATA_DIR, traj_folder)
        im_address = os.path.join(traj_folder, ss.xp_id.values[0] + '.png')
        if os.path.exists(im_address):
            return

    # if np.isnan(ss.length.iloc[0]):
    #    return
    col, row = 5, 12
    gs = gridspec.GridSpec(row, col)
    fig = plt.figure(figsize=(15, 10))
    axes = []
    if not ss.iloc[0].xp_type == 'control':
        axes.append(reac(ss.loc[ss.ss_no == 1], fig, gs,
                         slice(5, 8), 2, slice(5, 8), 3))
        axes.append(orient_reac(ss.loc[ss.ss_no == 1], fig, gs,
                                slice(2, 5), 4, slice(5, 8), 4))
    axes.append(info(ss, fig, gs, slice(0, 2), slice(0, col), comment))
    axes.append(traj(ss, fig, gs, slice(2, 5), slice(0, 4)))
    axes.append(occu(ss, fig, gs, slice(5, 8), 0, set_2_1=t_o))
    axes.append(entr(ss, fig, gs, slice(5, 8), 1, set_2_1=t_o))

    axes.append(raw(ss, fig, gs, slice(10, 12), slice(0, col-1)))
    axes.append(thigmo(ss, fig, gs, slice(8, 10), slice(0, col-1)))
    plt.tight_layout()
    gs.update(wspace=0.3, hspace=3)

    if save:
        plt.savefig(im_address)
        plt.close()
    else:
        plt.show()
        plt.close()


def thigmo(ss, f, gs, r1, r2):
    """
    Create thigmotaxis panel for business card

    :param ss:
    :param f:
    :param gs:
    :param r1:
    :param r2:
    :return:
    """
    ax = f.add_subplot(gs[r1, r2])
    plot_thigmo(ss, hmm=True, ax=ax)
    ax.set_xlim([0, ss.iloc[0].xp_duration])
    ax.set_ylim([0, 120])
    ax.spines['bottom'].set_visible(False)
    ax.set_xticks([])
    ax.grid(color=(0, 0, 0, 0.2), ls=':')
    return ax


def info(ss, f, gs, r1, r2, comment=False):
    """
    Create general information panel for a business card

    :param ss:
    :param f:
    :param gs:
    :param r1:
    :param r2:
    :param comment:
    :return:
    """
    from zebramaze.analysis import fish_types
    ax = f.add_subplot(gs[r1, r2])
    over = bool(fish_types(ss)['Overstayer'])
    s_0 = ss.iloc[0]
    if ss.shape[0] > 1:
        s_1 = ss.iloc[1]
    else:
        s_1 = s_0
    s = "ID: {}; xp_type: {}; Age: {}; Date: {}; {} mA;" \
        "\nTimed out: {}".format(s_0.xp_id, s_0.xp_type, s_0.xp_age,
                                 s_0.xp_date.date(), s_1.stim_current, over)
    y_s = 0.25
    if not s_0.success:
        s = "{}\nFAILED: {}".format(s, s_0.comment)
    else:
        if comment:
            s = "{}\n{}".format(s, s_0.comment)

    ax.text(0, y_s, s, **{"size": 20})
    ax.axis('off')
    return ax


def traj(ss, f, gs, r1, r2):
    """
    Create trajectory panel for a business card

    :param ss:
    :param f:
    :param gs:
    :param r1:
    :param r2:
    :return:
    """
    ax = f.add_subplot(gs[r1, r2])
    aplt.plot_traj(ss.xp_id.values[0], ax1=ax, save=False, split_test=True,
                   split_shock=True, join=True,
                   split_control=ss.iloc[0].xp_type == 'control')
    return ax


def occu(ss, f, gs, r1, r2, set_2_1=False):
    """
    Create occupancy panel for a business card

    :param ss:
    :param f:
    :param gs:
    :param r1:
    :param r2:
    :param set_2_1:
    :return:
    """
    ax = f.add_subplot(gs[r1, r2])
    aplt.plot_sliding(ss, als.occupancy, ax=ax, excl=False, show=False,
                      name='Arm occupancy in sliding window', two_one=set_2_1,
                      **{'val': [0, 1, 2, 3], 'win_size': 300,
                         'sliding_overlap': 270})
    return ax


def entr(ss, f, gs, r1, r2, set_2_1=False):
    """
    Create entry frequency panel for a business card

    :param ss:
    :param f:
    :param gs:
    :param r1:
    :param r2:
    :param set_2_1:
    :return:
    """
    ax = f.add_subplot(gs[r1, r2])
    aplt.plot_sliding(ss, als.entry, ax=ax, show=False, excl=False,
                      two_one=set_2_1, name='Entry frequency in sliding window',
                      **{'val': [0, 1, 2], 'win_size': 300, 'sliding_overlap':
                         270})
    return ax


def reac(ss, f, gs, r1, r2, r3, r4):
    """
    Create reactivity panel for a business card

    :param ss:
    :param f:
    :param gs:
    :param r1:
    :param r2:
    :param r3:
    :param r4:
    :return:
    """
    ax2 = f.add_subplot(gs[r1, r2])
    ax1 = f.add_subplot(gs[r3, r4])

    plot_reac(ss.iloc[0], ax1=ax1, ax2=ax2, overhang=24)

    return ax1, ax2


def raw(ss, f, gs, r1, r2):
    """
    Create raw transitions panel for a business card

    :param ss:
    :param f:
    :param gs:
    :param r1:
    :param r2:
    :return:
    """
    ax = f.add_subplot(gs[r1, r2])
    sr = 3600.0  # to have x axis in minutes
    width = 0.05
    spacing = 3  # distance on y axis between experiments
    grr = ss.groupby('xp_id')
    trans.transitions_raw(ss, ax, spacing, width, sr, label=False)
    plt.setp(ax, xlabel='Time in session (min)',
             yticks=spacing * (np.arange(1, len(grr) + 1)) - 2)
    ax.spines['left'].set_visible(False)
    ax.set_yticks([])
    ax.set_title('Arm transitions')
    return ax


def orient_reac(ss, f, gs, r1, r2, r3, r4):
    """
    Create orientation panel for a business card

    :param ss:
    :param f:
    :param gs:
    :param r1:
    :param r2:
    :param r3:
    :param r4:
    :return:
    """
    ax1 = f.add_subplot(gs[r1, r2], polar=True)
    ax2 = f.add_subplot(gs[r3, r4], polar=True)
    plot_orient(ss.iloc[0], ax1=ax1, ax2=ax2)

    ax1.set_facecolor((1, 1, 1))
    ax1.grid(color=(0, 0, 0, 0.3), ls=':')
    ax2.set_facecolor((1, 1, 1))
    ax2.grid(color=(0, 0, 0, 0.3), ls=':')
    return ax1, ax2

def plot_thigmo(ssub, hmm=True, ax=None):
    """
    Plot thigmotaxis index for an experiment

    :param ssub:
    :param hmm:
    :param ax:
    :return:
    """
    f, ax = aplt.create_mpl_ax(ax)
    thigmo_col = {0: 'b', 1: 'r', 2: 'purple', 3: 'orange'}
    th_all, z_all = als.thigmo(ssub, hmm_load=hmm)
    for cc, sub_thigmo in th_all.groupby('col'):
        ax.plot(sub_thigmo['x'], sub_thigmo['th'],
                c=thigmo_col[cc])

    # if hmm and os.path.exists(als.hmm_folder+'hmm.p'):
    #     widths = dict([(k, 100) for k in z_all.val.unique()])
    #
    #     def colmap(v): return {0: 'k', 1: 'w', 2: '#D2D2D2'}[v]
    #     viz.intv.ribbon(z_all, 1, colmap, 50, widths,
    #                     'val', 'a', 'b', ax, **{'alpha': 0.5})


def plot_orient(ssub, ax1=None, ax2=None, **kw):
    """
    Plot polar histogram of reactivity vs orientation

    :param ssub:
    :param ax1:
    :param ax2:
    :param kw:
    :return:
    """
    f, ax1 = aplt.create_mpl_ax(ax=ax1, **kw)
    f, ax2 = aplt.create_mpl_ax(ax=ax2, **kw)
    a_bins, reac_bins, num_bins, cond = als.sh_orient(ssub)
    ax1.bar((a_bins*np.pi/180)[cond], reac_bins[cond], width=0.1,
            bottom=0, color='r')
    ax1.bar((a_bins * np.pi / 180)[~cond], reac_bins[~cond], width=0.1,
            bottom=0, color='b')
    ax1.set_xlabel('Mean reactivity to shocks as a function\nof orientation in '
                   'the arm')
    ax2.bar(a_bins*np.pi/180, num_bins, width=0.1, bottom=0)
    ax2.set_xlabel('Number of shocks as a function\nof orientation in '
                   'the arm')


def plot_reac(ssub, ax1=None, ax2=None, win_len=6, overhang=0, **kw):
    """
    Plot reactivity

    :param ssub:
    :param ax1:
    :param ax2:
    :param win_len:
    :param overhang:
    :param kw:
    :return:
    """

    f, ax1 = aplt.create_mpl_ax(ax=ax1, **kw)
    f, ax2 = aplt.create_mpl_ax(ax=ax2, **kw)
    combine, a_cond, c_cond = als.sh_reactivity(ssub, winlen=win_len,
                                                overhang=overhang)

    if combine[a_cond & c_cond].shape[0] != 0:
        ax1.plot(np.arange(win_len + overhang),
                 combine[c_cond].T, c='r', alpha=0.05)
        ax1.plot(np.arange(win_len + overhang), combine[c_cond].mean(0),
                 'k', linewidth=2.5)
    # if combine[~a_cond & c_cond].shape[0] != 0:
    #    ax1.plot(np.arange(winlen + overhang),
    #             combine[~a_cond & c_cond].T, c='b', alpha=0.05)
    #    ax1.plot(np.arange(winlen + overhang), combine[~a_cond &
        # c_cond].mean(0),
    #             'b', linewidth=2.5)

    plt.setp(ax1, xlabel='Shock frames', ylabel='Displacement [a.u.]')
    ax1.set_title('Reactivity to shocks')
    ax1.set_ylim(0, 30)

    ax2.bar([0.6, 1.6], [combine[a_cond & c_cond].shape[0],
                         combine[a_cond & ~c_cond].shape[0]], color='r')
    ax2.bar([2.6, 3.6], [combine[~a_cond & c_cond].shape[0],
                         combine[~a_cond & ~c_cond].shape[0]], color='b')
    ax2.set_xticklabels(['', 'bouts', 'null', 'bouts', 'null'], rotation=30)
    ax2.set_ylabel('# of shock intervals')
    ax2.set_xlim(0, 5)


if __name__ == '__main__':
    ss_sub = md.ssa_turn.loc[md.xp_new]
    for g, gg in ss_sub.groupby('xp_id'):
        business_card(gg, save=True, traj_folder='bc_turn_new',
                      comment=True,
                      t_o=False)
