import numpy as np
import pandas as pd

from matplotlib import pyplot as plt
from matplotlib.transforms import blended_transform_factory
from PIL import Image

import scipy.interpolate as inter
from scipy.cluster import hierarchy
from scipy.stats import mannwhitneyu, kruskal
from sklearn.decomposition import PCA

import seaborn as sns

import zebramaze.md as md
import zebramaze.trans as trans
import zebramaze.analysis as als
import zebramaze.geom_utils as gu
import zebramaze.plotting as aplt

clust_num = 3


def plot_bouts(bout_arr, sh_dur, ax, alpha, mean=False, c='b'):
    """
    Plot multiple displacement curves, aligned at the shock onset
    :param bout_arr:
    :param sh_dur:
    :param ax:
    :param alpha:
    :param mean:
    :param c:
    :return:
    """

    ax.plot(bout_arr.T, c=c, alpha=alpha, lw=1)
    if mean:
        ax.plot(bout_arr.mean(0), c='k', lw=3)

    ax.set_xlabel('Time from shock onset [s]')
    ax.set_title('Group-wise\nfish responses to shocks', pad=10)
    ax.set_xticks([0, 30, 60, 90])
    ax.set_xticklabels([0, 0.1, 0.2, 0.3])
    ax.set_xlim(0, 110)
    ax.set_ylim(-0.5, 7)
    # create a vertical bar to mark to shock time
    ax.fill_between([0, sh_dur], [ax.get_ylim()[1]] * 2, [ax.get_ylim()[0]] * 2,
                    color=(1, 0, 0, 0.4))

    return ax


def cluster_ang(yp, y_pred, mm, ax=None, c='b'):
    """
    Plot angle distribution for a particular cluster
    yp - which cluster
    mm - metadata for clusters
    """
    if ax is None:
        f = plt.figure(figsize=(8, 8))
        ax = f.add_subplot('111', polar=True)
    aa = mm[np.isin(y_pred, yp), 2].astype(float)
    aa[aa < 0] += 360
    aa[aa > 360] -= 360
    a_bins = np.linspace(0, 360, 41)[:-1]
    num_bins = []
    for b in a_bins:
        r = aa[(aa >= b) & (aa < b + 9)]
        num_bins.append(r.shape[0])
    num_bins = np.array(num_bins)

    ax.bar((a_bins*np.pi/180), num_bins, width=0.15, bottom=0, color=c)
    ax.set_facecolor((1, 1, 1))
    ax.grid(color=(0, 0, 0, 0.3), ls=':')
    ax.set_title('Fish orientation'.format(yp[0]+1), pad=17)
    for it in ax.get_xticklabels():
        it.set_fontsize(10)
    for it in ax.get_yticklabels():
        it.set_fontsize(8)


def figure_3e(fig, sh_dur, combine, mm, y_pred, cf, left, bottom, w, h):
    h = h-(clust_num-1)*cf['gap_m']
    w = w-cf['gap_m']
    clust_name = ['"Early onset"\n(LA$_{early}$)',
                  '"Late onset"\n(LA$_{late}$)', '"High amplitude"\n(HA)']
    for i in range(clust_num):
        yp = [i]
        bottom_i = bottom+(clust_num-1-i)*(h/clust_num+cf['gap_m'])
        ax1 = fig.add_axes([left, bottom_i, w/2, h/clust_num],
                           polar=True)
        cluster_ang(yp, y_pred, mm, ax1, c=sns.color_palette()[i])
        ax2 = fig.add_axes([left+w/2+cf['gap_m'], bottom_i, w/2, h/clust_num])
        if i == 2:
            al = 0.05
        else:
            al = 0.005
        plot_bouts(combine[np.isin(y_pred, yp)], sh_dur, ax2,
                   mean=True, alpha=al, c=sns.color_palette()[i])
        ax2.text(1, 1, "N = {}".format(combine[np.isin(y_pred, yp)].shape[0]),
                 ha='right', va='top', transform=ax2.transAxes)
        fig.text(left, bottom_i+h/2/clust_num,
                 '{}'.format(clust_name[yp[0]]),
                 rotation='vertical', ha='center', va='center')

        if i == 0:
            ax1.set_xticklabels(['', '', 'To anode (+)', '', '', '', '', '',
                                 ''], fontsize=8)
            ax1.set_title("Number of responses as a\nfunction of orientation "
                          "in the arm", pad=17)
        if (i != 0) & (i != clust_num-1):
            ax1.set_xticklabels('')
        if i == clust_num-1:
            ax1.set_xticklabels(['', '', '', '', '', '', 'To cathode (-)',
                                 ''], fontsize=8)
        if i != 0:
            ax1.set_title('')
            ax2.set_title('')
        if i != clust_num-1:
            ax2.set_xticklabels([])
            ax2.set_xlabel('')

        ax1.tick_params(axis='x', pad=-5)
        ax1.spines['polar'].set_visible(False)
        r_max = ax1.get_rmax()
        r_step = max(10, r_max / 2 - np.mod(r_max / 2, 10))
        r_ticks = np.arange(0, 2 * r_step + 1, r_step)
        ax1.set_rticks(r_ticks[1:])
        ax1.set_rlabel_position(200)
        ax1.set_rgrids(ax1.get_yticks(), va='top', ha='right')


def calc_displacement(ss, timing=1, win=18000, bout_thresh=5, dx=0.2):
    """

    :param bout_thresh: int
    :param win:
    :param ss:
    :param timing:
    :param dx: resolution step for interpolation
    :return:
    """
    l0 = 23  # time window to look for bouts from shock onset, [frames]

    # safety flanks around l0 to extract full shape of bouts
    before_sh, after_sh = 10, l0

    # pts for interpolation
    xs = np.arange(0, l0 + before_sh + after_sh - 1, dx)

    disp_meta = []  # for bout shapes and angles
    disp_raw = []  # for smoothed bouts in original time frame

    for i, s in ss.iterrows():
        co = pd.read_hdf(s.ss_coords_path)
        if 'armfix' in co.columns:
            co.loc[:, 'arm'] = co.armfix
        disp = gu.displacement(co, x='xfix', y='yfix', smooth=True)

        nosh_arm = [0, 1, 2]
        nosh_arm.remove(s.stim_at)

        sh_ind = trans.sh_index(co)
        # make sure that all reaction intervals lie within the session timeline
        sh_ind = sh_ind[((sh_ind - before_sh) >= 0) &
                        ((s.ss_duration - sh_ind - after_sh - l0) >= 0)]
        split = [disp.values[sh - before_sh:sh + l0 + after_sh] for sh in
                 sh_ind]
        split = np.array(split)

        unispline = lambda y: inter.UnivariateSpline(np.arange(y.shape[0]), y,
                                                     s=20)
        unispline_strong = lambda y: inter.UnivariateSpline(
            np.arange(y.shape[0]), y, s=100)

        spline_split = np.zeros(split.shape[0]).astype(object)
        ind = split.max(1) < 7
        # stronger smoothing factor for low amplitude bouts to avoid sharp peaks
        spline_split[ind] = np.apply_along_axis(unispline_strong, 1, split[ind])
        spline_split[~ind] = np.apply_along_axis(unispline, 1, split[~ind])

        # find bout onsets and amplitudes within the l0 interval
        bout_split = []
        for spl in spline_split:
            bout_split.append(
                spl(xs)[int(before_sh / dx):int(before_sh / dx + l0 / dx)])

        splt = []
        m_center = s.env_geometry.mean(1).mean(0)
        arm_orient = gu.find_ang(m_center, s.env_geometry.mean(1).T)
        for i, sp in enumerate(bout_split):
            if np.max(sp) > bout_thresh:
                co_ind = co.iloc[sh_ind[i]+(timing-1)*win:sh_ind[i]+win*timing]
                if co_ind.shape[0] < win:
                    timing_list = [np.NaN] * 3
                else:
                    sh = (co_ind.arm == s.stim_at)
                    nosh1 = (co_ind.arm == nosh_arm[0])
                    nosh2 = (co_ind.arm == nosh_arm[1])
                    occu = (sh.sum() - (nosh1.sum()+nosh2.sum())/2)/win
                    entr_sum = (co_ind.arm.diff() != 0).sum() / 2
                    entr = (sh.astype(int).diff() == 1).sum() - \
                        ((nosh1.astype(int).diff() == 1).sum() +
                         (nosh2.astype(int).diff() == 1).sum())/2
                    timing_list = [occu, entr, entr_sum]
            else:
                timing_list = [np.NaN] * 3

            splt.append(np.hstack([sh_ind[i], s.xp_id,
                                   co.iloc[sh_ind[i]].ang -
                                   arm_orient[s.stim_at]] + timing_list))
        disp_meta.append(splt)
        disp_raw += bout_split
    disp_meta = np.vstack(disp_meta)
    disp_raw = np.vstack(disp_raw)
    return disp_raw, disp_meta


def figure_3(dpi=100):
    """
    Shock reactions
    :return:
    """

    cf = aplt.CF['BORDERS']
    f_w, f_h = 8, 10
    fig = plt.figure(figsize=(f_w, f_h))
    timing = 1
    dx = 0.2

    # shock trajectory
    top_b = 0.8
    middle_b = 0.35
    bouts_w = 0.35
    schema_w = 0.3
    prob_l = cf['l_margin']+schema_w+cf['gap_w']
    prob_w = 0.25
    box_h = middle_b-cf['b_margin']-3*cf['gap_h']
    box_w = (1-cf['r_margin']-cf['l_margin']-4*cf['gap_w'])/2

    ssa = md.ssa_replace[md.xp_new]
    ssa = ssa.loc[ssa.sh_duration == 4]
    ft = als.fish_types(ssa)
    print("{} fish with {} overstayers".format(
        int(ssa.shape[0] / len(ssa.ss_no.unique())), len(ft['Overstayer'])))
    ssa = ssa.loc[ssa.xp_id.isin(ft['Non-responder']+ft['Two arms']+ft[
        'Center']+ft['One arm'])]

    ss_sub = ssa.loc[ssa.ss_no == 1]
    disp_raw, disp_meta = calc_displacement(ss_sub, timing=timing, dx=dx)

    # consider only displacements with amplitude > 5 as bouts
    bout_ind = np.max(disp_raw, 1) > 5
    bout_traces = disp_raw[bout_ind]
    # convert pix/frame to cm/s (take intro account interpolation step
    bout_traces = bout_traces/cf['scale']*cf['fps']*dx

    # split metadata about each bout (id, ind, angle) and bout shape
    bout_meta = disp_meta[bout_ind]

    print("Number of response bouts is {}".format(bout_meta.shape[0]))
    print("Number of non-responses is {}".format(disp_meta.shape[0] -
                                                 bout_meta.shape[0]))

    # PCA to find features for clustering
    pca = PCA()
    _ = pca.fit_transform(bout_traces)
    min_comp = min(
        np.where(np.cumsum([0] + list(pca.explained_variance_ratio_)) > 0.9)[0])
    print("{} components explain > 90% of variance".format(min_comp))
    pca = PCA(n_components=min_comp)
    bout_proj = pca.fit_transform(bout_traces)

    # cluster PCA-defined projections using Ward's linkage
    Z = hierarchy.linkage(bout_proj, 'ward')
    y_pred = hierarchy.cut_tree(Z, n_clusters=clust_num)[:, 0]

    clust_df = np.vstack([bout_meta[:, 3:6].astype(float).T, y_pred]).T
    time_columns = ['occu', 'entry', 'entry_sum', 'pred']
    clust_df = pd.DataFrame(clust_df, columns=time_columns)

    moving = clust_df['entry_sum'] > 3
    clust_df.loc[moving, 'freq'] = clust_df.loc[moving, 'entry'] / \
        clust_df.loc[moving, 'entry_sum']
    clust_df.loc[~moving, 'freq'] = np.NaN

    # -------------SCHEMATIC FOR ORIENTATION---------------
    ax = fig.add_axes([cf['l_margin'], top_b-1.5*cf['gap_h'], schema_w,
                       1-cf['t_margin']-top_b+2*cf['gap_h']])
    arr_img = plt.imread('../plots/orientation.png', format='png')
    ax.imshow(arr_img, aspect='equal')
    ax.axis('off')

    # ----REACTION PROBABILITY/STRENGTH VS ORIENTATION-----
    ax1 = fig.add_axes([prob_l, top_b-0.5*cf['gap_h'], prob_w,
                        1-cf['t_margin']-top_b-cf['gap_h']], polar=True)
    ax2 = fig.add_axes([prob_l+prob_w+3*cf['gap_w'], top_b,
                        1-(prob_l+prob_w+2*cf['gap_w'])-cf['r_margin'],
                        1-cf['t_margin']-top_b])
    prob_dict, reac_dict = als.calc_orient(disp_meta[:, :3], disp_raw,
                                           bout_thresh=5)
    aplt.plot_radial_prob(prob_dict, ax=ax1, ax_extra=None)
    aplt.plot_reac_bar(reac_dict, dx, ax2)

    # ----------PLOT ALL BOUT TRACES ALIGNED TO SHOCK ONSET-------
    ax = fig.add_axes([cf['l_margin'] + cf['gap_w'], middle_b, bouts_w,
                       top_b - middle_b - 3*cf['gap_h']])
    plot_bouts(bout_traces, ss_sub.sh_duration.min()/0.2, ax, alpha=0.01, c='k')
    ax.set_title('All shock-triggered bouts')
    ax.set_ylabel('Speed [cm/s]')
    ax.text(1, 1, "N = {}".format(bout_traces.shape[0]),
            ha='right', va='top', transform=ax.transAxes)

    # ----------------------PLOT BOUT CLUSTERS---------------------
    figure_3e(fig, ssa.sh_duration.min()/0.2, bout_traces, bout_meta, y_pred,
              cf, cf['l_margin']+cf['gap_w']*4+bouts_w, middle_b,
              1 - (cf['l_margin']+cf['gap_w']*4+bouts_w) - cf['r_margin'],
              top_b - middle_b - 3.5 * cf['gap_h'])

    # -------------PLOT OC/EF SCORES FOR BOUT CLUSTERS-------------
    ax1 = fig.add_axes([cf['l_margin']+cf['gap_w'], cf['b_margin'], box_w,
                        box_h])
    clust_signif(ax1, clust_df, 'occu', clust_num)
    ax1.set_ylabel('OC score')

    ax2 = fig.add_axes([cf['b_margin']+4*cf['gap_w']+box_w, cf['b_margin'],
                        box_w, box_h])
    clust_signif(ax2, clust_df, 'freq', clust_num)
    ax2.set_ylabel('EF score')

    top_panel = 0.96
    middle_panel = 0.7
    bottom_panel = 0.25
    left_panel = 0.03
    panel_font = 15
    fig.text(left_panel, top_panel, 'A', fontsize=panel_font)
    fig.text(0.35, top_panel, 'B', fontsize=panel_font)
    fig.text(0.65, top_panel, 'C', fontsize=panel_font)
    fig.text(left_panel, middle_panel, 'D', fontsize=panel_font)
    fig.text(0.47, middle_panel, 'E', fontsize=panel_font)
    fig.text(left_panel, bottom_panel, 'F', fontsize=panel_font)
    fig.savefig('../figures/figure_4.png', dpi=dpi)
    im = Image.open('../figures/figure_4.png'.format(5))
    im.save('../figures/figure_4.tiff'.format(5), compression='tiff_lzw')


def clust_signif(ax, df, measure, clust_num):
    ax.hlines(df.groupby('pred')[measure].mean(), [-0.25, 0.75, 1.75],
              [0.25, 1.25, 2.25])
    lvl0 = 1
    lvl_gap = 0.12
    sns.swarmplot(x='pred', y=measure, ax=ax, data=df, size=1)

    ax.set_ylim(-0.52, 1.02)
    ax.set_yticks([-0.5, 0, 0.5, 1])
    ax.set_xlabel('Response type')
    ax.set_xticklabels(['$LA_{early}$', '$LA_{late}$', 'HA'])

    pval_list = []
    clust_list = range(clust_num)
    f, krus = kruskal(*[df.loc[df.pred == cl_i, measure].values for
                        cl_i in clust_list])
    print("{} F {} p-value {}".format(measure, f, krus))
    for cl_i in clust_list:
        for cl_j in range(cl_i + 1, clust_num):
            df_i = df.loc[df.pred == cl_i, measure].values
            df_j = df.loc[df.pred == cl_j, measure].values
            ui, pi = mannwhitneyu(df_i, df_j)
            pval_list.append(pi * 6)
            es = 1 - 2*ui/df_i.shape[0]/df_j.shape[0]
            print("{} Gr. {} {:.02f}+-{:.03f} Gr. {} {:.02f}+-"
                  "{:.03f} p-value = {} effect size = {:.02f} "
                  "".format(measure, cl_i, np.nanmean(df_i), np.nanstd(df_i),
                            cl_i, np.nanmean(df_j), np.nanstd(df_j), pi, es))
            trans = blended_transform_factory(ax.transData, ax.transAxes)

            lvl = lvl0 + (cl_j - cl_i - 1) * cl_i * lvl_gap + \
                lvl_gap * sum(range(0, cl_j - cl_i))
            ax.plot([cl_i + 0.1, cl_i + 0.1, cl_j - 0.1, cl_j - 0.1],
                    [-0.01 + lvl, lvl, lvl, -0.01 + lvl],
                    c='#888888', transform=trans, clip_on=False)
            if pi < 0.01:
                msg = '$p = ${:.0e}'.format(pi)
            else:
                msg = '$p = ${:.2f}'.format(pi)
            ax.text((cl_i + cl_j)/2, lvl, msg,
                    ha='center', va='bottom', transform=trans)


if __name__ == '__main__':
    figure_3(dpi=300)
