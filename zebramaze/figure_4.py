import numpy as np
from scipy.cluster import hierarchy

from PIL import Image

import matplotlib.pyplot as plt
import matplotlib.lines as mlines
import matplotlib.colors as mclr
import matplotlib.colorbar as mcbr
import matplotlib.spines as spn

import zebramaze.md as md
import zebramaze.analysis as als
import zebramaze.plotting as aplt
import zebramaze.maze_utils as mu


CF_X = aplt.CF['HEATMAPS_X']  # load configuration parameters for axes
CF_Y = aplt.CF['HEATMAPS_Y']


def heat_figure(fig, ss_list, strategy_names, arm_names,
                session_names, ss_sample, seq_list,
                push_high_shocked=0, x0=0., x1=1., y0=0., y1=1.,
                invert=False):
    """
    Plot Figure 4. First introduction of strategies, heatmaps and examples.
    """

    # rescale axes configurations depending on the fraction of the figure
    # that is used.
    calib = True
    Title = True
    dx, dy = (x1 - x0), (y1 - y0)
    cf_x = {k: v*dx for k, v in CF_X.items()}
    cf_y = {k: v*dy for k, v in CF_Y.items()}

    # load dataset and generated heatmaps
    occu, xp_ind, y_pred, clust_dict, clust_names = als.load_heat()

    occu = occu[np.isin(xp_ind, ss_sample.xp_id.values)]
    y_pred = y_pred[np.isin(xp_ind, ss_sample.xp_id.values)]
    xp_test = xp_ind[np.isin(xp_ind, ss_sample.xp_id.values)]

    # --------------CALCULATE HEATMAP AXES PARAMETERS---------------
    width_ratios = [3, 1, 3, 1]
    gap_ratios = [0.7, 1.4, 0.7]
    height_ratios = [np.isin(y_pred, clust_dict[x]).sum() for x in clust_names]
    nonzero_h = [x for x in height_ratios if x != 0]
    heat_h = (dy - cf_y['b_maze']) - cf_y['b_margin'] - cf_y['t_margin'] - \
        len(nonzero_h)*cf_y['gap_h']
    heat_w = cf_x['heat_w'] - cf_x['l_margin'] - \
        cf_x['gap_w']*(sum(gap_ratios) + 3)
    # scaling ratio for height and width
    ratio_h = heat_h/sum(height_ratios)
    ratio_w = heat_w/sum(width_ratios)

    # --------DRAW COLORBAR AXES ON THE LEFT OF THE PLOT-------------
    axc = fig.add_axes([x0 + cf_x['l_cb'], y0 + cf_y['b_margin']+0.4*dy,
                        cf_x['cb_w'], cf_y['cb_h']])
    cb = mcbr.ColorbarBase(axc, cmap='RdBu_r', ticks=[-1, 0, 1],
                           norm=mclr.SymLogNorm(linthresh=0.05, linscale=0.05,
                                                vmin=-1, vmax=1))
    axc.text(0, 1.08, r'Preference', ha='left', va='bottom')
    axc.text(0, -0.08, r'Avoidance', ha='left', va='top')
    cb.ax.set_yticklabels(['-1', '0', '1'])
    axc.tick_params(axis='y', length=0)
    cb.outline.set_visible(False)

    axc = fig.add_axes([x0 + cf_x['l_cb'], y0 + cf_y['b_margin']+0.03*dy,
                        cf_x['cb_w'], cf_y['cb_h']])
    cb = mcbr.ColorbarBase(axc, cmap='Greys', ticks=[0, 1],
                           norm=mclr.Normalize(vmin=0, vmax=1))
    axc.text(0, 1.08, r'Only center', ha='left', va='bottom')
    axc.text(0, -0.08, r'No center', ha='left', va='top')
    cb.ax.set_yticklabels(['0', '1'])
    axc.tick_params(axis='y', length=0)
    cb.outline.set_visible(False)

    # -------DRAW MAZE SCHEMATICS FOR CONDITIONING AND TEST SESSIONS
    maze_left = x0 + cf_x['l_margin']+4*cf_x['gap_w']
    maze = aplt.plot_maze(cont=False, arr=False, clrs=(1, 1, 1),
                          seq=seq_list[0], shock=True)
    axm = fig.add_axes([maze_left, y1 - cf_y['b_maze'], cf_x['maze_w'],
                        cf_y['maze_h']])
    axm.text(0.5, -0.1, session_names[0], ha='center', va='top',
             transform=axm.transAxes)
    axm.axis('off')
    axm.imshow(maze)

    maze = aplt.plot_maze(cont=False, arr=False, clrs=(1, 1, 1),
                          seq=seq_list[1], shock=False)
    axm = fig.add_axes([maze_left + cf_x['maze_w'] + 0.15*dx,
                        y1 - cf_y['b_maze'], cf_x['maze_w'], cf_y['maze_h']])
    axm.text(0.5, -0.1, session_names[1], ha='center', va='top',
             transform=axm.transAxes)
    axm.axis('off')
    axm.imshow(maze)

    # ------SET UP AXES FOR TRAJECTORY EXAMPLES------------
    fig.text(cf_x['heat_w'] + 3*cf_x['gap_w'] +
             (x1 - cf_x['heat_w'] - cf_x['r_margin'] - 3*cf_x['gap_w'])/2,
             y1 - cf_y['gap_h'], 'Individual trajectories', ha='center')
    traj_w = (x1 - cf_x['heat_w'] - cf_x['r_margin'] - 4*cf_x['gap_t'] -
              3*cf_x['gap_w'])/5
    titles = ['Habituation', 'Conditioning', 'Test']
    part_titles = ['Last 5\'', 'First 5\'', 'Last 5\'', 'First 5\'',
                   'Second 5\'']
    fig.text(cf_x['heat_w'] + 3 * cf_x['gap_w'] + traj_w/2,
             y1 - 1.5*cf_y['gap_h'], titles[0], ha='center')
    fig.text(cf_x['heat_w'] + 3*cf_x['gap_w'] + 1.5*cf_x['gap_t'] + 2*traj_w,
             y1 - 1.5*cf_y['gap_h'], titles[1], ha='center')
    fig.text(cf_x['heat_w'] + 3*cf_x['gap_w'] + 3.5*cf_x['gap_t'] + 4*traj_w,
             y1 - 1.5*cf_y['gap_h'], titles[2], ha='center')
    # new clear axis overlay with 0-1 limits for dashed vertical lines
    ax_fig = fig.add_axes([0, 0, 1, 1], facecolor=(1, 1, 1, 0))
    ax_fig.axis('off')
    traj_kw = {'s': 0.8, 'alpha': 0.02}

    # iterate through strategies
    nonzero_i = 0
    for i, height_i in enumerate(height_ratios):
        if height_i == 0:
            continue
        ax_h = height_i/sum(height_ratios)*heat_h
        ax_b_i = y0 + cf_y['b_margin'] + sum(height_ratios[i+1:])*ratio_h +\
            (len(nonzero_h)-nonzero_i-1)*cf_y['gap_h']

        occu_i = occu[np.isin(y_pred, clust_dict[clust_names[i]])[:, 0]]
        xp_i = xp_test[np.isin(y_pred, clust_dict[clust_names[i]])[:, 0]]
        if occu_i.shape[0] == 0:
            continue  # skip to next if strategy i is not present

        # Reorder rows in each group based on linkage in 'Test' session
        if clust_names[i] == 'Center-Non-responder':
            # Z_i = hierarchy.linkage(occu_i[:, 11:12], method='ward',
            #                         metric='euclidean')
            # Z_i = hierarchy.optimal_leaf_ordering(Z_i, occu_i[:, 11:12])
            sort_ind = occu_i[:, 11].argsort()
        else:
            if occu_i.shape[0] == 1:
                sort_ind = [0]
            else:
                Z_i = hierarchy.linkage(occu_i[:, 8:11], method='ward',
                                        metric='euclidean')
                Z_i = hierarchy.optimal_leaf_ordering(Z_i, occu_i[:, 8:11])
                sort_ind = hierarchy.leaves_list(Z_i)
        occu_ord = occu_i[sort_ind]
        xp_ord = xp_i[sort_ind]

        # If occupancy of the shocked arm (column 0) was higher than
        # threshold, move those rows to the top of the group
        if (push_high_shocked != 0):# & (clust_names[i] !=
                                     #  'Center-Non-responder'):
            ord_th = -0.05
            ord_ind = occu_ord[:, 8] > ord_th
            if push_high_shocked == 1:
                occu_ord = np.vstack([occu_ord[ord_ind], occu_ord[~ord_ind]])
                xp_ord = np.hstack([xp_ord[ord_ind], xp_ord[~ord_ind]])
            else:
                # push to the bottom
                occu_ord = np.vstack([occu_ord[~ord_ind], occu_ord[ord_ind]])
                xp_ord = np.hstack([xp_ord[~ord_ind], xp_ord[ord_ind]])
        if invert:# & (clust_names[i] != 'Center-Non-responder'):
            occu_ord = np.flip(occu_ord, 0)
            xp_ord = np.flip(xp_ord, 0)

        # iterate through sessions
        for j, width_j in enumerate(width_ratios):
            ax_w = width_j*ratio_w
            ax_l_j = x0 + cf_x['l_margin'] + (3 + sum(gap_ratios[:j]))*cf_x[
                'gap_w'] + sum(width_ratios[:j])*ratio_w
            ax_j = fig.add_axes([ax_l_j, ax_b_i, ax_w, ax_h])

            # choose colormap depending on the arm/center order
            if j % 2:
                col = ('Greys', mclr.Normalize(vmin=-0.1, vmax=1))
            else:
                col = ('RdBu_r', mclr.SymLogNorm(linthresh=0.05, linscale=0.05,
                                                 vmin=-1, vmax=1))

            # draw the heatmap j for strategy i

            ax_j.imshow(occu_ord[:, 4+sum(width_ratios[:j]):4+sum(
                width_ratios[:j+1])], cmap=col[0], norm=col[1], aspect='auto')
            if j == 0:
                ax_j.tick_params(axis='y', length=0)
                ax_j.set_yticks(np.arange(0, height_i))
                # ax_j.set_yticklabels(xp_ord)
                ax_j.set_yticklabels(np.arange(sum(height_ratios[:i])+1,
                                              sum(height_ratios[:i+1])+1),
                                     size=4)
            else:
                ax_j.set_yticks([])
            ax_j.set_xticks([])
            for child in ax_j.get_children():
                if isinstance(child, spn.Spine):
                    child.set_visible(True)
                    child.set_color('#888888')

            # label the arms for the bottom-most strategy
            if nonzero_i == len(nonzero_h)-1:
                # for arms use first three labels of arm_names
                if width_j == 3:
                    for k in range(width_j):
                        ax_j.text(1/6+k/3, -0.02, arm_names[j//2][k],
                                  ha='right', va='center', rotation=90,
                                  rotation_mode='anchor',
                                  transform=ax_j.transAxes)
                # for the center use the last label in arm_names
                else:
                    ax_j.text(0.5, -0.02, arm_names[j//2][-1], ha='right',
                              va='center', rotation=90, rotation_mode='anchor',
                              transform=ax_j.transAxes)

            # draw trajectory examples for the current group
            if j == len(width_ratios) - 1:
                for n, ss_n in zip(strategy_names, ss_list):
                    # find where in the current group the example is,
                    # skip to next if the example is not in the group
                    x_n = np.where(xp_ord == ss_n.xp_id.iloc[0])[0]
                    if x_n.shape[0] == 0:
                        continue

                    # draw the arrowhead
                    ax_j.annotate(n, xy=(1.1, 1 - (x_n + 0.5)/height_i),
                                  xytext=(1.3, 1 - (x_n + 0.5)/height_i),
                                  annotation_clip=False, va='center',
                                  xycoords='axes fraction',
                                  arrowprops=dict(arrowstyle="-|>", color='k'))

                    traj_bottom = ax_b_i - cf_y['traj_h']/2 + \
                        (height_i - x_n[0] - 0.5)*ratio_h

                    s = 0  # a counter for sessions
                    ang = 0  # how much to rotate the maze drawing
                    # iterate through sessions of the example
                    for s_ind, ss in ss_n.iterrows():

                        if s == 0:
                            ss_parts = 1
                        else:
                            ss_parts = 2
                        for sp in range(ss_parts):
                            traj_left = cf_x['heat_w'] + 3 * cf_x['gap_w'] + \
                                        s * (cf_x['gap_t'] + traj_w)
                            if s in [0, 2]:  # draw dashed lines between
                                # sessions
                                line = mlines.Line2D(
                                    [traj_left + traj_w + cf_x['gap_t']/2] * 2,
                                    [traj_bottom,
                                     traj_bottom + cf_y['traj_h']*0.95],
                                    lw=1, ls='--', color='k', alpha=0.4)
                                ax_fig.add_line(line)

                            axt = fig.add_axes(
                                [traj_left, traj_bottom, traj_w,
                                 cf_y['traj_h']])

                            if ss.ss_no == 0:
                                # standardize rotation of the maze (shocked arm
                                # should be on the left)
                                ang = mu.rotate_env(ss, ang)
                                if (ss_n.iloc[2].stim_at -
                                    ss_n.iloc[1].stim_at) in [1, -2]:
                                    flip = True
                                else:
                                    flip = False

                            # plot the maze as a background
                            aplt.plot_env(ss, ax=axt, calib=calib, ang=ang,
                                          edge=False)
                            # add a scale bar for s = 0
                            calib = False

                            # plot the trajectory of the first or last 10
                            # minutes of the session
                            if s == 0:
                                t0, t1 = 5/6, 1
                            elif s == 1:
                                t0, t1 = 0, 1/12
                            elif s == 2:
                                t0, t1 = 11/12, 1
                            elif s == 3:
                                t0, t1 = 0, 1/6
                            elif s == 4:
                                t0, t1 = 1/6, 2/6

                            f, axt = aplt.plot_ss(ss, ax=axt, x0=t0, x1=t1,
                                                  ang=ang, **traj_kw)
                            if flip:
                                axt.invert_yaxis()
                            if Title:
                                axt.set_title(part_titles[s])

                            s += 1
                    Title = False

        nonzero_i += 1


if __name__ == '__main__':
    # example xp_id and annotations to illustrate strategies
    ss_list = [md.ssa.loc[md.ssa.xp_id == x] for x in
               ['170418-04-01', '300517-08-01', '170418-07-01',
                '300517-03-01']]
    strategy_names = ['Does not\navoid', 'Prefers\ncenter',
                      'Prefers two\nsafe arms', 'Prefers one\nsafe arm']
    arm_names = [['Conditioned arm', 'Preferred safe arm', 'Other safe arm',
                  'Center']] * 2
    session_names = ['Conditioning\nlast 5\'', 'Test (no shocks)\nfirst 5\'']
    ss_test = md.ssa_test_3.loc[md.xp_new & (md.ssa.xp_age > 20)]
    seq_list = ['ivx'] * 2

    fig = plt.figure(figsize=[8, 11])
    heat_figure(fig, ss_list, strategy_names, arm_names, session_names, ss_test,
                seq_list, push_high_shocked=-1, x0=0., x1=1., y0=0.5, y1=1.0)

    # plot first half of the figure for xp with pattern replacement
    strategy_names = ['Does not\navoid', 'Prefers\ncenter',
                      'Continues\nto avoid', 'Continues\nto avoid']
    session_names = ['Conditioning\nlast 5\'',
                     'Test (after pattern replacement)\nfirst 5\'']
    arm_names = [['Conditioned arm', 'Preferred safe arm', 'Other safe arm',
                  'Center'],
                 ['Conditioned arm\n(pattern replaced)', 'Preferred safe arm',
                  'Other safe arm', 'Center']]
    seq_list = ['vxi', 'dxi']
    ss_list = [md.ssa.loc[md.ssa.xp_id == x] for x in
               ['220118-08-01', '300118-03-01', '070218-04-01', '120218-01-01']]
    ss_replace = md.ssa_replace[md.xp_new]
    heat_figure(fig, ss_list, strategy_names, arm_names,
                session_names, ss_replace, seq_list, invert=True,
                push_high_shocked=True, x0=0., x1=1., y0=0.0, y1=0.5)

    # label the panels
    panel_font = 15
    panel_top = 0.98
    panel_bot = 0.48
    fig.text(0.12, panel_top, 'A', fontsize=panel_font)
    fig.text(0.33, panel_top, 'B', fontsize=panel_font)
    fig.text(0.6, panel_top, 'C', fontsize=panel_font)
    fig.text(0.12, panel_bot, 'D', fontsize=panel_font)
    fig.text(0.33, panel_bot, 'E', fontsize=panel_font)
    fig.text(0.6, panel_bot, 'F', fontsize=panel_font)

    fig.savefig('../figures/figure_{}.png'.format(5), dpi=300)

    im = Image.open('../figures/figure_{}.png'.format(5))
    im.save('../figures/figure_{}.tiff'.format(5), compression='tiff_lzw')

