import numpy as np
import pandas as pd
import re
import cv2


def arm_centers(pts):
    """
    Return average of arm corner coordinates - an estimate of arm centers
    :param pts: array-like, (x, y) coordinates of arm points

    :return : array with x,y coordinates of arm point average - one pair for
                each arm
    """

    pts = np.asarray(pts)
    return np.mean(pts, axis=1)


def maze_center(ss):
    """
    Return average of maze points - an estimate of maze center
    :param ss: Series, contains information about a session
    :return: array-like, x,y coordinates of maze center
    """
    return ss.env_geometry.mean(1).mean(0)


def maze_contour(co, x_n='x', y_n='y'):
    """
    Calculate actual maze contour

    Maze points give only a wide contour of the maze. The actual area where
     a fish can swim is smaller. Here I estimate the actual area by looking
     at fish trajectory and finding a contour around it (obviously FAILS if
     trajectory is not dense enough)

    :param co: DataFrame, coordinates of a session
    :param x_n: string, name of column with x coordinates
    :param y_n: string, name of column with y coordinates
    :return co_cntr: array-like, a contour around the trajectory
    """

    # first plot the trajectory
    xmax, ymax = co[x_n].max(), co[y_n].max()
    im = np.zeros([int(xmax) + 30, int(ymax) + 30])
    for i, xy in co.iterrows():
        x, y = xy[x_n], xy[y_n]
        cv2.circle(im, (int(y), int(x)), 3, 255, -1)

    # close trajectory to close small gaps
    kernel = np.ones((20, 20), np.uint8)
    closed = cv2.morphologyEx(im, cv2.MORPH_CLOSE, kernel)

    # return largest contour - should be the outline of the maze
    im2, contours, hierarchy = cv2.findContours(closed.astype(np.uint8), 2,
                                                cv2.CHAIN_APPROX_NONE)
    areas = [cv2.contourArea(c) for c in contours]
    max_index = np.argmax(areas)
    co_cntr = contours[max_index]
    return co_cntr


def get_center(env):
    """
    Return coordinates of environment center.

    Maze points consist of arm points for each arm polygon. The first and last
    coordinate pairs of each arm polygon are by convention those opening to
    the center. Combining these points will produce the points of the center.
    Used for example for drawing the central compartment.

    :param env: array, contains arm points for each arm
    :return : array, center points"""

    return env[:, [0, -1]].reshape(-1, 2)


def dist2border(pt, l_pair):
    """
    Find the closest border to an arbitrary point

    The border is an interval, so the distance is calculated to that
    interval, not the collinnear line

    :param pt: tuple, (x, y) of the point
    :param l_pair: list, coordinates of maze borders (see dots2borders function)

    :return dist: float, distance from the point to closest border
    :return bord: int, index of closest border in l_pair
    """

    if np.isnan(pt).any():
        return None, None

    pt = pt.astype(int)
    bord = None
    dist = np.inf
    for j, p in enumerate(l_pair):
        # three points, border and input pt, form a triangle
        trio = np.vstack([p, np.array([pt])])

        # vectors between pairs of points
        s0 = np.diff(trio[:2], axis=0)  # 0 -> 1
        s1 = np.diff(trio[[0, 2]], axis=0)  # 0 -> 2
        s2 = np.diff(trio[1:], axis=0)  # 1 -> 2

        if np.vdot(s0, s1) < 0:
            d = (s1 ** 2).sum(axis=1) ** 0.5
        elif np.vdot(-s0, s2) < 0:
            d = (s2 ** 2).sum(axis=1) ** 0.5
        else:
            ar = cv2.contourArea(trio.astype(np.int32))
            base = (s0 ** 2).sum(axis=1) ** 0.5
            d = 2*ar/base
        if d < dist:
            dist = d
            bord = j

    return dist, bord


def dots2borders(ss):
    """
    Produce an array of borders from maze dots

    Takes a session, extracts array with maze coordinates (x,y) of dots.
    Combines adjacent dots into to produce an array of borders, one border
     per row.

    :param ss: Series, contains information about a session
    :return l_pair: array, contains borders of the maze
    """
    layout = ss.env_geometry
    lin_layout = layout.flatten()
    s = lin_layout.size
    l_flat = lin_layout.reshape([int(s/2), 2])
    l_roll = np.roll(l_flat, 2)

    # get pairs of dots, each row corresponds to a border
    l_pair = np.hstack([l_flat, l_roll]).reshape([int(s/2), 2, 2])

    return l_pair


def get_border_coords(num, ss):
    """
    Get coordinates of a border with index num

    :param num: int, border index
    :param ss: Series, contains information about a session
    :return: array, border coordinates
    """
    l_pair = dots2borders(ss)
    return l_pair[num]


def layout_from_points(pts):
    """
    Return maze arm layout from maze geometry.

    Given a contour (a list [[coords 1],[coords 2]...[coords n]],
    where coords i is a mx2 array of m corner points), produce a maze
    arm layout of the type e.g. 'lur' (left, upper, right).

    """

    left, right, upper, bottom = 'l', 'r', 'u', 'b'
    midpts = arm_centers(pts)
    no_arms = midpts.shape[0]
    layout = [None] * no_arms

    # find arms which are further apart in x-axis than in y-axis,
    # and define them as a left-right axis
    for p1 in range(midpts.shape[0]):
        for p2 in range(midpts.shape[0]):
            if abs(midpts[p1]-midpts[p2])[0] > abs(midpts[p1]-midpts[p2])[1]:
                lr = [p1, p2]  # horizontal axis
                break
    ub = []
    for p1 in range(midpts.shape[0]):
        if p1 not in lr:
            ub.append(p1)  # vertical axis

    if midpts[lr[0], 0] > midpts[lr[1], 0]:
        lr[0], lr[1] = lr[1], lr[0]
    layout[lr[0]] = left
    layout[lr[1]] = right

    # y coordinate is increasing from up to down! y(up) < y(down)
    if no_arms == 3:
        if midpts[ub[0], 1] > (midpts[lr[0], 1]+midpts[lr[1], 1])/2:
            layout[ub[0]] = bottom
        else:
            layout[ub[0]] = upper
    elif no_arms == 4:
        if midpts[ub[0], 1] > midpts[ub[1], 1]:
            ub[0], ub[1] = ub[1], ub[0]
        layout[ub[0]] = upper
        layout[ub[1]] = bottom

    return ''.join(layout)


def map_patt(patt2map, patt):
    """
    Map pattern patt2map to a sequence patt

    :param patt2map:
    :param patt:
    :return mapped: list, contains indices of pattern patt2map in sequence patt

    :Example:

    patt='ivd', patt2map='dvi'
    patt[0] = 'i', its index in patt2map is 2;
    patt[1] = 'v', its index in patt2map is 1;
    patt[2] = 'd', its index in patt2map is 0.

    >>> maze_utils.map_patt('dvi', 'ivd')
    [2, 1, 0]
    """
    mapped = []
    for p in patt:
        mapped.append(patt2map.index(p))
    return mapped


def relative(arm, arm_ref, arm_no=3):
    """
    Calculate relative positions or arms to a reference arm

    :param arm: array-like, contains arms of interest (in geometry notations -
                    0-left, 1-upper, 2-right...)
    :param arm_ref: int, reference arm (e.g. arm where shocks are applied,
                        in geometry notations - 0, 1, 2...)
    :param arm_no: int, number of arms in the maze
    :return rel_arm: array, contains indices of input arms in reference to
                        arm_ref, arm=arm_ref has index0,
                        then clockwise 1, 2, ...

    :Example:

    Arms 0,1,2 and center 3 to remap so that reference arm 1 is remapped to 0

    >>> relative([0,1,2,3], 1, 3)
    array([2, 0, 1, 3])
    """

    if not isinstance(arm_ref, int):
        raise TypeError('arm_ref must be an integer')

    arm = np.array(arm, dtype=int)

    # XXX isn't it better to define center and undefined such that
    # XXX their values don't change with the number of arms?
    # XXX e.g. CENTER, UNDEFINED = 254, 255
    CENTER, UNDEFINED = arm_no, arm_no + 1

    rel_arm = UNDEFINED*np.ones_like(arm)

    is_center = arm == arm_no
    is_undefd = arm > arm_no
    is_an_arm = np.isin(arm, range(arm_no))  # e.g. arm=0,1,2...arm_no-1

    # precondition (pretty obvious, but can fail e.g. if negative nos)
    assert len(arm) == np.sum(is_center)+np.sum(is_undefd)+np.sum(is_an_arm)

    # center does not need remapping
    rel_arm[arm == CENTER] = CENTER

    arm_diff = arm[is_an_arm] - arm_ref
    rel_arm[is_an_arm] = arm_diff % arm_no

    return rel_arm


def relative_pref(arm, arm_sh, arm_pref):
    arm_new = arm.copy()
    arm_new[arm == arm_pref] = 0
    arm_new[arm == arm_sh] = 1
    arm_new[(arm != arm_sh) & (arm != arm_pref) & (arm != 3)] = 2

    return arm_new


def rotate_env(ss, ang):
    sh_arg = ss.stim_at
    a_env = arm_centers(ss.env_geometry)
    c_env = maze_center(ss)
    if a_env[sh_arg, 0] > c_env[0]:
        ang += 120
    if (np.argmax(a_env[:, 0]) != np.argmin(
            a_env[:, 1])) and \
            (np.argmax(a_env[:, 0]) != np.argmax(
                a_env[:, 1])):
        ang += 60
    if sh_arg == np.argmin(a_env[:, 1]):
        ang *= -1
    return ang


def patt_2_1(patt_seq, stim_at):
    """
    Calculate pattern-based remapping of pattern sequence for 2-1 experiments

    There is a safe pattern (never shocked), and shocked pattern (present in
    two arms - only one arm is shocked). Remapping goes as follows:
    stimulated arm ('stim_at') is remapped to 0; safe arm with shocked
    pattern is remapped to 1; safe arm with safe pattern is remapped to 2.

    :param patt_seq: string, pattern sequence
    :param stim_at: int, index of stimulated arm
    :return: list, contains indices of remapped patterns

    Example:
    Pattern sequence 'xvx', stimulation in arm 0
    >>> patt_2_1('xvx', 0)
    [0, 2, 1]

    Pattern sequence 'xvx', stimulation in arm 2
    >>> patt_2_1('xvx', 2)
    [2, 0, 1]
    """

    p_sh = patt_seq[stim_at]  # identify shocked pattern
    for i in re.finditer(p_sh, patt_seq):
        if i.start() != stim_at:
            p_sh_1 = i.start()  # find index of safe arm with shocked pattern
    for i in range(3):
        if (i != stim_at) & (i != p_sh_1):
            p_nosh = i  # find the safe arm with safe pattern
    return [stim_at, p_sh_1, p_nosh]
