import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

from sklearn.cluster import KMeans, AgglomerativeClustering
from sklearn.decomposition import PCA
from scipy.cluster import hierarchy

import zebramaze.md as md
import zebramaze.maze_utils as mu
import zebramaze.analysis as analysis
import zebramaze.plotting as plotting


def ssa2matrix(ss_sub, funcs, center=False, arms=False, **kw):
    m = []
    for g, ss in ss_sub.groupby('xp_id'):
        show = [0]
        if center:
            show += [3]
        if arms:
            show += [1]
        v = trace2vector(funcs, ss, show=show, **kw)
        if np.isnan(v).any():
            print(ss.xp_id)
            continue
        m.append([g]+v)
    l = min([len(i) for i in m])
    m = [i[:l] for i in m]
    return m


def trace2vector(funcs, ss_sub, full=False, show=[0], sess=[],
                 orig=False, **kw):
    v = []
    if not sess:
        sess = ss_sub.ss_no.unique().tolist()
    for i, ss in ss_sub.iterrows():
        if ss.ss_no not in sess:
            continue
        coords = pd.read_hdf(ss.ss_coords_path)
        if 'recorded_arm' not in coords.columns:
            coords = coords.rename(columns={'arm': 'recorded_arm'})

        for ind in show:
            if orig & (ind != 3) & (ss.ss_no == 2):
                st = [ss.stim_at, int(ss_sub.iloc[0].stim_at)]
            else:
                st = [ss.stim_at]
            for stim_at in st:
                coords.recorded_arm = mu.relative(coords.recorded_arm, stim_at)
                for func in funcs:
                    if func == analysis.occupancy:
                        val = [0, 1, 2, 3]
                    else:
                        val = [0, 1, 2]
                    sldng, sldng_sum, win_size,\
                        ovrlp = analysis.sliding(coords.recorded_arm, func,
                                                 val=val, **kw)

                    if (func == analysis.entry) & (ind == 3):
                        continue
                    if ind == 1:
                        # calculate difference between two other arms
                        # if np.trapz(a) < 0:
                        a = np.abs(sldng[ind] - sldng[ind+1])
                        #    a = -a
                    else:
                        a = sldng[ind]
                    if full:
                        v += list(a)
                    else:
                        if ss.ss_no == 0:
                            v.append(a.values[-1])
                        elif ss.ss_no == 1:
                            v.append(a.values[0])
                            v.append(a.values[len(a)/2])
                            v.append(a.values[-1])
                        else:
                            v.append(a.values[0])
    return v


def norm(ssa_m):
    # normalize data before PCA
    tr_m = ssa_m[ssa_m.columns[1:]]
    ssa_m_n = tr_m - tr_m.mean(0)
    ssa_m_n /= ssa_m_n.std(0)
    ssa_m_n.T.plot()
    plt.legend('')
    plt.setp(plt.gca(), ylabel='Entry/occupancy of arms (merged and z-scored)')
    plt.show()
    return ssa_m_n


def pca(ssa_m):
    pca = PCA()
    ssa_mproj = pca.fit_transform(ssa_m.values)
    min_comp = min(
        np.where(np.cumsum([0] + list(pca.explained_variance_ratio_)) > 0.9)[0])
    plt.plot(np.cumsum([0] + list(pca.explained_variance_ratio_)))
    plt.setp(plt.gca(), ylabel='Explained variance', xlabel='Component number')
    print("{} components explain > 90% of variance".format(min_comp))
    pca = PCA(n_components=min_comp)
    ssa_mproj = pca.fit_transform(ssa_m.values)
    ssa_m_ = pca.inverse_transform(ssa_mproj)
    pd.DataFrame(ssa_m_).T.plot()
    plt.legend('')
    plt.setp(plt.gca(), ylabel='Entry/occupancy of arms,\nreplotted in first '
                               '{} components'.format(min_comp))
    plt.show()
    # return data reconstructed from first `min_comp` components
    return pca, ssa_m_


def km_alg(tr_m, n):
    kmeans = KMeans(init='k-means++', n_clusters=n, n_init=10)
    y_pred = kmeans.fit_predict(tr_m)
    # plot cluster centers
    plt.plot(np.arange(kmeans.cluster_centers_.shape[1]),
             kmeans.cluster_centers_.T)
    plt.setp(plt.gca(), ylabel='Centers of clusters')
    plt.show()
    return y_pred


def ac_alg(tr_m, n):
    ac = AgglomerativeClustering(n_clusters=n, linkage='ward')
    ac.fit(tr_m)
    y_pred = ac.labels_.astype(np.int)
    return y_pred


def hi_alg(tr_m, n):
    Z = hierarchy.linkage(tr_m, 'ward')
    dn = hierarchy.dendrogram(Z)
    y_pred = hierarchy.cut_tree(Z, n_clusters=n)[:, 0]
    return y_pred


def cluster(ssa_m, alg, **kw):
    if type(ssa_m) == pd.DataFrame:
        tr_m = ssa_m[ssa_m.columns[1:]]
    else:
        tr_m = ssa_m.copy()
    y_pred = alg(tr_m, **kw)
    print(y_pred)
    c_map = ['r', 'g', 'b', 'k', 'w']
    pd.DataFrame(tr_m).T.plot(color=[c_map[i] for i in y_pred])
    plt.ylim([0, 1])
    plt.legend('')
    plt.setp(plt.gca(), xlabel='',
             ylabel='Entry/occupancy of arms (merged)')
    plt.gca().set_xticks([])
    plotting.set_plot_params(plt.gca())
    plt.show()
    return y_pred


def good_bad(ss, ssa_m, y_pred, y):
    gb = ssa_m[0][y_pred == y]
    return ss.loc[ss.xp_id.isin(gb)]


if __name__ == '__main__':
    # ss_sub = md.ssa_test_3.loc[md.xp_new & (md.ssa.xp_age>20)]
    ss_sub = md.ssa.tail(100)
    over = analysis.overstay(ss_sub)
    ss_sub = ss_sub.loc[~ss_sub.xp_id.isin(over)]

    m = ssa2matrix(ss_sub, [analysis.entry, analysis.occupancy], center=True,
                   **{'full': True, 'win_size': 900, 'sliding_overlap': 820,
                   'sess': [0, 1]})
    ssa_m = pd.DataFrame(m)
    ssa_m[ssa_m.columns[1:]].T.plot()
    plt.setp(plt.gca(),
             ylabel='Occupancy/entries to shocked arm and \noccupancy'
                    ' of central compartment (merged)')
    plt.legend('')
    plt.show()

    m = ssa2matrix(md.ssa_turn, [analysis.occupancy],
                   **{'win_size': 1200, 'sliding_overlap': 1140, 'full': True})
    m = pd.DataFrame(m)
    pca(m)
    m_n = norm(m)
    pca(m_n)
    cluster(m, km_alg)
    cluster(m, ac_alg)
    cluster(m, hi_alg)
