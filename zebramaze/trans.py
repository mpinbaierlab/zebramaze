import os
import numpy as np
import pandas as pd

import zebramaze.geom_utils as gu
import zebramaze.maze_utils as mu
import zebramaze.md as md
import viz.intv


def raw_stays(coords, arm='armfix'):
    """Return intervals of stays in arms.

    Nothing is filtered out, in particular not outliers.

    Parameters
    ----------
    coords (df) -- df with |recorded_arm| column. df.index is assumed to run
    frame-wise


    Return
    ------
    stays (df) -- |raw_stay i8 (index)|a i8|b i8|ds i8|
                  a, b sample numbers in coordinate frames

    """

    if type(coords) == np.ndarray:
        coords = pd.DataFrame({arm: coords})

    stays = pd.DataFrame(columns=('a', 'b', 'ds', 'arm'))

    # NaN->True in astype makes 1st element an entry, last an exit, as desired
    is_entry = (coords[arm] - coords[arm].shift(1)).astype(np.bool)
    is_exit = (coords[arm] - coords[arm].shift(-1)).astype(np.bool)

    stays = stays.assign(arm=coords[arm].loc[is_entry])

    stays = stays.reset_index(drop=False)
    # renaming just for documentation
    stays.index.rename('raw_stay', inplace=True)

    stays = stays.assign(
        a=coords.index[is_entry].values,
        b=coords.index[is_exit].values)

    stays[['a', 'b']] = stays[['a', 'b']].astype(int)

    stays = stays.assign(ds=stays.b-stays.a+1)

    # check if durations of stay are positive - they MUST be!
    wormholes = stays.ds <= 0
    if wormholes.any():
        raise (Exception, '{} nonpositive durations!'.format(wormholes.sum()))

    return stays


def long_enough(stays, arm_set={0, 1, 2}, threshold=1, sr=60):
    """Return df without stays shorter than a threshold in arms from arm_set.

    arm_set (set) -- arms affected. Usually exclude center, where
                     short stays are OK.

    threshold (float) -- definition of a short stay in
                         seconds. Usually it is chosen to match the
                         shock policy, i.e. if stays below 1 second
                         did not result in shocks, threshold = 1;

    sr (float) -- sampling rate in frames per second

    """

    return stays.loc[((stays.arm.isin(arm_set)) & (stays.ds > threshold*sr)) |
                     (~stays.arm.isin(arm_set))]


def drop_value(stays, drop):
    """Return stays, after removing rows where arm value belongs to drop list.

    stays df -- df |arm i2|
    drop list of int -- which arm codes are to be disregarded (usually
                        either undefined arm assignments or the center)
    """

    is_keep_arm = ~stays.arm.isin(drop)
    return stays.loc[is_keep_arm]


# NOTE: drop has to be described separately, because it's not always
# applied together with merge (see example in docs\stays_transitions
# notebook)
def merge_arms(stays, drop):
    """Return stays merged when they are contiguous or separated by drop values.

    stays df -- df |arm i2|
    drop list of int -- which arm codes are bridged when
                        merging. Usually used to disregard either
                        undefined arm assignments or the center.
    """

    final_sample_no = stays.iloc[-1]['b']

    merged = drop_value(stays, drop=drop)

    # stay column now counts the merged raw_stays (those separated by
    # any of the values in drop)
    next_arm = merged.arm.shift(1).fillna(merged.arm.iloc[0]).astype(int)
    is_new_arm = next_arm != merged.arm
    merged = merged.assign(stay=is_new_arm.cumsum())

    # take 1st element of each arm group (gives right start 'a')
    def first_of_group(x): return x.iloc[0]
    merged = merged.groupby('stay').apply(first_of_group).drop('stay', axis=1)

    # use start 'a'-1 of next row to calculate end 'b' of present row,
    # fix the last (NaN) value and ensure good type
    next_start = merged['a'].shift(-1)
    merged['b'] = next_start.fillna(final_sample_no+1).astype(int) - 1

    # if the first entry was dropped, it should be counted into the next one...?
    merged.loc[0, 'a'] = 0  # stays.iloc[0].a

    # recompute the duration in samples
    merged['ds'] = merged['b'] - merged['a'] + 1

    return merged


def stays2sequence(stays):
    """ Inverse function of raw_stays

    Parameters:
    ----------
    stays   : a DataFrame with |stay i8 (index)|a i8|b i8|ds i8|
              a, b sample numbers in coordinate frames

    Returns:
    -------
    arms    : a sequence of arms corresponding to the stays
    """
    dur = stays.iloc[-1].b+1
    arms = np.ones([dur])*(-1)
    for i, st in stays.iterrows():
        arms[st.a:st.b+1] = st.arm

    if sum(arms == -1) > 0:
        raise Exception("There were gaps in stays file!")

    return arms.astype(np.uint8)


def transition_matrix(merged):
    """Return transition matrix with counts of pairs of transitions

    merged df -- df |arm i2|

    arm sequence arm=[1,2,1,0,2,1,1] has these pairs: (12)(21)(10)(02)(21)(11)
    The result of transition_matrix(pd.DataFrame({'arm':arm})) will be:
                            0       1       2
                        0   NaN     NaN     1.0
                        1   1.0     1.0     1.0
                        2   NaN     2.0     NaN

    """
    merged = merged.assign(next_arm=merged.arm.shift(-1))

    # last transition: we don't know where to
    merged = merged.iloc[:-1]
    merged['next_arm'] = merged.next_arm.astype(int)

    return merged.groupby(['arm', 'next_arm']).size().unstack('next_arm')


# XXX go line-by-line and comment-by-comment fixing/documenting
# additionally: check that the returned dtypes are right (integers
# should be integers)

# XXX discuss whether index of train should be frame number or stay
# number (current status) and rename/drop accordingly

def shock_trains(session, tr_buf=0, tr_len=60, arm='arm'):
    """Return guessed shock trains as interval dataframe

    Parameters
    ----------
    session -- dict-like with |ss_coords_path|stim_abs|ss_no|cross_date|

    Return
    ------
    trains -- data frame with only those stays where shocks were
              applied, and the times and durations of the shock trains inside

              stay data given by |a|b|ds| columns
              shock train data given by |a_train|b_train|ds_train
              frame number in .index

    ss_duration -- the total number of frames of the session
    """

    # relevant constants, (converted to frame numbers)
    sr = 60  # frames per second
    TRAIN_BUFFER = tr_buf*sr   # unshocked time right after entry
    TRAIN_LENGTH = tr_len*sr  # cap to the length of a shock train

    coords = pd.read_hdf(session['ss_coords_path'])

    stays = raw_stays(coords, arm=arm)
    # removes stays shorter than TRAIN_BUFFER
    stays = long_enough(merge_arms(stays, [4]), threshold=tr_buf, sr=sr)

    # pick only transitions in and out of shocked arm, where rel = 0
    arm_ref = int(session.stim_at)
    stays = stays.assign(rel=mu.relative(stays.arm, arm_ref))
    stays = stays.loc[stays.arm == arm_ref]

    # stays.a ... stays.b is time in shocked arm, but
    # stays.a_train ... stays.b_train is time with actual shocks

    trains = stays[['index', 'a', 'b', 'arm', 'rel']].copy()
    trains = trains.assign(a_train=trains.a.values + TRAIN_BUFFER)

    # if fish was already in the shocked arm at the start of the session,
    # shocks start immediately and NOT after TRAIN_BUFFER (see
    # docs\shock_timestamps notebook)
    if not trains.empty:
        if (trains.a.values[0] == 0) & (session['ss_no'] != 0):
            trains.loc[trains.index.values[0], 'a_train'] = 0

    # capping of shock triggering is counted as TRAIN_LENGTH from the entry
    # to the arm, not from the moment of first shock, will be changed in the
    # future in new experiments (after 2016-03-16)
    if session.cross_date <= pd.to_datetime('2016-03-16'):
        trains = trains.assign(cap=trains.a.values+TRAIN_LENGTH)
    else:
        trains = trains.assign(cap=trains.a_train.values + TRAIN_LENGTH)

    # trains end when too long or when exiting, whichever happens first
    trains = trains.assign(b_train=trains[['cap', 'b']].min(axis=1))
    del trains['cap']

    trains = trains.assign(ds_train=trains.b_train-trains.a_train)

    overlong_trains = trains.ds_train > TRAIN_LENGTH
    negative_trains = trains.ds_train < 0
    if overlong_trains.any():
        raise Exception('Overlong trials:\n {}'.format(trains[overlong_trains]))
    if negative_trains.any():
        raise Exception('Phantom trials:\n {}'.format(trains[negative_trains]))

    # interval between triggering of shocks is 60 frames, excluding
    # initial frames of shocks, so shocks happen every 61st frame
    SHOCK_EACH = sr+tr_buf
    shock_count = np.floor(trains.ds_train/SHOCK_EACH).astype(np.int)
    # +1 because in the interval always at least one shock is included at the
    #  a_train frame number
    trains = trains.assign(train_shocks=shock_count+1)

    ss_duration = len(coords)  # CHECKED: matches session.ss_duration

    return trains, ss_duration


def shocks_iv(trains, ss_duration):
    """Returns a df of all intervals when shock was applied in a session

    Parameters
    ----------
    trains      -- dataframe with |a_train|b_train| columns
    ss_duration -- total duration of a session in frames

    Returns
    -------
    shocks -- df with intervals during which shocks were applied
            interval data given by |a_shock|b_shock"| columns
    """

    sr = 60
    SHOCK_DURATION = 6

    shocks = pd.DataFrame(columns=['a_shock', 'b_shock'])
    a_mask = np.zeros(ss_duration, dtype=np.bool)

    # stride is sr+1, because interval between triggering of shocks is sr=60
    # frames not including initial frames of shocks, so shocks happen every
    # (sr+1)th = 61st frame
    for i, row in trains.iterrows():
        a_mask[int(row['a_train']):int(row['b_train']):(sr+1)] = True

    # find start and end frames of each shock, duration of a shock is a
    # constant
    shocks['a_shock'] = np.where(a_mask)[0]
    shocks['b_shock'] = shocks['a_shock'] + SHOCK_DURATION - 1

    return shocks


def shocks_mask(shocks_iv, ss_duration):
    """Return mask showing when shocks were active.

    Parameters
    ----------
    shocks_iv   -- dataframe with |a_shock|b_shock| columns
    ss_duration -- total duration of a session in frames

    Returns
    -------
    mask -- bool array, on at sample numbers where shock active
    """

    mask = np.zeros(ss_duration, dtype=np.bool)

    # b_shock is an inclusive interval end!
    for i, row in shocks_iv.iterrows():
        mask[row['a_shock']:row['b_shock']+1] = True

    return mask


def colmap(arm):
    colors = {1: '#5B84A3', 0: 'r', 2: '#97B364', 3: 'black', 4: 'white',
              100: '#BB0000', 99: 'grey'}
    return colors[arm]


def sh_index(co):
    # indices of shock onsets
    sh_start = co.shock.astype(int).diff() == 1
    if co.iloc[0].shock:
        sh_start[0] = True
    sh_index = co.index[sh_start]
    return sh_index


def sh_index_off(co):
    # indices of shock offsets
    sh_index = co.index[co.shock.astype(int).diff() == -1]
    return sh_index


def transitions_raw(ss, ax, spacing=3, width=0.05, sr=3600.0, label=True,
                    bouts=False):
    """Plots raw transitions of each experiment in ss_sample

    Parameters:
    -----------
    ss_sample  -- df, a subtable of ssa

    Transitions are calculated session-wise for each experiment and then
    concatenated (for better visualization).
    Shock sessions (ss_type='SHOCK') are marked by grey background.
    In addition shock trains are calculated (the times when actual shocks were
    present, not just the stay in shocked arm) and are shown in red color and
     wider bands.

    !!! Short segments (e.g. 6 frames durations of shocks) are not visible
    on this time scale, zoom in is needed
    """

    widths = {0: 0.5, 1: 0.5, 2: 0.5, 3: 0.5, 4: 0.5, 100: 0.6}
    # 100 is arbitrary number used for marking
    # shock trains as opposed to shock arm

    ax.grid(b=False)
    ss_sample = ss.reset_index(drop=True, inplace=False)
    i = 0
    grr = ss_sample.groupby('xp_id')
    for g_name, ssi in grr:
        i += 1
        last = 0
        sa = pd.DataFrame()
        ta = pd.DataFrame()
        rect = [0, 0]
        sh_ind = np.array([])
        for j, s in ssi.iterrows():  # calculate raw stays for each session
            # and concat
            coords = pd.read_hdf(s.ss_coords_path)
            st = raw_stays(coords)
            # load shock trains separately to show them with wider bands
            tr, dur = shock_trains(s)

            st.loc[:, ['index', 'a', 'b']] += last
            tr.loc[:, ['a_train', 'b_train']] += last
            st.arm = mu.relative(st.arm, s.stim_at)

            if s.ss_type == 'SHOCK':
                disp = gu.displacement(coords)
                sh_ind = sh_index(coords)
                disp_roll = np.array([disp[x:x+20].max() for x in sh_ind])
                sh_ind = sh_ind[disp_roll > 15] + last

                rect = [last / sr, (st.b.values[-1]) / sr]
                tr['rel'] = 100
            last = st.b.values[-1] + 1
            sa = pd.concat([sa, st], ignore_index=True)
            ta = pd.concat([ta, tr], ignore_index=True)

        # will plot unfiltered transitions!
        # grey shock patch
        grey_df = pd.DataFrame({'a': [rect[0]], 'b': [rect[1]], 'k': 99})
        ax = viz.intv.ribbon(grey_df, 1, colmap, spacing * (i-1)+1,
                             {99: 1.2}, 'k', ax=ax, **{'alpha': 0.3})

        ax = viz.intv.ribbon(sa, sr, colmap, spacing * (i-1)+1, widths,
                             'arm', 'a', 'b', ax)
        ax = viz.intv.ribbon(ta, sr, colmap, spacing * (i-1)+1, widths,
                             'rel', a='a_train', b='b_train', ax=ax)

        if bouts:
            try:
                mrk = ['o', '.', '*', 'd', 'v', '.']
                sz = [10, 30, 50, 15, 15, 30]
                clrs = ['k', 'g', 'r', 'k', 'g', 'k']
                bout_clust = pd.read_csv('../notebooks/test_1_bouts.csv')
                current_id = bout_clust.loc[bout_clust.xp_id == g_name]
                for cl in bout_clust.cluster.unique()[:-1]:
                    cl = int(cl)
                    big_sh = current_id.loc[current_id.cluster == cl,
                                            'time'].values
                    ax.scatter((big_sh+ssi.iloc[0].ss_duration)/sr,
                               np.ones(big_sh.shape[0]) *
                               (spacing*(i-1) + 1.1 + cl*0.2 + widths[100]/2),
                               marker=mrk[cl], s=sz[cl], color=clrs[cl])
                # ax.set_ylim([0, 2.7])
            except:
                pass

        # 1.2 is width of 'shock' patch
        # ax.add_patch(Rectangle((rect[0], spacing * (i-1) +0.4), rect[1] -
        #                        rect[0], 1.2, facecolor="grey", alpha=0.3))

    if label:
        ax.set_yticks(spacing*(np.arange(0, len(grr)))+1)
        ax.set_yticklabels(ss.xp_id.unique(), rotation=20)
    return ax


if __name__ == '__main__':
    pass
    # ssa = md.ss_xp_sj()
    # ss = ssa.sample(1).iloc[0]
    # co = pd.read_hdf(ss.ss_coords_path)
    #
    # stays = raw_stays(co)
    # merged = merge_arms(stays, drop=[4])
    #
    # arms = stays2sequence(stays)
    # print(sum(arms != co.recorded_arm.values))
    #
    # merged_arms = stays2sequence(merged)
    # print(sum(merged_arms != co.recorded_arm.values))
